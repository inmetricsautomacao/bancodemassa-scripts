# README #


## 1. Introdução ##

Este repositório contém o código fonte do componente **Bancodemassa-scripts** da solução **Bancodemassa - Cantina**. O componente *bancodemassa-scripts* é um pacote de scripts powershell responsável pela extração, transformação e carga das informações dos sistemas origens para dentro do banco de dados da solução *Bancodemassa Cantina*.

### 2. Documentação ###

### 2.1. Diagrama de Caso de Uso (Use Case Diagram) ###

```image-file
./doc/UseCaseDiagram*.jpg
../../bancodemassa-doc/*
```

### 2.2. Diagrama de Implantação (Deploy Diagram) ###

```image-file
./doc/DeployDiagram*.jpg
../../bancodemassa-doc/*
```

### 2.3. Diagrama Modelo Banco de Dados (Database Data Model) ###

```image-file
./doc/DatabaseDataModel*.jpg
```

## 3. Projeto ##

### 3.1. Pré-requisitos ###

* Linguagem de programação: Powershell
* Sistema Operacional: Windows 7, 8, 10 (requer software Powershell )
* JRE: 1.7 ou 1.8
* Postgresql 9.5+
* Postgresql database schema 'bmnode' instalado
* Postgresql database initialization/configuration scripts executados

### 3.2. Guia para Desenvolvimento ###

* Obtenha o código fonte através de um "git clone". Utilize a branch "master" se a branch "develop" não estiver disponível.
* Faça suas alterações, commit e push na branch "develop".

### 3.3. Guia para Implantação ###

* Obtenha o código fonte através de um "git clone". 
* Configure os arquivos de parametrizações conforme o seu ambiente
* Faca as configurações para o seu ambiente

### 3.4. Guia para Demonstração ###

* n/a

## Referências ##

* n/a
