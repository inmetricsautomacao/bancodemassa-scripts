#!/usr/bin/bash
echo off
###############################################################################
# filename : etl-stage-steps-demo.sh
# parameter: P1 - Environment Type
#            [
#              0: HML - Homologacao (default)
#              1: TI  - Teste Integrado
#              2: DEV - Desenvolvimento
#            ]
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-26 JosemarSilva Development
###############################################################################
#
echo ""
echo "1. Setting up ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
ps -f | grep sh
# Setting variables for command line arguments ...
HOME_ETL_BM=/root/rails-home/bancodemassa-cantina-railsapp/lib/scripts
MYSQL_JDBC_URL="jdbc:mariadb://10.10.1.37/redmineprod?user=guest&password=guest"
POSTGRES_HOST=127.0.0.1
POSTGRES_PORT=5432
POSTGRES_DBNAME=bancodemassanode
POSTGRES_USERNAME=root #bancodemassa
POSTGRES_PASSWORD=root #bancodemassa
POSTGRES_JDBC_URL="jdbc:postgresql://${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DBNAME}?user=${POSTGRES_USERNAME}&password=${POSTGRES_PASSWORD}&ssl=false"


#
echo ""
echo "2. Clean-up stage ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
POSTGRES_CLEANUP_FILENAME=${HOME_ETL_BM}/sql/etl/demo/script-01-postgres-cleanup.sql
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -f ${POSTGRES_CLEANUP_FILENAME}


#
echo ""
echo "3. Copy MySQL tables To Postgres tables ..."
echo ""

QUERY_SELECT_FILENAME=${HOME_ETL_BM}/sql/etl/demo/script-02-mysql-queryselect.sql
QUERY_INSERT_FILENAME=${HOME_ETL_BM}/sql/etl/demo/script-02-postgres-queryinsert.sql
java -jar ${HOME_ETL_BM}/bin/copymysqltopostgres.jar -M ${MYSQL_JDBC_URL} -P ${POSTGRES_JDBC_URL}  -i ${QUERY_INSERT_FILENAME} -s ${QUERY_SELECT_FILENAME}


#
echo ""
echo "4. Loading ..."
echo ""

echo ""
echo "4.1. Loading "
echo ""
date +%Y-%m-%d:%H:%M:%S
POSTGRES_LOAD_NODE_SCRIPT_FUNCTION="SELECT fx_node_execute_etl('${ENVIRONMENT_TYPE_NAME}')"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_LOAD_NODE_SCRIPT_FUNCTION}"

echo ""
echo "4.99. Loading ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
POSTGRES_LOAD_NODE_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/etl/cielo/script-03-postgres-load-node-status.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -f ${POSTGRES_LOAD_NODE_SCRIPT_FILENAME}

#
echo ""
echo "5. Finish ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
#
