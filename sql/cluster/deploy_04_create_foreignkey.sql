﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_04_create_foreignkey.sql
-- Description:
--              
-- Log-changes:
--              * 2018-05-05 JosemarSilva Correcao REFERENCES de fk_cluster_content_type_cluster_config
--              * 2017-05-01 JosemarSilva cluster_content_type_id
--              * 2017-03-14 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'create constraints foreign key ...'
-- ----------------------------------------------------------------------------
ALTER TABLE bm_cluster_config    ADD CONSTRAINT fk_cluster_content_type_cluster_config     FOREIGN KEY (cluster_content_type_id)     REFERENCES bm_cluster_content_type(id);
ALTER TABLE bm_cluster_config    ADD CONSTRAINT fk_cluster_status_cluster_config           FOREIGN KEY (cluster_status_id)           REFERENCES bm_cluster_status(id);
ALTER TABLE bm_cluster_config    ADD CONSTRAINT fk_cluster_environment_type_cluster_config FOREIGN KEY (cluster_environment_type_id) REFERENCES bm_cluster_environment_type(id);
