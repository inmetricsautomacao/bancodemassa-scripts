﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_07_function.sql
-- Description:
--
-- Log-changes:
--              * 2017-07-09 JosemarSilva dbUsername dbPassword
--              * 2017-05-08 JosemarSilva url_rest_service, db_host, db_name, db_port
--              * 2017-05-05 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_available_node( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_available_node( pJsonMsg JSON ) 
RETURNS JSONB AS
$fx_available_node$
DECLARE
  vJsonResult        JSONB;
  vEnvironmentType   VARCHAR(30);
  vContentType       VARCHAR(30);
  vIdClusterConfig   INTEGER;
  vIdEnvironmentType INTEGER;
  vIdContentType     INTEGER;
  vUrlRestService    VARCHAR(100);
  vDbHost            VARCHAR(100);
  vDbPort            INTEGER;
  vDbName            VARCHAR(30);
  vDbUsername        VARCHAR(30);
  vDbPassword        VARCHAR(30);
  vStatusCode        INTEGER;
  vStatusMessage     VARCHAR(100);
BEGIN
  --
  RAISE info '>>> fx_available_node() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  vJsonResult      := pJsonMsg;
  vEnvironmentType := pJsonMsg->'node'->>'environment_type';
  vContentType     := pJsonMsg->'node'->>'content_type';
  vStatusCode      := 1 /* 0: SUCCESS */;
  vStatusMessage   := '';
  --
  RAISE info '       - vEnvironmentType: %', vEnvironmentType;
  RAISE info '       - vContentType: %', vContentType;
  --
  RAISE info '    b. Check pJsonMsg ( environment_type, content_type ) ...';
  --
  SELECT id
  INTO   vIdEnvironmentType
  FROM   bm_cluster_environment_type
  WHERE  cluster_environment_type = vEnvironmentType;
  --
  IF vIdEnvironmentType IS NULL THEN 
    RAISE info '       - ERROR: Unrecognized environment_type';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: Unrecognized JSON element {"node": {"environment_type": "' || COALESCE(vEnvironmentType,'') || '" }}';
  END IF;
  --
  SELECT id
  INTO   vIdContentType
  FROM   bm_cluster_content_type
  WHERE  cluster_content_type = vContentType;
  --
  IF vIdContentType IS NULL THEN 
    RAISE info '       - ERROR: Unrecognized content_type';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: Unrecognized JSON element {"node": {"content_type": "' || COALESCE(vContentType,'') || '" }}';
  END IF;
  --
  IF vStatusCode != 0 /* 0: ERROR */ THEN
    --
    RAISE info '    c. Query Cluster Config ...';
    --
    SELECT cc.id, cc.url_rest_service, cc.db_host, cc.db_port, cc.db_name, cc.db_username, cc.db_password
    INTO   vIdClusterConfig, vUrlRestService, vDbHost, vDbPort, vDbName, vDbUsername, vDbPassword
    FROM   bm_cluster_config cc,
           bm_cluster_content_type ct,
           bm_cluster_environment_type et
    WHERE  cc.cluster_status_id        = 1 /* ENABLED */
    AND    et.id                       = cc.cluster_environment_type_id
    AND    et.cluster_environment_type = vEnvironmentType
    AND    ct.id                       = cc.cluster_content_type_id
    AND    ct.cluster_content_type     = vContentType
    ORDER  BY cc.metric
    LIMIT  1;
    --
    RAISE info '       - vIdClusterConfig: %', vIdClusterConfig;
    RAISE info '       - vUrlRestService: %', vUrlRestService;
    RAISE info '       - vDbHost: %', vDbHost;
    RAISE info '       - vDbPort: %', vDbPort;
    RAISE info '       - vDbName: %', vDbName;
    RAISE info '       - vDbUsername: %', vDbUsername;
    RAISE info '       - vDbPassword: %', vDbPassword;
    --
    IF vIdClusterConfig IS NOT NULL THEN
      --
      vStatusMessage := 'Id(Cluster Config): ' || vIdClusterConfig;
      --
      RAISE info '    d. Update Cluster Config metric ...';
      --
      UPDATE bm_cluster_config
      SET    metric = COALESCE(metric,0) + 1
      WHERE  id = vIdClusterConfig;
      --
    ELSE
      --
      vStatusCode    := 0 /* 0: ERROR */;
      vStatusMessage := 'ERROR: No node enabled is availabe';
      --
    END IF;
    --
  END IF;
      --
      RAISE info '    e. Json Result ...';
      RAISE info '       - vStatusCode: %', vStatusCode;
      RAISE info '       - vUrlRestService: %', vUrlRestService;
      RAISE info '       - vDbHost: %', vDbHost;
      RAISE info '       - vDbName: %', vDbName;
      RAISE info '       - vStatusMessage: %', vStatusMessage;
      --
  --
  vJsonResult := vJsonResult 
                 || ('{'
                 || '"node_return": {'
                 ||     '  "status_code": '      || vStatusCode 
                 ||     ', "status_message": "'  || REPLACE(vStatusMessage,'"','') || '"'
                 ||     ', "url_rest_service": ' || '"' || COALESCE(vUrlRestService,'') || '"'
                 ||     ', "db_host": '          || '"' || COALESCE(vDbHost,'') || '"'
                 ||     ', "db_port": '          || 
                                                    CASE
                                                      WHEN vDbPort IS NULL THEN
                                                           '"' || '"'
                                                      ELSE 
                                                           '"' || vDbPort || '"'
                                                    END
                 ||     ', "db_name": '          || '"' || COALESCE(vDbName,'') || '"'
                 ||   '}'
                 || '}')::jsonb;
  --
  RAISE info '<<< fx_available_node() <<<';
  --
  RETURN vJsonResult;
  --
END;
$fx_available_node$ 
LANGUAGE plpgsql ;



/*
-- ----------------------------------------------------------------------------
-- \echo 'EXECUTE FUNCTION fx_available_node( ) ...'
-- ----------------------------------------------------------------------------
-- ETL x ( HML, TI, DEV ) x 1th time
SELECT fx_available_node( '{ "node": {"environment_type": "HML", "content_type": "ETL"}, "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
SELECT fx_available_node( '{ "node": {"environment_type": "TI",  "content_type": "ETL"}, "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
SELECT fx_available_node( '{ "node": {"environment_type": "DEV", "content_type": "ETL"}, "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
-- STAGE-PIPELINE x ( HML, TI, DEV ) x 1th time
SELECT fx_available_node( '{ "node": {"environment_type": "HML", "content_type": "STAGE-PIPELINE"}, "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
SELECT fx_available_node( '{ "node": {"environment_type": "TI",  "content_type": "STAGE-PIPELINE"}, "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
SELECT fx_available_node( '{ "node": {"environment_type": "DEV", "content_type": "STAGE-PIPELINE"}, "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
-- INTEGRATED TEST
SELECT fx_available_node( '{ "node": {"environment_type": "HML", "content_type": "ETL"}, "get": {"classe_massa": "EC.CONFIG.STAR", "cod_reserva": "RESERVA.PARA.MIM"} }' ) ;
-- INVALID REQUIREMENTS
SELECT fx_available_node( '{ "node": {"environment_type": "***", "content_type": "ETL"},      "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
SELECT fx_available_node( '{ "node": {"environment_type": "TI",  "content_type": "***"},      "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
SELECT fx_available_node( '{ "get": {"classe_massa": "EC.CONFIG.STAR"} }' ) ;
*/

/*
-- ----------------------------------------------------------------------------
-- \echo 'Checking  ...'
-- ----------------------------------------------------------------------------
select cc.id,
       cc.obs,
       '(' || ct.cluster_content_type || ' - ' || et.cluster_environment_type || ' - ' || cs.cluster_status || ' )' AS attributes,
       cc.metric
from   bm_cluster_config cc,
       bm_cluster_content_type ct,
       bm_cluster_environment_type et,
       bm_cluster_status cs
WHERE  ct.id = cluster_content_type_id
AND    et.id = cluster_environment_type_id
AND    cs.id = cluster_status_id
ORDER  BY ct.cluster_content_type, et.cluster_environment_type, cs.cluster_status
;
*/

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
--
