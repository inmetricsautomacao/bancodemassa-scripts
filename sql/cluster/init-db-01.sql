﻿-- ----------------------------------------------------------------------------
-- Filename:
--              init-db-01.sql
-- Description:
--
-- Log-changes:
--              * 2018-05-05 JosemarSilva AD-HOC-SQL
--              * 2018-01-29 JosemarSilva database=bmnode, user=postgres, password=postgres
--              * 2017-06-17 JosemarSilva Development
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- \echo 'Initializing database ...'

-- \echo 'bm_cluster_config ...'

INSERT INTO bm_cluster_config
(
  cluster_content_type_id     ,
  cluster_status_id           ,
  cluster_environment_type_id ,
  url_rest_service            ,
  db_host                     ,
  db_port                     ,
  db_name                     ,
  db_username                 ,
  db_password                 ,
  obs
)
VALUES
(
  1                        /* cluster_content_type_id     (1=ETL; 2=STAGE-PIPELINE;3=AD-HOC-SQL)    */,
  1                        /* cluster_status_id           (0=DISABLED; 1=ENABLED; 2=UNDER CONSTRUCTION */,
  0                        /* cluster_environment_type_id (0=HML; 1=TI; 2=DEV) */,
  'http://localhost:8080/bancodemassa-ws-node/dados/'
                           /* url_rest_service            */,
  'bmcantina.cfjmox99qzpa.us-east-1.rds.amazonaws.com'
                           /* db_host                     */,
  5432                     /* db_port                     */,
  'bmnode'                 /* db_name                     */,
  'bancodemassa'           /* db_username                 */,
  'bancodemassa'           /* db_password                 */,
  'init-db'                /* obs                         */
);


INSERT INTO bm_cluster_config
(
  cluster_content_type_id     ,
  cluster_status_id           ,
  cluster_environment_type_id ,
  url_rest_service            ,
  db_host                     ,
  db_port                     ,
  db_name                     ,
  db_username                 ,
  db_password                 ,
  obs
)
VALUES
(
  3                        /* cluster_content_type_id     (1=ETL; 2=STAGE-PIPELINE;3=AD-HOC-SQL)    */,
  1                        /* cluster_status_id           (0=DISABLED; 1=ENABLED; 2=UNDER CONSTRUCTION */,
  0                        /* cluster_environment_type_id (0=HML; 1=TI; 2=DEV) */,
  'http://localhost:8080/bancodemassa-ws-adhocsql/dados/'
                           /* url_rest_service            */,
  'bmcantina.cfjmox99qzpa.us-east-1.rds.amazonaws.com'
                           /* db_host                     */,
  5432                     /* db_port                     */,
  'bmnode'                 /* db_name                     */,
  'bancodemassa'           /* db_username                 */,
  'bancodemassa'           /* db_password                 */,
  'init-db'                /* obs                         */
);



--
-- \echo 'Checking bm_cluster_config ...'
--
SELECT cc.id,
       COALESCE(cc.obs, '') AS obs,
       '(' || ct.cluster_content_type || ' - ' || et.cluster_environment_type || ' - ' || cs.cluster_status || ' )' AS attributes,
       COALESCE(cc.metric,0) AS metric
FROM   bm_cluster_config cc,
       bm_cluster_content_type ct,
       bm_cluster_environment_type et,
       bm_cluster_status cs
WHERE  ct.id = cluster_content_type_id
AND    et.id = cluster_environment_type_id
AND    cs.id = cluster_status_id
ORDER  BY ct.cluster_content_type, et.cluster_environment_type, cs.cluster_status
;
