﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_03_create_table.sql
-- Description:
--              
-- Log-changes:
--              * 2017-06-17 JosemarSilva add columns db_user e db_password
--              * 2017-05-05 JosemarSilva (url_rest_service, metric)
--              * 2017-04-20 JosemarSilva Constraint PK
--              * 2017-03-14 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'bm_cluster_config ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_cluster_config (
  id SERIAL NOT NULL,
  cluster_content_type_id smallint NOT NULL,
  cluster_status_id smallint NOT NULL,
  cluster_environment_type_id smallint NOT NULL,
  url_rest_service character varying(100) NOT NULL,
  db_host character varying(100) NOT NULL,
  db_port integer NOT NULL,
  db_name character varying(30) NOT NULL,
  db_username character varying(30) NOT NULL,
  db_password character varying(30) NOT NULL,
  dt_created timestamp without time zone,
  dt_delivered timestamp without time zone,
  metric BIGINT,
  obs character varying(2000),
  CONSTRAINT pk_cluster_config PRIMARY KEY(id)
);


-- ----------------------------------------------------------------------------
-- \echo 'bm_cluster_status ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_cluster_status (
  id smallint NOT NULL,
  cluster_status character varying(30) NOT NULL,
  description character varying(100),
  CONSTRAINT pk_cluster_status PRIMARY KEY(id)
);

ALTER TABLE bm_cluster_status ADD CONSTRAINT ak_cluster_status UNIQUE (cluster_status);



-- ----------------------------------------------------------------------------
-- \echo 'bm_cluster_environment_type ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_cluster_environment_type (
  id smallint NOT NULL,
  cluster_environment_type character varying(30) NOT NULL,
  description character varying(100),
  CONSTRAINT pk_cluster_environment_type PRIMARY KEY(id)
);

ALTER TABLE bm_cluster_environment_type ADD CONSTRAINT ak_cluster_environment_type UNIQUE (cluster_environment_type);


-- ----------------------------------------------------------------------------
-- \echo 'bm_cluster_content_type ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_cluster_content_type (
  id smallint NOT NULL,
  cluster_content_type character varying(30) NOT NULL,
  description character varying(100),
  CONSTRAINT pk_cluster_content_type PRIMARY KEY(id)
);

ALTER TABLE bm_cluster_content_type ADD CONSTRAINT ak_cluster_content_type UNIQUE (cluster_content_type);
