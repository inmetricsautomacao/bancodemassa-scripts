﻿-- ----------------------------------------------------------------------------
-- Filename:
--              undeploy_99_drop_all_objects.sql
-- Description:
--              
-- Log-changes:
--              * 2017-06-17 [JosemarSilva] 
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

--
-- \echo 'List all objects ...'
--
--\df
--\d

--
-- \echo 'Drop functions ...'
--
DROP FUNCTION fx_available_node        (pjsonmsg json);

--
-- \echo 'Drop tables ...'
--
DROP TABLE bm_cluster_config               CASCADE;
DROP TABLE bm_cluster_content_type         CASCADE;
DROP TABLE bm_cluster_environment_type     CASCADE;
DROP TABLE bm_cluster_status               CASCADE;

--
-- \echo 'List all remaining objects (should be nothing) ...'
--
--\df
--\d

--
-- \echo 'Fim!'
--
