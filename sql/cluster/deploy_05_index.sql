﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_05_index.sql
-- Description:
--              
-- Log-changes:
--              * 2017-05-01 JosemarSilva 
--              * 2017-03-14 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'create index ...'
-- ----------------------------------------------------------------------------
CREATE INDEX fk_cluster_content_type_cluster_config     ON bm_cluster_config (cluster_content_type_id);
CREATE INDEX fk_cluster_status_cluster_config           ON bm_cluster_config (cluster_status_id);
CREATE INDEX fk_cluster_environment_type_cluster_config ON bm_cluster_config (cluster_environment_type_id);
