﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_01_create_database.sql
-- Description:
--              
-- Log-changes:
--              * 2017-10-24 JosemarSilva Reuzindo nome do database
--              * 2017-03-14 JosemarSilva Develop
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

--
-- -- \echo 'Create database ...'
--
CREATE DATABASE bmcluster;
