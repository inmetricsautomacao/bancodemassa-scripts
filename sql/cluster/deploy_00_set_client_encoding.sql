﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_00_set_client_encoding.sql
-- Description:
--              
-- Log-changes:
--              * 2018-02-04 JosemarSilva Develop
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

--
-- \echo 'Set client_encoding ...'
--
SET CLIENT_ENCODING TO WIN1252;
