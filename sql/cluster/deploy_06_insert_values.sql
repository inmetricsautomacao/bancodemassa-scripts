﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_06_insert_values.sql
-- Description:
--              
-- Log-changes:
--              * 2018-05-05 JosemarSilva AD-HOC-SQL
--              * 2017-03-14 JosemarSilva
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'insert into bm_cluster_status values ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_cluster_status(id, cluster_status, description ) VALUES ( 0, 'DISABLED', 'For historical and statitical reasons' );
INSERT INTO bm_cluster_status(id, cluster_status, description ) VALUES ( 1, 'ENABLED', 'Being used to serve data' );
INSERT INTO bm_cluster_status(id, cluster_status, description ) VALUES ( 2, 'UNDER CONSTRUCTION', 'Being constructed' );


-- ----------------------------------------------------------------------------
-- \echo 'insert into bm_cluster_environment_type values ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_cluster_environment_type(id, cluster_environment_type, description ) VALUES ( 0, 'HML',  'Homologacao');
INSERT INTO bm_cluster_environment_type(id, cluster_environment_type, description ) VALUES ( 1, 'TI',   'Teste Integrado');
INSERT INTO bm_cluster_environment_type(id, cluster_environment_type, description ) VALUES ( 2, 'DEV',  'Desenvolvimento');


-- ----------------------------------------------------------------------------
-- \echo 'insert into bm_cluster_environment_type values ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_cluster_content_type(id, cluster_content_type, description ) VALUES ( 1, 'ETL',            'Extract Tranform Load' );
INSERT INTO bm_cluster_content_type(id, cluster_content_type, description ) VALUES ( 2, 'STAGE-PIPELINE', 'Stage for Pipeline Transfer Area');
INSERT INTO bm_cluster_content_type(id, cluster_content_type, description ) VALUES ( 3, 'AD-HOC-SQL',     'Ad hoc SQL Query');
