﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_07_grant_to.sql
-- Description:
--              
-- Log-changes:
--              * 2018-01-29 JosemarSilva root user discontinued
--              * 2017-04-19 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'GRANT ALL ON ... TO bancodemassa ...'
-- ----------------------------------------------------------------------------

GRANT ALL ON bm_cluster_config           TO bancodemassa;
GRANT ALL ON bm_cluster_config_id_seq    TO bancodemassa;
GRANT ALL ON bm_cluster_environment_type TO bancodemassa;
GRANT ALL ON bm_cluster_status           TO bancodemassa;
GRANT ALL ON bm_cluster_content_type     TO bancodemassa;

