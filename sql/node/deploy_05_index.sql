﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_05_index.sql
-- Description:
--              
-- Log-changes:
--              * 2018-01-29 JosemarSilva bm_node_lookup_value.node_classe_massa_id e  bm_node_lookup_value(node_classe_massa_id) discontinued
--              * 2017-11-29 JosemarSilva bm_node_metrica
--              * 2017-11-26 JosemarSilva bm_node_tipo_classe_massa
--              * 2017-10-24 JosemarSilva add column node_json_key_datatype_id
--              * 2017-07-09 JosemarSilva Index: idx_node_instancia_massa_classe_status
--              * 2017-07-05 JosemarSilva Index: bm_node_lookup_value e bm_node_json_filter
--              * 2017-05-06 JosemarSilva prefixo "node_"
--              * 2017-05-01 JosemarSilva Drop bm_classe_massa.id_tipo_classe_massa
--              * 2017-04-19 JosemarSilva Hashtag can't be AK, must be only index 
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'create index ...'
-- ----------------------------------------------------------------------------
CREATE INDEX fk_node_status_massa_node_classe_massa                ON bm_node_classe_massa(controle_uso_status_massa_id);
CREATE INDEX fk_nodetipoclassemassa_nodeclassemassa                ON bm_node_classe_massa(node_tipo_classe_massa_id);

CREATE INDEX fk_node_classe_massa_node_instancia_massa             ON bm_node_instancia_massa(node_classe_massa_id);
CREATE INDEX fk_node_status_massa_node_instancia_massa             ON bm_node_instancia_massa(node_status_massa_id);
CREATE INDEX idx_node_instancia_massa_classe_status                ON bm_node_instancia_massa(node_classe_massa_id,node_status_massa_id);

--CREATE INDEX fk_nodeclassemassa_nodelookupvalue                    ON bm_node_lookup_value(node_classe_massa_id);

CREATE INDEX fk_node_classe_massa_node_json_filter                 ON bm_node_json_filter(node_classe_massa_id);
CREATE INDEX fk_node_json_key_datatype_node_json_filter            ON bm_node_json_filter(node_json_key_datatype_id);

CREATE INDEX idx_nodemetrica_01                                    ON bm_node_metrica(datahora);
CREATE INDEX fk_nodeclassemassa_nodemetrica                        ON bm_node_metrica(node_classe_massa_id);

