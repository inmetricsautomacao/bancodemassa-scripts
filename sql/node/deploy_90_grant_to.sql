﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_90_grant_to.sql
-- Description:
--              
-- Log-changes:
--              * 2017-11-29 JosemarSilva bm_node_metrica
--              * 2017-09-26 JosemarSilva separando objetos _stage
--              * 2017-06-25 JosemarSilva depreacting vw_stage_lookup_values, stage_config_reserva_id_seq, vw_node_json_filter, vw_node_lookup_value
--				* 2017-06-19 Aliomar Junior incluido o GRANT to para a tabela stage_tipo_pagamento_cliente para bancodemassa e root
--              * 2017-05-01 JosemarSilva DROP bm_tipo_classe_massa
--              * 2017-04-19 JosemarSilva GRANT TO bancodemassa
--              * 2017-04-17 JosemarSilva stage_lookup_value
--              * 2017-04-03 JosemarSilva creating document
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
---- \echo 'GRANT ALL ON ... TO root ...'
-- ----------------------------------------------------------------------------
--GRANT ALL ON bm_node_classe_massa                                            TO root;
--GRANT ALL ON bm_node_classe_massa_id_seq                                     TO root;
--GRANT ALL ON bm_node_instancia_massa                                         TO root;
--GRANT ALL ON bm_node_instancia_massa_id_seq                                  TO root;
--GRANT ALL ON bm_node_status_massa                                            TO root;
--GRANT ALL ON bm_node_log_falha                                               TO root;
--GRANT ALL ON bm_node_log_falha_id_seq                                        TO root;
--GRANT ALL ON bm_node_lookup_value                                            TO root;
--GRANT ALL ON bm_node_lookup_value_id_seq                                     TO root;
--GRANT ALL ON bm_node_json_filter                                             TO root;
--GRANT ALL ON bm_node_json_filter_id_seq                                      TO root;
--GRANT ALL ON bm_node_json_key_datatype                                       TO root;
--GRANT ALL ON bm_node_tipo_classe_massa                                       TO root;
--GRANT ALL ON bm_node_metrica                                                 TO root;
--GRANT ALL ON vw_node_metric_instancia_massa                                  TO root;
--GRANT ALL ON vw_node_json_filter                                             TO root;
--GRANT ALL ON vw_node_lookup_value                                            TO root;
--GRANT ALL ON vw_node_json_filter                                             TO root;



-- ----------------------------------------------------------------------------
-- \echo 'GRANT ALL ON ... TO bancodemassa ...'
-- ----------------------------------------------------------------------------
GRANT ALL ON bm_node_classe_massa                                            TO bancodemassa;
GRANT ALL ON bm_node_classe_massa_id_seq                                     TO bancodemassa;
GRANT ALL ON bm_node_instancia_massa                                         TO bancodemassa;
GRANT ALL ON bm_node_instancia_massa_id_seq                                  TO bancodemassa;
GRANT ALL ON bm_node_status_massa                                            TO bancodemassa;
GRANT ALL ON bm_node_log_falha                                               TO bancodemassa;
GRANT ALL ON bm_node_log_falha_id_seq                                        TO bancodemassa;
GRANT ALL ON bm_node_lookup_value                                            TO bancodemassa;
GRANT ALL ON bm_node_lookup_value_id_seq                                     TO bancodemassa;
GRANT ALL ON bm_node_json_filter                                             TO bancodemassa;
GRANT ALL ON bm_node_json_filter_id_seq                                      TO bancodemassa;
GRANT ALL ON bm_node_json_key_datatype                                       TO bancodemassa;
GRANT ALL ON bm_node_metrica                                                 TO bancodemassa;
--
GRANT ALL ON vw_node_metric_instancia_massa                                  TO bancodemassa;
GRANT ALL ON vw_node_json_filter                                             TO bancodemassa;
GRANT ALL ON vw_node_json_filter                                             TO bancodemassa;
--
GRANT ALL ON bm_cluster_config                                               TO bancodemassa;
GRANT ALL ON bm_cluster_status                                               TO bancodemassa;
GRANT ALL ON bm_cluster_environment_type                                     TO bancodemassa;
GRANT ALL ON bm_cluster_content_type                                         TO bancodemassa;
GRANT ALL ON bm_config_tipo_reserva                                          TO bancodemassa;
GRANT ALL ON bm_config_tipo_ambiente                                         TO bancodemassa;
GRANT ALL ON bm_config_classe_massa                                          TO bancodemassa;


-- ----------------------------------------------------------------------------
-- \echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
