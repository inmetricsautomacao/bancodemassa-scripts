﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_06_insert_values.sql
-- Description:
--              
-- Log-changes:
--              * 2018-05-05 JosemarSilva bm_node_tipo_classe_massa ( 4, 'Ad hoc SQL' )
--              * 2017-07-08 JosemarSilva 'Reservado Uso Código Reserva'
--              * 2017-05-01 JosemarSilva Drop bm_tipo_classe_massa
--              * 2017-03-14 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'insert into  ...'
-- ----------------------------------------------------------------------------

INSERT INTO bm_node_tipo_classe_massa ( id, tipo_classe_massa ) VALUES ( 1, 'Classe de Massa - Base' );
INSERT INTO bm_node_tipo_classe_massa ( id, tipo_classe_massa ) VALUES ( 2, 'Subclasse - Criterio Condicao' );
INSERT INTO bm_node_tipo_classe_massa ( id, tipo_classe_massa ) VALUES ( 3, 'Classe Massa - Pipeline Stage' );
INSERT INTO bm_node_tipo_classe_massa ( id, tipo_classe_massa ) VALUES ( 4, 'Ad hoc SQL' );

INSERT INTO bm_node_status_massa(id, status_massa, ind_disponibilidade ) VALUES ( 0, 'Disponível para Uso',          1 );
INSERT INTO bm_node_status_massa(id, status_massa, ind_disponibilidade ) VALUES ( 1, 'Disponível para Reuso',        1 );
INSERT INTO bm_node_status_massa(id, status_massa, ind_disponibilidade ) VALUES ( 2, 'Indisponível para Reuso',      0 );
INSERT INTO bm_node_status_massa(id, status_massa, ind_disponibilidade ) VALUES ( 3, 'Indisponível Config. Reserva', 0 );
INSERT INTO bm_node_status_massa(id, status_massa, ind_disponibilidade ) VALUES ( 4, 'Reservado Uso restrito "Código Reserva"', 0 );

INSERT INTO bm_node_json_key_datatype( id, json_key_datatype, obs ) VALUES ( 1, 'Number', 'Number: double- precision floating-point format in JavaScript' );
INSERT INTO bm_node_json_key_datatype( id, json_key_datatype, obs ) VALUES ( 2, 'String', 'String: double-quoted Unicode with backslash escaping' );
INSERT INTO bm_node_json_key_datatype( id, json_key_datatype, obs ) VALUES ( 3, 'Boolean', 'Boolean: true or false' );
INSERT INTO bm_node_json_key_datatype( id, json_key_datatype, obs ) VALUES ( 4, 'Array', 'Array: an ordered sequence of values' );
INSERT INTO bm_node_json_key_datatype( id, json_key_datatype, obs ) VALUES ( 5, 'Value', 'Value: it can be a string, a number, true or false, null etc' );
INSERT INTO bm_node_json_key_datatype( id, json_key_datatype, obs ) VALUES ( 6, 'Object', 'Object: an unordered collection of key:value pairs' );

-- ----------------------------------------------------------------------------
-- \echo 'Fim do script!'
-- ----------------------------------------------------------------------------
