﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_02_insert_values_stage.sql
-- Description:
--              
-- Log-changes:
--              * 2018-05-05 JosemarSilva bm_node_classe_massa ( 'AD-HOC-SQL.ORACLE.CURRENT-CONFIG', 'AD-HOC-SQL.POSTGRESQL.CURRENT-CONFIG' )
--              * 2017-09-26 JosemarSilva separando objetos _stage da demo
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
-- \echo 'Início ...'
-- ----------------------------------------------------------------------------

INSERT INTO bm_node_classe_massa( node_tipo_classe_massa_id, classe_massa, descricao, controle_uso_status_massa_id, json_config_classe_massa )
SELECT CAST(cm.json_config->'config'->>'tipo_classe_massa_id' AS INTEGER) node_tipo_classe_massa_id, 
       cm.classe_massa, 
       cm.obs AS descricao, 
       0 /* 0: Disponivel para Uso */ AS controle_uso_status_massa_id, 
       cm.json_config json_config_classe_massa
FROM   bm_config_classe_massa cm
INNER  JOIN bm_node_tipo_classe_massa tcm
ON     CAST(tcm.id AS VARCHAR) = cm.json_config->'config'->>'tipo_classe_massa_id'
WHERE  cm.classe_massa NOT IN
       (
         SELECT classe_massa
         FROM   bm_node_classe_massa
       )
;




-- ----------------------------------------------------------------------------
-- \echo 'Fim do script!'
-- ----------------------------------------------------------------------------
