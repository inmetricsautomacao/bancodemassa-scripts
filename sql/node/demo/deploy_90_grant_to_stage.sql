﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_90_grant_to.sql
-- Description:
--              
-- Log-changes:
--              * 2017-09-26 JosemarSilva separando objetos _stage da cielo
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
-- \echo 'GRANT ALL ON ... TO root ...'
-- ----------------------------------------------------------------------------
--GRANT ALL ON stage_redmine_issue                                             TO root;
--GRANT ALL ON stage_redmine_issue_id_seq                                      TO root;




-- ----------------------------------------------------------------------------
-- \echo 'GRANT ALL ON ... TO bancodemassa ...'
-- ----------------------------------------------------------------------------
GRANT ALL ON stage_redmine_issue                                             TO bancodemassa;
GRANT ALL ON stage_redmine_issue_id_seq                                      TO bancodemassa;
-- foreign tables
GRANT ALL ON bm_config_cliente                                               TO bancodemassa;
GRANT ALL ON bm_config_login_sistema                                         TO bancodemassa;


-- ----------------------------------------------------------------------------
-- \echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
