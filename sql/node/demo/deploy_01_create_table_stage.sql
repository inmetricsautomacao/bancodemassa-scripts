﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_01_create_table_stage.sql
-- Description:
--              
-- Log-changes:
--              * 2017-09-27 JosemarSilva config_tipo_reserva_id e cod_reserva
--              * 2017-09-26 JosemarSilva separando objetos _stage da demo
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
-- \echo 'stage_redmine_issue ...'
-- ----------------------------------------------------------------------------
CREATE TABLE stage_redmine_issue(
  id SERIAL NOT NULL,
  config_tipo_reserva_id SMALLINT NULL,
  cod_reserva   VARCHAR(100) NULL,
  issue_id      INTEGER,
  subject       VARCHAR(255) NOT NULL,
  created_date  TIMESTAMP,
  due_date      TIMESTAMP,
  status_id     SMALLINT     NOT NULL,
  status_name   VARCHAR(30)  NOT NULL,
  category_id   SMALLINT, 
  category_name VARCHAR(255),
  login         VARCHAR(255),
  complete_name VARCHAR(500),
  description   VARCHAR(2000),
  CONSTRAINT pk_stage_redmine_issue PRIMARY KEY(id)
);



-- ----------------------------------------------------------------------------
-- \echo 'Fim do script!'
-- ----------------------------------------------------------------------------
