SELECT i.id issue_id, 
       i.subject, 
       i.created_on AS created_date,
       i.due_date, 
       i.status_id,
       s.name AS status_name,
       i.category_id, 
       ic.name AS category_name,
       u.login,
       CONCAT( u.firstname,  ' ', u.lastname) AS complete_name,
       i.description
FROM   issues i
INNER  JOIN projects p
ON     p.id = i.project_id
INNER  JOIN issue_statuses s
ON     s.id = i.status_id
INNER  JOIN users u
ON     u.id = i.author_id
LEFT   OUTER JOIN issue_categories ic
ON     ic.id = i.category_id
WHERE  i.project_id = 174 /* 174 = 'Cantina' */
