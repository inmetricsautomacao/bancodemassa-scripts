﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_04_foreigntable.sql
-- Description:
--              
-- Log-changes:
--              * 2017-11-30 JosemarSilva Desenvolvimento
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
-- \echo 'bm_config_cliente ...'
-- ----------------------------------------------------------------------------

CREATE FOREIGN TABLE bm_config_cliente (
  id INTEGER NOT NULL,
  config_tipo_ambiente_id SMALLINT NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  cod_reserva VARCHAR(100) NULL,
  tags  VARCHAR(100) NULL,
  datahora_ini_reserva TIMESTAMP NULL,
  datahora_fim_reserva TIMESTAMP NULL,
  solicitante_reserva VARCHAR(100) NULL,
  obs VARCHAR(100),
  --
  chave_num BIGINT NULL,
  chave_str VARCHAR(30) NULL,
  --
  nome_cliente VARCHAR(100),
  cnpj_cpf bigint NULL
)
SERVER postgres_fdw_config_server;

-- ----------------------------------------------------------------------------
-- \echo 'bm_config_login_sistema ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_config_login_sistema (
  id INTEGER NOT NULL,
  config_tipo_ambiente_id SMALLINT NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  sistema VARCHAR(30) NOT NULL,
  login_name VARCHAR(30) NOT NULL,
  login_password VARCHAR(30) NOT NULL,
  role_name VARCHAR(30) NULL,
  cod_reserva VARCHAR(100) NULL,
  tags  VARCHAR(100) NULL,
  obs VARCHAR(1000) NULL
)
SERVER postgres_fdw_config_server;


-- ----------------------------------------------------------------------------
-- \echo 'bm_config_ ...'
-- ----------------------------------------------------------------------------
