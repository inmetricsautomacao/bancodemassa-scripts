﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_03_function_stage.sql
-- Description:
--
-- Log-changes:
--              & 2017-09-27 JosemarSilva fx_node_load_redmine_issue()
--              * 2017-09-26 JosemarSilva separando objetos _stage
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_load_config_login( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_config_login( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_load_config_login$
DECLARE
  vClasseMassaId INTEGER;
  vClasseMassa   VARCHAR(200);
  vCount         INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_load_config_login() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  -- Busca/Cria id da classe 'ETL.LOGIN' ...
  --
  vClasseMassaId := NULL;
  vClasseMassa   := 'ETL.LOGIN';
  --
  BEGIN
    INSERT INTO bm_node_classe_massa ( node_tipo_classe_massa_id, classe_massa, descricao, controle_uso_status_massa_id )
    VALUES ( 1 /* 1: Classe Base */, vClasseMassa, vClasseMassa, 1 /* 1: Disponivel para Reuso*/ );
  EXCEPTION
    WHEN unique_violation THEN
      NULL; -- OK!
  END;
  --
  SELECT id  
  INTO   vClasseMassaId 
  FROM   bm_node_classe_massa 
  WHERE  classe_massa = vClasseMassa;
  --
  RAISE info '    b. Clean-up tables: bm_instancia_massa (%) ...', vClasseMassa;
  --
  DELETE FROM bm_node_instancia_massa WHERE node_classe_massa_id = vClasseMassaId;
  --
  RAISE info '    c. Transforming Stage: n/a ...';
  --
  RAISE info '    d. Load into bm_node_instancia_massa (ETL.LOGIN)...';
  --
  INSERT INTO bm_node_instancia_massa 
  ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
  SELECT CASE
           WHEN ( stage.cod_reserva IS NULL OR stage.cod_reserva <> '' ) THEN 
                   0 /* 0: Disponível para Uso */
           WHEN ( stage.cod_reserva IS NOT NULL AND stage.cod_reserva <> '' ) THEN 
                   4 /* 4: Reservado Uso restrito "Código Reserva" */
           ELSE
                   3 /* 3: Indisponível Config. Reserva	 */
         END node_status_massa_id,
         vClasseMassaId AS node_classe_massa_id,
         json_build_object
         (
           'tags',    json_build_object
                      (
                        'classe_massa',         'ETL.LOGIN',
                        'cod_reserva',          CASE
                                                  WHEN ( stage.cod_reserva IS NOT NULL AND stage.cod_reserva <> '' ) THEN 
                                                        stage.cod_reserva
                                                  ELSE
                                                        null
                                                END
                      ),
           'massa', json_build_object
                      (
                        'cod_tipo_ambiente', stage.cod_tipo_ambiente,
                        'sistema',           stage.sistema,
                        'login_name',        stage.login_name,
                        'login_password',    stage.login_password,
                        'obs',               stage.obs
                      )
         ) AS json_massa,
         current_timestamp AS datahora_carga
  FROM   (
           SELECT ls.config_tipo_reserva_id,
                  ls.config_tipo_ambiente_id,
                  ta.cod_tipo_ambiente,
                  ls.sistema,
                  ls.login_name,
                  ls.login_password,
                  ls.cod_reserva,
                  ls.tags,
                  ls.obs
           FROM   bm_config_login_sistema ls
           INNER  JOIN bm_config_tipo_ambiente ta
           ON     ta.id = ls.config_tipo_ambiente_id
           WHERE  ta.cod_tipo_ambiente = 'HML'
           ) AS stage
  WHERE  1=1;
  --
  RAISE info '<<< fx_node_load_config_login() <<<';
  --
END;
$fx_node_load_config_login$ 
LANGUAGE plpgsql ;


-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_load_redmine_issue( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_redmine_issue( pAmbiente VARCHAR DEFAULT '') 
RETURNS void AS
$fx_node_load_redmine_issue$
DECLARE
  vClasseMassaId INTEGER;
  vClasseMassa   VARCHAR(200);
  vCount         INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_load_redmine_issue() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  -- Busca/Cria id da classe 'REDMINE.ISSUE' ...
  --
  vClasseMassaId := NULL;
  vClasseMassa   := 'REDMINE.ISSUE';
  --
  BEGIN
    INSERT INTO bm_node_classe_massa ( classe_massa, descricao, controle_uso_status_massa_id )
    VALUES ( vClasseMassa, vClasseMassa, 2 /* 1: Disponível para Reuso*/ );
  EXCEPTION
    WHEN unique_violation THEN
      NULL; -- OK!
  END;
  --
  SELECT id  
  INTO   vClasseMassaId 
  FROM   bm_node_classe_massa 
  WHERE  classe_massa = vClasseMassa;
  --
  RAISE info '    b. Clean-up tables: bm_instancia_massa ...';
  --
  DELETE FROM bm_node_instancia_massa WHERE node_classe_massa_id = vClasseMassaId;
  --
  RAISE info '    c. Transforming Stage: cod_reserva e status_massa ...';
  --
  UPDATE stage_redmine_issue
  SET    cod_reserva = 'INMETRICS_DAY'
  WHERE  issue_id IN
         (
           SELECT issue_id
           FROM   (
                    SELECT status_id, max(issue_id) AS issue_id
                    FROM   stage_redmine_issue
                    GROUP  BY status_id
                  ) a
         );
  --
  UPDATE stage_redmine_issue
  SET    config_tipo_reserva_id = 1;
  --
  RAISE info '    d. Load into bm_node_instancia_massa ...';
  --
  INSERT INTO bm_node_instancia_massa 
    ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
  SELECT CASE WHEN a.config_tipo_reserva_id = 1 /* 1: Disponível p/ Banco de Massa */ THEN
                   0 /* 0: Disponível para Uso */
              WHEN a.config_tipo_reserva_id = 2 /* 2: Reservado p/ uso Restrito */ THEN
                   4 /* 4: Reservado Uso restrito "Código Reserva" */
              ELSE 
                   3 /* 3: Indisponível Config. Reserva	 */
         END AS node_status_massa_id,
         vClasseMassaId AS node_classe_massa_id,
         CONCAT('{"tags":', 
                  CONCAT('{"classe_massa":"', vClasseMassa, '"'
                        ,CASE WHEN a.cod_reserva IS NOT NULL THEN
                                   ',"cod_reserva": "' || a.cod_reserva || '"'
                           ELSE
                                   ''
                        END
                  )
                  ,'}'
                  ,CONCAT(',"massa":{',
                                  '"issue_id":"'       || a.issue_id      ||'"'
                                  ,',"subject":"'      || a.subject       ||'"'
                                  ,',"created_date":"' || COALESCE(TO_CHAR(a.created_date,'yyyy-mm-dd'),'')
                                                                          ||'"'
                                  ,',"due_date":"'     || COALESCE(TO_CHAR(a.due_date,'yyyy-mm-dd'),'')
                                                                          ||'"'
                                  ,',"status_id":"'    || a.status_id     ||'"'
                                  ,',"status_name":"'  || a.status_name   ||'"'
                                  ,',"category_id":"'  || COALESCE(CAST(a.category_id AS VARCHAR),'')
                                                                          ||'"'
                                  ,',"category_name":"'|| COALESCE(a.category_name,'')
                                                                          ||'"'
                                  ,',"login":"'        || COALESCE(a.login,'')
                                                                          ||'"'
                                  ,',"complete_name":"'|| COALESCE(a.complete_name,'')
                                                                          ||'"'
                                  ,',"description":"'  || COALESCE(a.description,'')
                                                                          ||'"'
                                  ,'}'), 
         '}')::jsonb AS json_massa,
         current_timestamp AS datahora_carga
  FROM   stage_redmine_issue a
  WHERE  1 = 1 ;
  --
  RAISE info '<<< fx_node_load_redmine_issue() <<<';
  --
END;
$fx_node_load_redmine_issue$ 
LANGUAGE plpgsql;



-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_execute_etl( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_execute_etl( pAmbiente VARCHAR DEFAULT 'TI') 
RETURNS void AS
$fx_node_execute_etl$
DECLARE
  vCount INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_execute_etl() >>>';
  --
  PERFORM fx_node_load_redmine_issue();
  --
  RAISE info '<<< fx_node_execute_etl() <<<';
  --  
END;
$fx_node_execute_etl$ 
LANGUAGE plpgsql ;





-- ----------------------------------------------------------------------------
-- \echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
