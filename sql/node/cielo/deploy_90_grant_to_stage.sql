-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_90_grant_to.sql
-- Description:
--              
-- Log-changes:
--              * 2017-09-26 JosemarSilva separando objetos _stage da cielo
--              * 2017-06-25 JosemarSilva depreacting vw_stage_lookup_values, stage_config_reserva_id_seq, vw_node_json_filter, vw_node_lookup_value
--				* 2017-06-19 Aliomar Junior incluido o GRANT to para a tabela stage_tipo_pagamento_cliente para bancodemassa e root
--              * 2017-05-01 JosemarSilva DROP bm_tipo_classe_massa
--              * 2017-04-19 JosemarSilva GRANT TO bancodemassa
--              * 2017-04-17 JosemarSilva stage_lookup_value
--              * 2017-04-03 JosemarSilva creating document
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
\echo 'GRANT ALL ON ... TO root ...'
-- ----------------------------------------------------------------------------
GRANT ALL ON stage_payment_hierarchy_product                                 TO root;
GRANT ALL ON stage_payment_hierarchy_product_id_seq                          TO root;
GRANT ALL ON stage_star_transaction                                          TO root;
GRANT ALL ON stage_star_transaction_id_seq                                   TO root;
GRANT ALL ON stage_simuterm_lote_transacao                                   TO root;
GRANT ALL ON stage_tipo_pagamento_cliente                                    TO root;
GRANT ALL ON stage_tipo_pagamento_cliente_id_seq                             TO root;
GRANT ALL ON stage_bm_config_reserva_nu_customer                             TO root;
GRANT ALL ON stage_star_tbctmr_merchant                                      TO root;
GRANT ALL ON stage_star_tbctmr_merchant_bank                                 TO root;
GRANT ALL ON stage_star_tbctmr_config_pmt_group                              TO root;
GRANT ALL ON stage_star_tbctmr_merchant_bank_type                            TO root;
GRANT ALL ON stage_star_tbctmr_status_activity                               TO root;
GRANT ALL ON stage_star_tbhrqr_hierarchical_node                             TO root;
GRANT ALL ON stage_star_tbhrqr_merchant_node                                 TO root;
GRANT ALL ON stage_star_tbhrqr_node_type                                     TO root;
GRANT ALL ON stage_star_tbprdr_product                                       TO root;
GRANT ALL ON stage_star_tbprdr_card_association                              TO root;
GRANT ALL ON stage_star_tbprdr_payment_method                                TO root;
GRANT ALL ON stage_star_tbprdr_payment_category                              TO root;
GRANT ALL ON stage_star_tbctmr_acquirer                                      TO root;
GRANT ALL ON stage_star_tbprdr_contract_card_assoc                           TO root;
GRANT ALL ON stage_star_tbsetr_adjustment                                    TO root;
GRANT ALL ON stage_star_tbsetr_settlement_transaction                        TO root;
GRANT ALL ON stage_star_tbctmr_config_pmt_merchant                           TO root;
GRANT ALL ON stage_star_tbhrqr_financial_chain_node                          TO root;
GRANT ALL ON stage_star_tbhrqr_root_cnpj_node                                TO root;
GRANT ALL ON stage_star_tbhrqr_relationship_hrql_node                        TO root;
GRANT ALL ON stage_star_tbhrqr_management_hierar_node                        TO root;
GRANT ALL ON stage_star_tbhrqr_hierarchy                                     TO root;
GRANT ALL ON stage_star_tbctmr_terminal_type                                 TO root;
GRANT ALL ON stage_star_tbctmr_logic_terminal                                TO root;
GRANT ALL ON stage_star_tbctmr_geogr_address                                 TO root;
GRANT ALL ON stage_star_tbctmr_host_bank                                     TO root;



-- ----------------------------------------------------------------------------
\echo 'GRANT ALL ON ... TO bancodemassa ...'
-- ----------------------------------------------------------------------------
GRANT ALL ON stage_simutermweb_lote_transacao                                TO bancodemassa;
GRANT ALL ON stage_simutermweb_lote_transacao_id_seq                         TO bancodemassa;



-- ----------------------------------------------------------------------------
\echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
