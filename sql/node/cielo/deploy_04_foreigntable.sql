-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_04_foreigntable.sql
-- Description:
--              
-- Log-changes:
--              * 2017-11-30 JosemarSilva Desenvolvimento
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
\echo 'bm_config_merchant ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_config_merchant (
  id INTEGER NOT NULL,
  config_tipo_ambiente_id SMALLINT NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  nu_customer BIGINT NOT NULL,
  cod_reserva VARCHAR(100) NULL,
  nu_root_cnpj_cpf BIGINT NULL,
  nu_branch_cnpj INTEGER NULL,
  nu_control_cnpj INTEGER NULL,
  tags  VARCHAR(100) NULL,
  datahora_ini_reserva TIMESTAMP NULL,
  datahora_fim_reserva TIMESTAMP NULL,
  solicitante_reserva VARCHAR(100) NULL,
  obs VARCHAR(100)
)
SERVER postgres_fdw_config_server;

-- ----------------------------------------------------------------------------
\echo 'bm_config_login_sistema ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_config_login_sistema (
  id INTEGER NOT NULL,
  config_tipo_ambiente_id SMALLINT NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  sistema VARCHAR(30) NOT NULL,
  login_name VARCHAR(30) NOT NULL,
  login_password VARCHAR(30) NOT NULL,
  role_name VARCHAR(30) NULL,
  cod_reserva VARCHAR(100) NULL,
  tags  VARCHAR(100) NULL,
  obs VARCHAR(1000) NULL
)
SERVER postgres_fdw_config_server;


-- ----------------------------------------------------------------------------
\echo 'bm_config_capirdmdr ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_config_capirdmdr (
  id SERIAL NOT NULL,
  config_tipo_ambiente_id INTEGER NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  test_case_id INTEGER NOT NULL,
  test_plan_id INTEGER NOT NULL,
  test_case_name VARCHAR(250) NOT NULL,
  cycle_folder_path_name VARCHAR(250) NOT NULL,
  dias_anterior SMALLINT NOT NULL,
  nome_projeto VARCHAR(30) NOT NULL,
  nome_lote VARCHAR(100) NOT NULL,
  flag_enable SMALLINT NOT NULL,
  flag_check_status_alm SMALLINT NOT NULL,
  canal_venda VARCHAR(30) NULL,
  bandeira VARCHAR(30) NULL,
  tags VARCHAR(200) NULL,
  tipo_capirdmdr VARCHAR(30) NULL
)
SERVER postgres_fdw_config_server;


-- ----------------------------------------------------------------------------
\echo 'bm_config_capirdmdr ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_config_param_capirdmdr (
  id SERIAL NOT NULL,
  config_capirdmdr_id INTEGER NOT NULL,
  chave VARCHAR(100) NOT NULL,
  operador VARCHAR(30) NOT NULL,
  valor_esperado VARCHAR(100) NOT NULL,
  valor_capturado VARCHAR(100) NULL
)
SERVER postgres_fdw_config_server;


-- ----------------------------------------------------------------------------
\echo 'bm_config_ ...'
-- ----------------------------------------------------------------------------
