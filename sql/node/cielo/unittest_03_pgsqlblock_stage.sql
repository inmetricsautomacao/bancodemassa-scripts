-- ----------------------------------------------------------------------------
-- Filename:
--              unittest_03_pgsqlblock_stage.sql
-- Description:
--              
-- Log-changes:a
--              * 2017-09-26 JosemarSilva Develop Unit Test Postgresql
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
\echo 'Início ...'
-- ----------------------------------------------------------------------------

DO $$ 
<<first_block>>
DECLARE
  bException BOOLEAN := FALSE;
  vMsgException VARCHAR := '';  -- SQLERRM, SQLSTATE
  vEvidencia    VARCHAR := '';
  vStatus       VARCHAR := '';
  vJsonResult   JSON;
BEGIN
  --
  BEGIN
    SELECT fx_node_test_data_get( ( '{"get": {"classe_massa": "ETL.EC"} }' )::json ) INTO vJsonResult;
  EXCEPTION
    WHEN OTHERS THEN
      bException := TRUE;
	  vMsgException := CONCAT('SQLERRM:', SQLERRM, ' - SQLSTATE ', SQLSTATE);
	  RAISE INFO '%', vMsgException;
  END;
  --
  IF NOT bException THEN 
    vStatus := 'Passed' ;
	vEvidencia := vJsonResult;
	IF LENGTH(vEvidencia) > 500 THEN
      vEvidencia := SUBSTR(vEvidencia,1,500) || ' ... ' || SUBSTR(vEvidencia,LENGTH(vEvidencia)-200,200);
	END IF;
	--
	IF vJsonResult->'get_return'->>'status_code' <> 1::VARCHAR THEN
      vStatus := 'Failed - "status_code <> 1" - {"get_return": "status_code": ... }' ;
	END IF;
	IF vJsonResult->'get_return'->'data' IS NULL THEN
      vStatus := 'Failed - "data" - {"get_return": "data": ... }' ;
	END IF;
  ELSE 
    vStatus := 'Failed' ;
  END IF ;
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO 'Funcionalidade: Objetivo testar a mecanica basica das chamadas API para o Banco de Massa';
  RAISE INFO '  Cenario: Testar a chamada a stored function delivery com uma massa "ETL.EC"';
  RAISE INFO '    Dado: Um json de GET de uma massa "ETL.EC"';
  RAISE INFO '    Quando: Eu fizer uma chamada a stored function delivery com um json de GET de uma massa "ETL.EC"';
  RAISE INFO '    Entao: Eu obtenho um json de retorno com codigo de status SUCESSO';
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '';
  RAISE INFO '    Status: %', vStatus;
  RAISE INFO '    Evidencia: %', vEvidencia;
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '';
END first_block $$;


-- ----------------------------------------------------------------------------
\echo 'Fim do script!'
-- ----------------------------------------------------------------------------
