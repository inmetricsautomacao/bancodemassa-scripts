-- ----------------------------------------------------------------------------
-- Filename:
--              undeploy_99_drop_all_objects.sql
-- Description:
--              
-- Log-changes:
--              * 2017-08-08 AliomarJunior stage_mainframe_redes_g126, fx_node_load_ec_canal_redes, fx_node_load_ec_config_redes, fx_node_ec_redes.
--              * 2017-08-02 JosemarSilva stage_mainframe_redes_g124, stage_mainframe_redes_g125
--              * 2017-07-20 JosemarSilva fx_node_load_ec_config() -> fx_node_load_ec_config_star()
--              * 2017-07-12 JosemarSilva fx_node_test_data_post()
--              * 2017-07-11 JosemarSilva Ren fx_node_mass_delivery() -> fx_node_test_data_get
--              * 2017-07-07 JosemarSilva fx_node_create_indexes(), fx_node_drop_indexes(), 
--                           fx_node_load_json_filter(), drop stage_tipo_pagamento_cliente
--              * 2017-06-25 JosemarSilva Drop fx_node_load_transac_simuterm(), deprecate stage_config_reserva
--				* 2017-06-19 Aliomar Junior incluido o DROP para a TABLE stage_tipo_pagamento_cliente
--				* 2017-06-19 Aliomar Junior incluido o DROP para a function fx_node_load_tipo_pagamento_star
--              * 2017-06-18 JosemarSilva Drop foreign table
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

--
\echo 'List all objects ...'
--
\df
\d

--
\echo 'Drop functions ...'
--
DROP FUNCTION fx_load_ec_config                (pambiente character varying);
DROP FUNCTION fx_mass_delivery_node            (pjsonmsg json);
DROP FUNCTION fx_node_execute_etl              (pambiente character varying);
DROP FUNCTION fx_node_load_ec_config_star      (pambiente character varying);
DROP FUNCTION fx_node_load_transac_star        (pambiente character varying);
DROP FUNCTION fx_node_load_tipo_pagamento_star (pambiente character varying);
DROP FUNCTION fx_node_load_transac_simuterm    (pambiente character varying);
DROP FUNCTION fx_node_test_data_get       (pjsonmsg json);
DROP FUNCTION fx_node_load_lookup_value        ();
DROP FUNCTION fx_node_create_indexes           ();
DROP FUNCTION fx_node_drop_indexes             ();
DROP FUNCTION fx_node_load_json_filter         ();
DROP FUNCTION fx_node_transform_stage          ();
DROP FUNCTION fx_node_test_data_post         (pjsonmsg json);
DROP FUNCTION fx_node_load_ec_redes            (pambiente character varying);
DROP FUNCTION fx_node_load_ec_config_redes     (pambiente character varying);
DROP FUNCTION fx_node_load_ec_canal_redes      (pambiente character varying);
DROP FUNCTION fx_node_load_ec_convivencia      (pambiente character varying);
DROP FUNCTION fx_node_load_ec_sec              (pambiente character varying);
DROP FUNCTION fx_node_load_ec_config_sec       (pambiente character varying);

--
\echo 'Drop views ...'
--
DROP VIEW  vw_node_metric_instancia_massa                                CASCADE;
DROP VIEW  vw_node_json_filter                                           CASCADE;
DROP VIEW  vw_node_lookup_value                                          CASCADE;
DROP VIEW  vw_node_lookup_table                                          CASCADE;
DROP VIEW  vw_node_json_filter                                           CASCADE;

--
\echo 'Drop tables ...'
--
DROP TABLE bm_node_classe_massa            CASCADE;
DROP TABLE bm_node_instancia_massa         CASCADE;
DROP TABLE bm_node_status_massa            CASCADE;
DROP TABLE bm_node_log_falha               CASCADE;
DROP TABLE bm_node_lookup_value            CASCADE;
DROP TABLE bm_node_json_filter             CASCADE;
DROP TABLE bm_node_json_key_datatype       CASCADE;
--
DROP TABLE stage_payment_hierarchy_product CASCADE;
DROP TABLE stage_simuterm_lote_transacao   CASCADE;
DROP TABLE stage_star_transaction          CASCADE;
DROP TABLE stage_tipo_pagamento_cliente    CASCADE;
DROP TABLE stage_mainframe_sec_at01        CASCADE;
DROP TABLE stage_mainframe_sec_at02        CASCADE;
DROP TABLE stage_mainframe_redes_g124      CASCADE;
DROP TABLE stage_mainframe_redes_g125      CASCADE;
DROP TABLE stage_mainframe_redes_g126      CASCADE;

--
\echo 'Drop foreign tables ...'
--
DROP FOREIGN TABLE bm_cluster_config;
DROP FOREIGN TABLE bm_cluster_content_type;
DROP FOREIGN TABLE bm_cluster_environment_type;
DROP FOREIGN TABLE bm_cluster_status;


--
\echo 'Drop Extension, Server, User Mapping ...'
--
DROP USER MAPPING FOR root SERVER postgres_fdw_cluster_server;
DROP SERVER    postgres_fdw_cluster_server CASCADE;
DROP EXTENSION postgres_fdw CASCADE;


--
\echo 'List all remaining objects (should be nothing) ...'
--
\df
\d



--
\echo 'Fim!'
--
