﻿-- ----------------------------------------------------------------------------
-- Filename:
--              unittest_17_pgsqlblock_stage.sql
-- Description:
--              
-- Log-changes:
--              * 2017-12-06 JosemarSilva Develop Unit Test Postgresql
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
\echo 'Início ...'
-- ----------------------------------------------------------------------------

DO $$ 
<<first_block>>
DECLARE
  bException BOOLEAN := FALSE;
  vMsgException VARCHAR := '';  -- SQLERRM, SQLSTATE
  vEvidencia    VARCHAR := '';
  vStatus       VARCHAR := '';
  vId           VARCHAR := '';
  vmd5          VARCHAR := '';
  vJsonResult   JSON;
BEGIN
  --
  -- POST
  --
  BEGIN
    SELECT fx_node_test_data_post( ( '{"post": { "tags": {"classe_massa": "PIPELINE.UNITTEST"}, "massa":{ "key#1":"value#1", "key#2": "value#2", "key#3": "value#3"} } }' )::json ) INTO vJsonResult;
  EXCEPTION
    WHEN OTHERS THEN
      bException := TRUE;
      vMsgException := CONCAT('SQLERRM:', SQLERRM, ' - SQLSTATE ', SQLSTATE);
      RAISE INFO '%', vMsgException;
  END;
  --
  IF NOT bException THEN 
    vStatus := 'Passed' ;
    vEvidencia := vJsonResult;
    IF LENGTH(vEvidencia) > 500 THEN
      vEvidencia := SUBSTR(vEvidencia,1,500) || ' ... ' || SUBSTR(vEvidencia,LENGTH(vEvidencia)-200,200);
    END IF;
    --
    IF vJsonResult->'post_return'->>'status_code' <> '1' THEN
      vStatus := 'Failed - "status_code <> 1" - {"post_return": "status_code": ... }' ;
    END IF;
    --
    IF NOT bException THEN 
      vStatus := 'Passed' ;
      --
      -- GET
      --
      BEGIN
        SELECT fx_node_test_data_get( ( '{"get": { "classe_massa": "PIPELINE.UNITTEST"} }' )::json ) INTO vJsonResult;
      EXCEPTION
        WHEN OTHERS THEN
          bException := TRUE;
            vMsgException := CONCAT('SQLERRM:', SQLERRM, ' - SQLSTATE ', SQLSTATE);
          RAISE INFO '%', vMsgException;
      END;
      --
      IF NOT bException AND vStatus = 'Passed' THEN 
        vStatus := 'Passed' ;
        vEvidencia := vJsonResult;
        IF LENGTH(vEvidencia) > 500 THEN
          vEvidencia := SUBSTR(vEvidencia,1,500) || ' ... ' || SUBSTR(vEvidencia,LENGTH(vEvidencia)-200,200);
        END IF;
        --
        IF vJsonResult->'get_return'->>'status_code' <> '1' THEN
          vStatus := 'Failed - "status_code <> 1" - {"post_return": "status_code": ... }' ;
        ELSIF COALESCE( vJsonResult->'get_return'->>'id', '') = '' THEN
          vStatus := 'Failed - "id" <> "" - {"get_return": "id": ... }' ;
        ELSIF COALESCE( vJsonResult->'get_return'->>'md5', '') = '' THEN
          vStatus := 'Failed - "md5" <> "" - {"get_return": "md5": ... }' ;
        END IF;
        --
        -- PUT
        --
        RAISE INFO 'Extraindo ( vId, vMd5 ) ';
        vId  := vJsonResult->'get_return'->>'id';
        vMd5 := vJsonResult->'get_return'->>'md5';
        RAISE INFO ' - vId: %', vId;
        RAISE INFO ' - vMd5: %',vMd5;
        RAISE INFO ' - vStatus: %',vStatus;
        IF NOT bException AND vStatus = 'Passed' THEN 
          vStatus := 'Passed' ;
          vEvidencia := vJsonResult;
          BEGIN
            SELECT fx_node_test_data_put( ( '{"put": { "classe_massa": "PIPELINE.UNITTEST", "id": ' || vId || ', "md5": "' || vMd5 || '" , "update": {"reuso": false } } }' )::json ) INTO vJsonResult;
          EXCEPTION
            WHEN OTHERS THEN
              bException := TRUE;
              vMsgException := CONCAT('SQLERRM:', SQLERRM, ' - SQLSTATE ', SQLSTATE);
              RAISE INFO '%', vMsgException;
              vStatus := 'Failed - SQL EXCEPTION' ;
            --
          END;
            --
          vEvidencia := vJsonResult;
          --
        END IF;	  
        --
      END IF;	  
      --
    END IF;
    --
  ELSE 
    vStatus := 'Failed' ;
  END IF ;
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO 'Funcionalidade: Objetivo testar a mecanica basica das chamadas API para o Banco de Massa';
  RAISE INFO '  Cenario: Testar a chamada a stored function POST com uma massa "PIPELINE.UNITTEST" ';
  RAISE INFO '    Dado: Um json de POST de uma massa "PIPELINE.UNITTEST" com informacao de massa preenchida';
  RAISE INFO '    Quando: Eu fizer uma chamada a stored function com um json de POST de uma massa "PIPELINE.UNITTEST" ';
  RAISE INFO '    E: Eu fizer uma chamada a stored function com um json de GET da mesma classe de massa "PIPELINE.UNITTEST" ';
  RAISE INFO '    E: Eu fizer uma chamada a stored function com um json de PUT da mesma classe de massa "PIPELINE.UNITTEST" passando (Id=obtido no GET e Reuso=False)';
  RAISE INFO '    Entao: Eu obtenho um json PUT de retorno com codigo de status SUCESSO ';
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '';
  RAISE INFO '    Status: %', vStatus;
  RAISE INFO '    Evidencia: %', vEvidencia;
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '';
END first_block $$;


-- ----------------------------------------------------------------------------
\echo 'Fim do script!'
-- ----------------------------------------------------------------------------
