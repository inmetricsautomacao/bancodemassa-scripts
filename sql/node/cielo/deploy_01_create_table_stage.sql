﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_01_create_table_stage.sql
-- Description:
--              
-- Log-changes:
--              * 2017-02-28 JosemarSilva tipo_bandeira_newelo
--              * 2017-11-15 JosemarSilva STAR Stage tables
--              * 2017-09-26 JosemarSilva separando objetos _stage da cielo
--              * 2017-08-02 JosemarSilva stage_mainframe_redes_g124, stage_mainframe_redes_g125
--              * 2017-07-09 JosemarSilva add cod_reserva
--              * 2017-07-07 JosemarSilva Drop stage_lookup_value, add config_tipo_reserva_id to stage_*
--              * 2017-06-25 JosemarSilva Renaming 'stage_config_reserva' to 'bm_config_reserva'
--				* 2017-06-19 [Aliomar Junior] incluido a tabela stage stage_tipo_pagamento_cliente
--              * 2017-04-03 [JosemarSilva] Config Reserva, PK (id), stage_lookup_value
--              * 2017-03-21 [JosemarSilva] 
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
\echo 'stage_simuterm_lote_transacao ...'
-- ----------------------------------------------------------------------------
CREATE TABLE stage_simutermweb_lote_transacao (
  id SERIAL NOT NULL,
  config_tipo_reserva_id SMALLINT NULL,
  cod_reserva VARCHAR(100) NULL,
  id_trans BIGINT,
  data_trans DATE,
  data_hora_trans TIMESTAMP,
  nome_trans VARCHAR(200),
  id_lote BIGINT,
  nome_lote VARCHAR(50),
  versao_lote VARCHAR(50),
  espec VARCHAR(50),
  comentario VARCHAR(255),
  terminal VARCHAR(50),
  cod_estab VARCHAR(50),
  mcc VARCHAR(50),
  bandeira VARCHAR(50),
  produto VARCHAR(512),
  tipo_cartao VARCHAR(50),
  label_cartao VARCHAR(50),
  tipo_senha VARCHAR(50),
  valor BIGINT,
  nro_parcelas INTEGER,
  id_processamento BIGINT NULL,
  data_processamento TIMESTAMP NULL,
  nome_projeto VARCHAR(50) NULL,
  data_hora_transacao VARCHAR(12) NULL,
  status VARCHAR(250) NULL,
  data_lote_processado_transacao TIMESTAMP NULL,
  nsu BIGINT NULL,
  display VARCHAR(250) NULL,
  cod_autorizacao VARCHAR(6) NULL,
  star_nu_customer BIGINT NULL,
  CONSTRAINT pk_stage_simuterm_lote_transacao PRIMARY KEY(id)
);


-- ----------------------------------------------------------------------------
\echo 'stage_mainframe_sec ...'
-- ----------------------------------------------------------------------------
CREATE TABLE stage_mainframe_sec_at01(
  matriz           VARCHAR,
  prin             VARCHAR,
  ass              VARCHAR,
  cadeia           VARCHAR,
  piloto           VARCHAR,
  estab            VARCHAR,
  tipoDePgto       VARCHAR,
  cgc              VARCHAR,
  statusEc         VARCHAR,
  statusAtivo      VARCHAR,
  rating           VARCHAR,
  classe           VARCHAR,
  nomeFantasia     VARCHAR,
  fone             VARCHAR,
  razaoSocial      VARCHAR,
  fax              VARCHAR,
  nomePlaqueta     VARCHAR,
  quantPlaqueta    VARCHAR,
  endFisico        VARCHAR,
  cep              VARCHAR,
  serv             VARCHAR,
  complemento      VARCHAR,
  uf               VARCHAR,
  cb               VARCHAR,
  bl               VARCHAR,
  cidade           VARCHAR,
  seg              VARCHAR,
  cdpct            VARCHAR,
  cp               VARCHAR,
  gs               VARCHAR,
  contato          VARCHAR,
  gmar             VARCHAR,
  ativ             VARCHAR,
  data             VARCHAR,
  proprie          VARCHAR,
  top10            VARCHAR,
  recor            VARCHAR,
  cbanc            VARCHAR,
  dombanc          VARCHAR,
  bco              VARCHAR,
  ag               VARCHAR,
  cc               VARCHAR,
  antecCat         VARCHAR,
  perc             VARCHAR,
  autom            VARCHAR,
  cedir            VARCHAR,
  afiliacao        VARCHAR,
  origem           VARCHAR,
  alelo            VARCHAR,
  sd               VARCHAR,
  dataAbert        VARCHAR,
  dataFecham       VARCHAR,
  posQtde          VARCHAR,
  pp               VARCHAR,
  ultExtrato       VARCHAR,
  assArquivo       VARCHAR,
  anEcm            VARCHAR,
  tpDep            VARCHAR,
  ramoAtividade    VARCHAR,
  franca           VARCHAR,
  limite           VARCHAR,
  parcelLojista    VARCHAR,
  cva              VARCHAR,
  orizon           VARCHAR,
  comEletr         VARCHAR,
  eci6             VARCHAR,
  multivan         VARCHAR,
  mobPayment       VARCHAR,
  recebeSms        VARCHAR,
  origem2          VARCHAR,
  visaDistr        VARCHAR,
  dataUltManut     VARCHAR,
  ultManut         VARCHAR,
  usuario          VARCHAR,
  juridico         VARCHAR,
  vol              VARCHAR,
  status           VARCHAR
);

CREATE TABLE stage_mainframe_sec_at02(
  pag            VARCHAR,
  matriz         VARCHAR,
  prin           VARCHAR,
  ass            VARCHAR,
  cadeia         VARCHAR,
  statusEc       VARCHAR,
  estab          VARCHAR,
  nome           VARCHAR,
  cia            VARCHAR,
  pagto          VARCHAR,
  dombanc        VARCHAR,
  bco            VARCHAR,
  ag             VARCHAR,
  cc             VARCHAR,
  ecPagueConta   VARCHAR,
  tar            VARCHAR,
  ecAvs          VARCHAR,
  tar2           VARCHAR,
  admVisa        VARCHAR,
  saude          VARCHAR,
  outraBandeira  VARCHAR,
  estabAmex      VARCHAR,
  indPrazoFlex   VARCHAR,
  moto           VARCHAR,
  afiliador      VARCHAR,
  gestor         VARCHAR,
  atend          VARCHAR,
  statusAT02     VARCHAR,
  cod            VARCHAR,
  descricao      VARCHAR,
  pzo            VARCHAR,
  tipoCob        VARCHAR,
  tipoLiq        VARCHAR,
  taxaComis      VARCHAR,
  tarifaPItem    VARCHAR,
  taxaPrzFl      VARCHAR,
  obs            VARCHAR
);


-- ----------------------------------------------------------------------------
\echo 'stage_mainframe_redes ...'
-- ----------------------------------------------------------------------------
CREATE TABLE stage_mainframe_redes_g124(
opcao            VARCHAR,
codestb          VARCHAR,
prdmtz           VARCHAR,
prdsec           VARCHAR,
datainclusao     VARCHAR,
alteracao        VARCHAR,
situacao         VARCHAR,
nome             VARCHAR,
reduzido         VARCHAR,
endereco         VARCHAR,
cep              VARCHAR,
cidade           VARCHAR,
uf               VARCHAR,
bairro           VARCHAR,
ddd              VARCHAR,
telefone         VARCHAR,
nomecomprov      VARCHAR,
moeda            VARCHAR,
endcomprov       VARCHAR,
pais             VARCHAR,
cgccpf           VARCHAR,
tppessoa         VARCHAR,
msgecf           VARCHAR,
parceria         VARCHAR,
avs              VARCHAR,
srvmkp           VARCHAR,
pp               VARCHAR,
maladireta       VARCHAR,
grpimg           VARCHAR,
mccCode          VARCHAR,
mccDescr         VARCHAR,
lmthrflot        VARCHAR,
clfatur          VARCHAR,
solecom          VARCHAR,
grinfadic        VARCHAR,
estabelec        VARCHAR,
alugado          VARCHAR,
comodato         VARCHAR,
redecard         VARCHAR,
outros           VARCHAR,
telefone2        VARCHAR,
multiec          VARCHAR,
anlsfrd          VARCHAR,
cartaopresente   VARCHAR,
autterc          VARCHAR,
internet         VARCHAR,
pgtrecor         VARCHAR,
multivan         VARCHAR,
bloqinternac     VARCHAR,
autparc          VARCHAR,
reentrada        VARCHAR,
bloqueado        VARCHAR,
st               VARCHAR,
mot              VARCHAR,
zonafr           VARCHAR,
migbob           VARCHAR,
correspbco       VARCHAR,
cartintnauten    VARCHAR,
blist            VARCHAR,
grpprivate       VARCHAR
);


CREATE TABLE stage_mainframe_redes_g125(
opcao             VARCHAR,
codestb           VARCHAR,
prdmtz            VARCHAR,
prdsec            VARCHAR,
datainclusao      VARCHAR,
alteracao         VARCHAR,
situacao          VARCHAR,
nome              VARCHAR,
reduzido          VARCHAR,
endereco          VARCHAR,
cep               VARCHAR,
msg               VARCHAR,
codmsg            VARCHAR,
opc               VARCHAR,
mtz               VARCHAR,
sec               VARCHAR,
descricaoproduto  VARCHAR,
liq               VARCHAR,
regras            VARCHAR,
dig               VARCHAR,
habil             VARCHAR
);

-- ----------------------------------------------------------------------------
\echo 'STAR Stage tables ...'
-- ----------------------------------------------------------------------------
CREATE TABLE stage_bm_config_reserva_nu_customer
(
  id SERIAL NOT NULL,
  nu_customer BIGINT NOT NULL,
  cod_reserva character varying(100),
  CONSTRAINT pk_stage_bm_config_reserva PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_merchant (
  id SERIAL NOT NULL,
  nu_customer BIGINT,
  cd_activity_status VARCHAR(3),
  cd_mcc BIGINT,
  nm_dba VARCHAR(70),
  nm_legal VARCHAR(70),
  nu_root_cnpj_cpf BIGINT,
  nu_branch_cnpj BIGINT,
  nu_control_cnpj_cpf BIGINT,
  cd_amex_id BIGINT,
  in_debit_balance VARCHAR(1),
  in_active VARCHAR(1),
  cd_arv_category BIGINT,
  sg_business_type VARCHAR(1)
  CONSTRAINT pk_tbctmr_merchant PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_merchant_bank (
  id SERIAL NOT NULL,
  nu_merchant_bank BIGINT,
  cd_bank VARCHAR(4),
  nu_agency VARCHAR(5),
  nu_digit_control_agency VARCHAR(2),
  nu_current_account VARCHAR(14),
  nu_customer BIGINT,
  nu_digit_control_account VARCHAR(2),
  cd_status_approval VARCHAR(1),
  cd_merchant_bank_type BIGINT,
  CONSTRAINT pk_tbctmr_merchant_bank PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_config_pmt_group (
  id SERIAL NOT NULL,
  dt_config_reference TIMESTAMP,
  nu_hierarchical_node BIGINT,
  cd_product BIGINT,
  nu_merchant_bank BIGINT,
  dt_valid_start TIMESTAMP,
  dt_valid_end TIMESTAMP,
  in_active VARCHAR(1),
  CONSTRAINT pk_tbctmr_config_pmt_group PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_merchant_bank_type (
  id SERIAL NOT NULL,
  cd_merchant_bank_type BIGINT,
  dc_short_merchant_bank_type VARCHAR(12),
  dc_merchant_bank_type VARCHAR
  CONSTRAINT pk_tbctmr_merchant_bank_type PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_status_activity (
  id SERIAL NOT NULL,
  cd_activity_status VARCHAR(3),
  nm_status_activity VARCHAR(30),
  CONSTRAINT pk_tbctmr_status_activity PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbhrqr_hierarchical_node (
  id SERIAL NOT NULL,
  dt_hierarchy_reference TIMESTAMP,
  nu_hierarchical_node BIGINT,
  cd_hierarchy_type BIGINT,
  nu_node_level BIGINT,
  cd_node_type BIGINT,
  dh_record_creation TIMESTAMP,
  nm_login_user_creation VARCHAR(50),
  dh_record_last_modified TIMESTAMP,
  nm_login_user_last_modified VARCHAR(50),
  in_node_active VARCHAR(1),
  CONSTRAINT pk_tbhrqr_hierarchical_node PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbhrqr_merchant_node (
  id SERIAL NOT NULL,
  dt_hierarchy_reference TIMESTAMP,
  nu_hierarchical_node BIGINT,
  nu_customer BIGINT,
  nu_mod_customer BIGINT,
  CONSTRAINT pk_tbhrqr_merchant_node PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbhrqr_node_type (
  id SERIAL NOT NULL,
  cd_node_type BIGINT,
  cd_hierarchy_type BIGINT,
  nm_node_type VARCHAR(70),
  nu_node_level BIGINT,
  CONSTRAINT pk_tbhrqr_node_type PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbprdr_product (
  id SERIAL NOT NULL,
  cd_product BIGINT,
  cd_payment_category BIGINT,
  cd_payment_method BIGINT,
  cd_card_association BIGINT,
  cd_acquirer BIGINT,
  cd_contract_card_assoc BIGINT,
  CONSTRAINT pk_tbprdr_product PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbprdr_card_association (
  id SERIAL NOT NULL,
  cd_card_association BIGINT,
  nm_card_association VARCHAR(50),
  CONSTRAINT pk_tbprdr_card_association PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbprdr_payment_method (
  id SERIAL NOT NULL,
  cd_payment_method BIGINT,
  dc_payment_method VARCHAR(50),
  CONSTRAINT pk_tbprdr_payment_method PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbprdr_payment_category (
  id SERIAL NOT NULL,
  cd_payment_category BIGINT,
  dc_payment_category VARCHAR(30),
  CONSTRAINT pk_tbprdr_payment_category PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_acquirer (
  id SERIAL NOT NULL,
  cd_acquirer BIGINT,
  cd_iso_country BIGINT,
  nm_dba_acquirer VARCHAR(50),
  CONSTRAINT pk_tbctmr_acquirer PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbprdr_contract_card_assoc (
  id SERIAL NOT NULL,
  cd_contract_card_assoc BIGINT,
  nm_contract_card_assoc VARCHAR(20),
  CONSTRAINT pk_tbprdr_contract_card_assoc PRIMARY KEY(id)
);


CREATE TABLE stage_star_tbsetr_adjustment (
  id SERIAL NOT NULL,
  nu_financial_adjustment BIGINT,
  dt_batch TIMESTAMP,
  nu_mod_customer BIGINT,
  cd_movement_type BIGINT,
  nu_customer BIGINT,
  vl_adjustment_amount DECIMAL,
  dh_adjustment TIMESTAMP,
  dt_settlement_adjustment TIMESTAMP,
  CONSTRAINT pk_tbsetr_adjustment PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbsetr_settlement_transaction (
  id SERIAL NOT NULL,
  nu_transaction_id VARCHAR(15),
  dt_batch TIMESTAMP,
  pc_interchange_rate DECIMAL,
  dc_interchange_category VARCHAR(200),
  nu_installment_sequence BIGINT,
  vl_transaction_amount DECIMAL,
  cd_currency_code VARCHAR(3),
  vl_auth_amount DECIMAL,
  nu_authorization VARCHAR(6),
  vl_orig_transaction_amount DECIMAL,
  nu_customer BIGINT,
  nu_terminal VARCHAR(8),
  pc_discount_rate DECIMAL,
  vl_mdr DECIMAL,
  vl_ic DECIMAL,
  cd_product BIGINT,
  qt_total_installment BIGINT,
  cd_issuer_bank VARCHAR(4),
  cd_card_association BIGINT,
  nu_card_bin BIGINT,
  nu_mod_customer BIGINT,
  nu_serial_sequence_transaction BIGINT,
  dt_authorization_transaction TIMESTAMP,
  CONSTRAINT pk_tbsetr_settlement_transaction PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_config_pmt_merchant (
  id SERIAL NOT NULL,
  dt_config_reference TIMESTAMP,
  nu_hierarchical_node BIGINT,
  nu_customer BIGINT,
  cd_product BIGINT,
  nu_merchant_bank BIGINT,
  dt_valid_start TIMESTAMP,
  dt_valid_end TIMESTAMP,
  nu_operation BIGINT,
  in_active VARCHAR,
  CONSTRAINT pk_tbctmr_config_pmt_merchant PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbhrqr_financial_chain_node (
  id SERIAL NOT NULL,
  dt_hierarchy_reference TIMESTAMP,
  nu_hierarchical_node BIGINT,
  nu_main_customer BIGINT,
  in_centralized_payment VARCHAR(1),
  dh_record_creation TIMESTAMP,
  nm_payment_group VARCHAR(70),
  CONSTRAINT pk_tbhrqr_financial_chain_node PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbhrqr_root_cnpj_node (
  id SERIAL NOT NULL,
  dt_hierarchy_reference TIMESTAMP,
  nu_hierarchical_node BIGINT,
  nu_customer BIGINT,
  nu_root_cnpj_cpf BIGINT,
  nm_root_cnpj_cpf_group VARCHAR(70),
  in_individual_customer VARCHAR(1),
  nu_mod_customer BIGINT,
  sg_business_type VARCHAR(1),
  CONSTRAINT pk_tbhrqr_root_cnpj_node PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbhrqr_relationship_hrql_node (
  id SERIAL NOT NULL,
  dt_hierarchy_reference TIMESTAMP,
  nu_hierarchical_node BIGINT,
  nu_main_customer BIGINT,
  nm_relationship_group VARCHAR(70),
  CONSTRAINT pk_tbhrqr_relationship_hrql_node PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbhrqr_management_hierar_node (
  id SERIAL NOT NULL,
  dt_hierarchy_reference TIMESTAMP,
  nu_hierarchical_node BIGINT,
  nm_management_group VARCHAR(70),
  CONSTRAINT pk_tbhrqr_management_hierar_node PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbhrqr_hierarchy (
  id SERIAL NOT NULL,
  dt_hierarchy_reference TIMESTAMP,
  cd_hierarchy_type BIGINT,
  nu_child_hierarchical_node BIGINT,
  nu_child_node_level BIGINT,
  cd_child_node_type BIGINT,
  nu_parent_hierarchical_node BIGINT,
  nu_parent_node_level BIGINT,
  cd_parent_node_type BIGINT,
  CONSTRAINT pk_tbhrqr_hierarchy PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_terminal_type (
  id SERIAL NOT NULL,
  cd_terminal_type VARCHAR(2),
  cd_technology BIGINT,
  dc_equipament_type VARCHAR(70),
  dc_category_type VARCHAR(70),
  dc_technology_type VARCHAR(70),
  in_terminal_type_status VARCHAR(2),
  num_loja_redes smallint,
  CONSTRAINT pk_tbctmr_terminal_type PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_logic_terminal (
  id SERIAL NOT NULL,
  nu_terminal_id VARCHAR(8),
  dt_last_start TIMESTAMP,
  cd_terminal_type VARCHAR(2),
  cd_address BIGINT,
  dt_terminal_installation TIMESTAMP,
  dt_start_operation TIMESTAMP,
  dt_end_operation TIMESTAMP,
  qt_pinpad BIGINT,
  nm_terminal_version VARCHAR(70),
  dt_pairing TIMESTAMP,
  nu_dv_terminal_id VARCHAR(1),
  cd_stco_cvnc CHAR(1),
  CONSTRAINT pk_tbctmr_logic_terminal PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_geogr_address (
  id SERIAL NOT NULL,
  cd_address BIGINT,
  nu_customer BIGINT,
  CONSTRAINT pk_tbctmr_geogr_address PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_host_bank (
  id SERIAL NOT NULL,
  cd_bank VARCHAR(4),
  nm_bank_bacen VARCHAR(64),
  cd_status_active VARCHAR(1),
  nu_ispb BIGINT,
  CONSTRAINT pk_tbctmr_host_bank PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbctmr_technology_type (
  id SERIAL NOT NULL,
  cd_technology_type BIGINT,
  dc_technology_type VARCHAR(50),
  qt_limit_day_refund BIGINT,
  dc_net_tecnology_type VARCHAR(5),
  CONSTRAINT pk_tbctmr_technology_type PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbcapr_auth_input_type (
  id SERIAL NOT NULL,
  cd_input_type VARCHAR(1),
  nm_input_type VARCHAR(50),
  CONSTRAINT pk_tbcapr_auth_input_type PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbcapr_captured_transaction (
  id SERIAL NOT NULL,
  dt_batch TIMESTAMP,
  cd_card_association BIGINT,
  nu_card_ref BIGINT,
  nu_batch BIGINT,
  nu_captured_transaction BIGINT,
  nu_transaction_id VARCHAR(15),
  nu_terminal VARCHAR(8),
  nu_customer BIGINT,
  cd_customer_mcc VARCHAR(4),
  nu_store VARCHAR(4),
  dh_authorization TIMESTAMP,
  nu_nsu_transaction BIGINT,
  cd_product BIGINT,
  nu_card_bin BIGINT,
  dt_transaction TIMESTAMP,
  vl_transaction BIGINT,
  in_debit_credit VARCHAR(1),
  cd_currency VARCHAR(3),
  cd_capture_type VARCHAR(1),
  in_ecommerce VARCHAR(1),
  nu_contract BIGINT,
  nu_load_file_id BIGINT,
  cd_acquirer BIGINT,
  nu_card_truncated VARCHAR(19),
  nu_branch_cnpj BIGINT,
  nu_control_cnpj_cpf BIGINT,
  nu_terminal_original VARCHAR(8),
  cd_card_technology_type VARCHAR(1),
  nu_mod_customer BIGINT,
  CONSTRAINT pk_tbcapr_captured_transaction PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbshrr_movement_date_process (
  id SERIAL NOT NULL,
  cd_process_type BIGINT,
  dt_movement TIMESTAMP,
  cd_movement_status VARCHAR(1),
  CONSTRAINT pk_tbshrr_movement_date_process PRIMARY KEY(id)
);

CREATE TABLE stage_star_tbshrr_holiday (
  id SERIAL NOT NULL,
  cd_holiday INTEGER,
  dd_holiday INTEGER,
  mm_holiday INTEGER,
  yy_holiday INTEGER,
  nm_holiday VARCHAR(100),
  CONSTRAINT pk_tbshrr_holiday PRIMARY KEY(id)
);


-- ----------------------------------------------------------------------------
\echo 'stage_files_etlhml ...'
-- ----------------------------------------------------------------------------

CREATE TABLE stage_files_etlhml (
  id SERIAL NOT NULL,
  dt_referencia VARCHAR(30),
  cod_environment VARCHAR(10),
  funcionalidade VARCHAR(100),
  bandeira VARCHAR(10),
  tipo_bandeira_newelo VARCHAR(10),
  num_banco VARCHAR(10),
  tipo_arquivo_banco_domicilio VARCHAR(30),
  tipo_remessa_retorno VARCHAR(30),
  tipo_arquivo_relatorios_clientes VARCHAR(30),
  sistema VARCHAR(30),
  stage_file_path VARCHAR(300)
);


-- ----------------------------------------------------------------------------
\echo 'Fim do script!'
-- ----------------------------------------------------------------------------
