SELECT
       cpm.dt_config_reference,
       cpm.nu_hierarchical_node,
       cpm.nu_customer,
       cpm.cd_product,
       cpm.nu_merchant_bank,
       cpm.dt_valid_start,
       cpm.dt_valid_end,
       cpm.nu_operation,
       cpm.in_active
FROM   STARR4.TBCTMR_MERCHANT m,
       STARR4.tbctmr_config_pmt_merchant cpm
WHERE  1=1
AND    m.DH_RECORD_CREATION < TO_DATE('01/12/2017','DD/MM/YYYY')
AND    cpm.nu_customer = m.nu_customer
AND    cpm.dt_config_reference =  TRUNC(SYSDATE)
