DROP INDEX pk_host_bank;

TRUNCATE TABLE stage_star_tbctmr_host_bank;
ALTER SEQUENCE stage_star_tbctmr_host_bank_id_seq RESTART WITH 1;
