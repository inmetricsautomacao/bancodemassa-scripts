SELECT ga.nu_customer, tt.num_loja_redes--, lt.nu_terminal_id
FROM   stage_star_tbctmr_terminal_type tt
INNER  JOIN stage_star_tbctmr_logic_terminal lt
ON     lt.cd_terminal_type = tt.cd_terminal_type
INNER  JOIN stage_star_tbctmr_geogr_address ga
ON     ga.cd_address = lt.cd_address
WHERE  tt.num_loja_redes IS NOT NULL
AND    ga.nu_customer IN 
       ( 
         SELECT nu_customer FROM stage_bm_config_reserva_nu_customer
       )
GROUP  BY ga.nu_customer, tt.num_loja_redes
;