DROP INDEX IF EXISTS pk_config_pmt_merchant;
DROP INDEX IF EXISTS fk_config_pmt_merchant_01;
DROP INDEX IF EXISTS fk_config_pmt_merchant_02;
DROP INDEX IF EXISTS fk_config_pmt_merchant_03;
TRUNCATE TABLE stage_star_tbctmr_config_pmt_merchant;
ALTER SEQUENCE stage_star_tbctmr_config_pmt_merchant_id_seq RESTART WITH 1;
