SELECT
       cpg.dt_config_reference,
       cpg.nu_hierarchical_node,
       cpg.cd_product,
       cpg.nu_merchant_bank,
       cpg.dt_valid_start,
       cpg.dt_valid_end,
       cpg.in_active
FROM   STARR4.tbctmr_config_pmt_group cpg
WHERE  cpg.dt_config_reference =  TRUNC(SYSDATE)
AND    cpg.nu_hierarchical_node IN
       (
         SELECT fcn.nu_hierarchical_node
         FROM   STARR4.tbctmr_merchant m,
                STARR4.tbhrqr_financial_chain_node fcn
         WHERE  m.DH_RECORD_CREATION < TO_DATE('12/12/2017','DD/MM/YYYY')
         AND    fcn.nu_main_customer = m.nu_customer
         AND    fcn.dt_hierarchy_reference = TRUNC(SYSDATE)
       )
