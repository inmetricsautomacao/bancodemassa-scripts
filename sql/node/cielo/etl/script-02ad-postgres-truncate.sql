DROP INDEX IF EXISTS pk_logic_terminal;
DROP INDEX IF EXISTS fk_logic_terminal_01;

TRUNCATE TABLE stage_star_tbctmr_logic_terminal;
ALTER SEQUENCE stage_star_tbctmr_logic_terminal_id_seq RESTART WITH 1;
