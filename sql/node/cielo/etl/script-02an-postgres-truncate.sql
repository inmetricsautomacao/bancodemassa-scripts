DROP INDEX IF EXISTS pk_node_type;
TRUNCATE TABLE stage_star_tbhrqr_node_type;
ALTER SEQUENCE stage_star_tbhrqr_node_type_id_seq RESTART WITH 1;
