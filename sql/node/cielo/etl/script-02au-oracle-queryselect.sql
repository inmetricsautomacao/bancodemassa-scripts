SELECT
       nu_financial_adjustment,
       dt_batch,
       nu_mod_customer,
       cd_movement_type,
       nu_customer,
       vl_adjustment_amount,
       dh_adjustment,
       dt_settlement_adjustment
FROM   STARR4.tbsetr_adjustment
WHERE  dt_batch > TRUNC(SYSDATE) - 3