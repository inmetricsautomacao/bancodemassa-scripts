DROP INDEX IF EXISTS pk_financial_chain_node;
DROP INDEX IF EXISTS ak_financial_chain_node;
TRUNCATE TABLE stage_star_tbhrqr_financial_chain_node;
ALTER SEQUENCE stage_star_tbhrqr_financial_chain_node_id_seq RESTART WITH 1;
