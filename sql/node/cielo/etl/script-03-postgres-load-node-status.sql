-- ----------------------------------------------------------------------------
\echo 'a. Grouping and counting ...'
-- ----------------------------------------------------------------------------
SELECT c.classe_massa, s.status_massa, COUNT(*) 
FROM   bm_node_instancia_massa i,
       bm_node_classe_massa    c,
       bm_node_status_massa    s
WHERE  i.node_classe_massa_id = c.id
AND    i.node_status_massa_id = s.id
GROUP  BY c.classe_massa, s.status_massa
ORDER  BY c.classe_massa, s.status_massa;

