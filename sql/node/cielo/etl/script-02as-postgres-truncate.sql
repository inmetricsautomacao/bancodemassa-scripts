DROP INDEX IF EXISTS pk_acquirer;
TRUNCATE TABLE stage_star_tbctmr_acquirer;
ALTER SEQUENCE stage_star_tbctmr_acquirer_id_seq RESTART WITH 1;
