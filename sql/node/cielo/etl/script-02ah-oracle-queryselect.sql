SELECT
       mb.nu_merchant_bank,
       mb.cd_bank,
       mb.nu_agency,
       mb.nu_digit_control_agency,
       mb.nu_current_account,
       mb.nu_customer,
       mb.nu_digit_control_account,
       mb.cd_status_approval,
       mb.cd_merchant_bank_type
FROM   STARR4.tbctmr_merchant m,
       STARR4.tbctmr_merchant_bank mb
WHERE  1=1
AND    m.DH_RECORD_CREATION < TO_DATE('12/12/2017','DD/MM/YYYY')
AND    mb.nu_customer = m.nu_customer