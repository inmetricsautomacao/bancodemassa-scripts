INSERT INTO stage_star_tbcapr_captured_transaction
(
  dt_batch,
  cd_card_association,
  nu_card_ref,
  nu_batch,
  nu_captured_transaction,
  nu_transaction_id,
  nu_terminal,
  nu_customer,
  cd_customer_mcc,
  nu_store,
  dh_authorization,
  nu_nsu_transaction,
  cd_product,
  nu_card_bin,
  dt_transaction,
  vl_transaction,
  in_debit_credit,
  cd_currency,
  cd_capture_type,
  in_ecommerce,
  nu_contract,
  nu_load_file_id,
  cd_acquirer,
  nu_card_truncated,
  nu_branch_cnpj,
  nu_control_cnpj_cpf,
  nu_terminal_original,
  cd_card_technology_type,
  nu_mod_customer
)
