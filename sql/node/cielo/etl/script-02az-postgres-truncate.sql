DROP INDEX IF EXISTS pk_relationship_hrql_node;
TRUNCATE TABLE stage_star_tbhrqr_relationship_hrql_node;
ALTER SEQUENCE stage_star_tbhrqr_relationship_hrql_node_id_seq RESTART WITH 1;
