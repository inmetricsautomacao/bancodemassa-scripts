DROP INDEX IF EXISTS pk_merchant_bank_type;
TRUNCATE TABLE stage_star_tbctmr_merchant_bank_type;
ALTER SEQUENCE stage_star_tbctmr_merchant_bank_type_id_seq RESTART WITH 1;
