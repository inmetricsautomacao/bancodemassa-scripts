DROP INDEX IF EXISTS pk_payment_category;
TRUNCATE TABLE stage_star_tbprdr_payment_category;
ALTER SEQUENCE stage_star_tbprdr_payment_category_id_seq RESTART WITH 1;
