CREATE UNIQUE INDEX pk_product    ON stage_star_tbprdr_product(cd_product);
CREATE        INDEX fk_product_01 ON stage_star_tbprdr_product(cd_payment_category);
CREATE        INDEX fk_product_02 ON stage_star_tbprdr_product(cd_payment_method);
CREATE        INDEX fk_product_03 ON stage_star_tbprdr_product(cd_card_association);
CREATE        INDEX fk_product_04 ON stage_star_tbprdr_product(cd_acquirer);
CREATE        INDEX fk_product_05 ON stage_star_tbprdr_product(cd_contract_card_assoc);
