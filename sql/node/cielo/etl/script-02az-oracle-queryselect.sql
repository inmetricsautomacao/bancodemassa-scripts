SELECT
       rhn.dt_hierarchy_reference,
       rhn.nu_hierarchical_node,
       rhn.nu_main_customer,
       rhn.nm_relationship_group
FROM   STARR4.tbhrqr_relationship_hrql_node rhn
WHERE  rhn.dt_hierarchy_reference =  TRUNC(SYSDATE)
