SELECT
       rcn.dt_hierarchy_reference,
       rcn.nu_hierarchical_node,
       rcn.nu_customer,
       rcn.nu_root_cnpj_cpf,
       rcn.nm_root_cnpj_cpf_group,
       rcn.in_individual_customer,
       rcn.nu_mod_customer,
       rcn.sg_business_type
FROM   STARR4.tbhrqr_root_cnpj_node rcn
WHERE  rcn.dt_hierarchy_reference =  TRUNC(SYSDATE)
