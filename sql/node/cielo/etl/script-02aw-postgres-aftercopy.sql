CREATE UNIQUE INDEX pk_config_pmt_merchant    ON stage_star_tbctmr_config_pmt_merchant(nu_hierarchical_node,cd_product);
CREATE        INDEX fk_config_pmt_merchant_01 ON stage_star_tbctmr_config_pmt_merchant(cd_product);
CREATE        INDEX fk_config_pmt_merchant_02 ON stage_star_tbctmr_config_pmt_merchant(nu_merchant_bank);
CREATE        INDEX fk_config_pmt_merchant_03 ON stage_star_tbctmr_config_pmt_merchant(nu_hierarchical_node);
