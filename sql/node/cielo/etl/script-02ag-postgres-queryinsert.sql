INSERT INTO stage_star_tbctmr_merchant
(
  nu_customer,
  cd_activity_status,
  cd_mcc,
  nm_dba,
  nm_legal,
  nu_root_cnpj_cpf,
  nu_branch_cnpj,
  nu_control_cnpj_cpf,
  cd_amex_id,
  in_debit_balance,
  in_active,
  sg_business_type,
  cd_arv_category
)
