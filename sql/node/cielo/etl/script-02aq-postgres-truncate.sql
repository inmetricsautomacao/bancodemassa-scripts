DROP INDEX IF EXISTS pk_payment_method;
TRUNCATE TABLE stage_star_tbprdr_payment_method;
ALTER SEQUENCE stage_star_tbprdr_payment_method_id_seq RESTART WITH 1;
