SELECT lotetrans.IdTrans,                               
       trans.Data,
       trans.DataHora,
       trans.NomeTrans,
       lote.IdLote,
       lote.Nome,
       lote.Versao,
       lote.Espec,
       lotetrans.Comentario,
       lotetrans.Terminal,
       lotetrans.CodEstab,
       lotetrans.MCC,
       lotetrans.Bandeira,
       lotetrans.Produto,
       lotetrans.TipoCartao,
       lotetrans.LabelCartao,
       lotetrans.TipoSenha,
       lotetrans.Valor,
       lotetrans.NroParcelas,
       loteproc.IdProcessamento,
       loteproc.DataProcessamento,
       prj.nome NomeProjeto,
       loteproctran.DataHoraTransacao,
       loteproctran.Status,
       loteproctran.Data DataLoteProcessadoTransacao,
       loteproctran.Nsu,
       loteproctran.Display,
       loteproctran.CodAutorizacao
FROM dbo.Lote lote
INNER  JOIN dbo.LoteTransacao lotetrans
ON     lotetrans.IdLote = lote.IdLote
INNER  JOIN dbo.Transacao trans
ON     trans.SeqTrans = lotetrans.IdTrans
INNER  JOIN dbo.LoteProcessado loteproc
ON     loteproc.IdLote = lote.IdLote
INNER  JOIN dbo.Projeto prj
ON     prj.IdProjeto = loteproc.IdProjeto
INNER  JOIN dbo.LoteProcessadoTransacao loteproctran
ON     loteproctran.IdProcessamento = loteproc.IdProcessamento
AND    loteproctran.IdTrans = lotetrans.IdTrans
WHERE  lote.Nome IN 
       (
         'BoB_HML_Aut_HP_Cap_Diners_ID2203',
         'BoB_HML_Aut_HP_Cap_Diners_Mob_ID2204',
         'Aut_HP_Cap_ELO_Mob_ID2206',
         'Aut_HP_Cap_ELO_Mob_ID2206',
         'Aut_HP_Cap_ELO_P1_ID2207',
         'Aut_HP_Cap_ELO_P2_ID2208',
         'Aut_HP_Cap_ELOCONS_ID2210',
         'Aut_HP_Cap_ELONOVOS_2212',
         'BoB_HML_Aut_HP_Cap_Master_Mob_ID2213',
         'BoB_HML_Aut_HP_Cap_Master_P1_ID2214',
         'BoB_HML_Aut_HP_Cap_Master_P2_ID2215',
         'BoB_HML_Aut_HP_Cap_Master_Mob_ID2216',
         'Aut_HP_Cap_VISA_Mob_ID2217',
         'Aut_HP_Cap_VISA_P1',
         'Aut_HP_Cap_VISA_P2_ID2219',
         'Aut_HP_Cap_VISA_P3_ID2220',
         'BoB_HML_Aut_HP_Cap_MDR_P2_ID2222',
         'Aut_HP_Cap_OTHERS_Mob_2246',
         'BoB_HML_Aut_HP_Cap_Visa_RE_ID2280',
         'Aut_HP_Cap_ELOCONS_Mob2327',
         'AUT_HP_CAP_MASTER_P3_ID2346',
         'AUT_HML_CAP_MASTER_P4_2347',
         'AUT_HP_CAP_VISA_P4_2348',
         'AUT_HP_CAP_VISA_P5_2349',
         'AUT_HP_CAP_VISA_P6_2350',
         'AUT_HP_CAP_VISA_P7_2351',
         'AUT_HP_CAP_VISA_P8_2352',
         'AUT_HP_CAP_ELO_P3_ID2353.1',
         'AUT_HP_CAP_ELO_P3_ID2353.2',
         'AUT_HP_CAP_ELO_P3_ID2353.3',
         'AUT_HP_CAP_ELO_P4_2354',
         'AUT_HP_CAP_ELO_P4_2354_2',
         'AUT_HP_CAP_ELO_P5_2355',
         'AUT_HP_CAP_DINERS_P2_2356',
         'AUT_HP_CAP_DINERS_P3_ID2373.1',
         'AUT_HP_CAP_DINERS_P3_ID2373.2',
         ''
       )
AND   lote.IdLote = 
      (
        SELECT MAX( ultlote.IdLote )
        FROM   dbo.Lote ultlote
        WHERE  lote.Nome = ultlote.Nome
      )
AND   lotetrans.Comentario NOT LIKE 'Confirmação EMV%'
AND   lotetrans.Comentario NOT LIKE 'Fechamento de Lote%'
AND   loteproc.DataProcessamento > GETDATE() - 7 /* dias */
/*
ORDER BY lote.Nome, lotetrans.Comentario
*/