DROP INDEX IF EXISTS fk_merchant_bank_01;
DROP INDEX IF EXISTS fk_merchant_bank_02;
DROP INDEX IF EXISTS fk_merchant_bank_03;
TRUNCATE TABLE stage_star_tbctmr_merchant_bank;
ALTER SEQUENCE stage_star_tbctmr_merchant_bank_id_seq RESTART WITH 1;
