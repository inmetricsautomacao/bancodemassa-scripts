DROP INDEX IF EXISTS pk_root_cnpj_node;
TRUNCATE TABLE stage_star_tbhrqr_root_cnpj_node;
ALTER SEQUENCE stage_star_tbhrqr_root_cnpj_node_id_seq RESTART WITH 1;
