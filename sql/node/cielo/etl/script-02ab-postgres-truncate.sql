DROP INDEX IF EXISTS fk_hierarchy_01;
DROP INDEX IF EXISTS fk_hierarchy_02;
DROP INDEX IF EXISTS fk_hierarchy_03;
DROP INDEX IF EXISTS fk_hierarchy_04;
DROP INDEX IF EXISTS fk_hierarchy_05;
TRUNCATE TABLE stage_star_tbhrqr_hierarchy;
ALTER SEQUENCE stage_star_tbhrqr_hierarchy_id_seq RESTART WITH 1;
