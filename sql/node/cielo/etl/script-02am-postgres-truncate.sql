DROP INDEX IF EXISTS pk_merchant_node;
DROP INDEX IF EXISTS fk_merchant_node_01;
TRUNCATE TABLE stage_star_tbhrqr_merchant_node;
ALTER SEQUENCE stage_star_tbhrqr_merchant_node_id_seq RESTART WITH 1;
