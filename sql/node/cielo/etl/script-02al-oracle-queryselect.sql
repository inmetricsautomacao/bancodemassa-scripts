SELECT
       dt_hierarchy_reference,
       nu_hierarchical_node,
       cd_hierarchy_type,
       nu_node_level,
       cd_node_type,
       dh_record_creation,
       nm_login_user_creation,
       dh_record_last_modified,
       nm_login_user_last_modified,
       in_node_active
FROM   STARR4.tbhrqr_hierarchical_node
WHERE  dt_hierarchy_reference = TRUNC(SYSDATE)-1