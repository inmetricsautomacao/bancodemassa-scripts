DROP INDEX IF EXISTS pk_product   ;
DROP INDEX IF EXISTS fk_product_01;
DROP INDEX IF EXISTS fk_product_02;
DROP INDEX IF EXISTS fk_product_03;
DROP INDEX IF EXISTS fk_product_04;
DROP INDEX IF EXISTS fk_product_05;
TRUNCATE TABLE stage_star_tbprdr_product;
ALTER SEQUENCE stage_star_tbprdr_product_id_seq RESTART WITH 1;
