DROP INDEX IF EXISTS pk_terminal_type;
TRUNCATE TABLE stage_star_tbctmr_terminal_type;
ALTER SEQUENCE stage_star_tbctmr_terminal_type_id_seq RESTART WITH 1;
