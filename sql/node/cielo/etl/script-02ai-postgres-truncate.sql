DROP INDEX IF EXISTS fk_tbctmr_config_pmt_group_01;
DROP INDEX IF EXISTS fk_tbctmr_config_pmt_group_02;
DROP INDEX IF EXISTS fk_tbctmr_config_pmt_group_03;


TRUNCATE TABLE stage_star_tbctmr_config_pmt_group;
ALTER SEQUENCE stage_star_tbctmr_config_pmt_group_id_seq RESTART WITH 1;
