DROP INDEX IF EXISTS pk_status_activity;
TRUNCATE TABLE stage_star_tbctmr_status_activity;
ALTER SEQUENCE stage_star_tbctmr_status_activity_id_seq RESTART WITH 1;
