DROP INDEX pk_geogr_address;
DROP INDEX fk_geogr_address_01;

TRUNCATE TABLE stage_star_tbctmr_geogr_address;
ALTER SEQUENCE stage_star_tbctmr_geogr_address_id_seq RESTART WITH 1;
