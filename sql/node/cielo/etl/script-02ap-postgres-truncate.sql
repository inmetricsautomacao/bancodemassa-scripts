DROP INDEX IF EXISTS pk_card_association;
TRUNCATE TABLE stage_star_tbprdr_card_association;
ALTER SEQUENCE stage_star_tbprdr_card_association_id_seq RESTART WITH 1;
