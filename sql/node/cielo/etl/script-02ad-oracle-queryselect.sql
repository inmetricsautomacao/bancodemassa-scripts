SELECT
  nu_terminal_id,
  dt_last_start,
  cd_terminal_type,
  cd_address,
  dt_terminal_installation,
  dt_start_operation,
  dt_end_operation,
  qt_pinpad,
  nm_terminal_version,
  dt_pairing,
  nu_dv_terminal_id,
  cd_stco_cvnc
FROM STARR4.tbctmr_logic_terminal
