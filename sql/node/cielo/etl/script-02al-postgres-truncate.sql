DROP INDEX IF EXISTS pk_hierarchical_node;
DROP INDEX IF EXISTS fk_hierarchical_node_01;
TRUNCATE TABLE stage_star_tbhrqr_hierarchical_node;
ALTER SEQUENCE stage_star_tbhrqr_hierarchical_node_id_seq RESTART WITH 1;
