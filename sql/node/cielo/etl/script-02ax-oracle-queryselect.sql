SELECT
       fcn.dt_hierarchy_reference,
       fcn.nu_hierarchical_node,
       fcn.nu_main_customer,
       fcn.in_centralized_payment,
       fcn.dh_record_creation,
       fcn.nm_payment_group
FROM   STARR4.tbhrqr_financial_chain_node fcn
WHERE  fcn.dt_hierarchy_reference =  TRUNC(SYSDATE)
