-- ----------------------------------------------------------------------------
-- Filename:
--              unittest_13_pgsqlblock_stage.sql
-- Description:
--              
-- Log-changes:
--              * 2017-09-26 JosemarSilva Develop Unit Test Postgresql
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
\echo 'Início ...'
-- ----------------------------------------------------------------------------

DO $$ 
<<first_block>>
DECLARE
  bException BOOLEAN := FALSE;
  vMsgException VARCHAR := '';  -- SQLERRM, SQLSTATE
  vEvidencia    VARCHAR := '';
  vStatus       VARCHAR := '';
  vJsonResult   JSON;
BEGIN
  --
  BEGIN
    SELECT fx_node_test_data_get( ( '{"get": {"classe_massa": "ETL.LOGIN" } }' )::json ) INTO vJsonResult;
  EXCEPTION
    WHEN OTHERS THEN
      bException := TRUE;
	  vMsgException := CONCAT('SQLERRM:', SQLERRM, ' - SQLSTATE ', SQLSTATE);
	  RAISE INFO '%', vMsgException;
  END;
  --
  IF NOT bException THEN 
    vStatus := 'Passed' ;
	vEvidencia := vJsonResult;
	IF LENGTH(vEvidencia) > 500 THEN
      vEvidencia := SUBSTR(vEvidencia,1,500) || ' ... ' || SUBSTR(vEvidencia,LENGTH(vEvidencia)-200,200);
	END IF;
	--
	IF vJsonResult->'get_return'->>'status_code' <> '1' THEN
      vStatus := 'Failed - "status_code <> 1" - {"post_return": "status_code": ... }' ;
	ELSIF vJsonResult->'get_return'->>'id' = '' THEN
      vStatus := 'Failed - "id" MUST BE NOT NULL" - {"post_return": "id": "<???>" }' ;
	END IF;
  ELSE 
    vStatus := 'Failed' ;
  END IF ;
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO 'Funcionalidade: Objetivo testar a mecanica basica das chamadas API para o Banco de Massa';
  RAISE INFO '  Cenario: Testar a chamada a stored function delivery com uma massa "ETL.LOGIN" ';
  RAISE INFO '    Dado: Um json de GET de uma massa "ETL.LOGIN "';
  RAISE INFO '    Quando: Eu fizer uma chamada a stored function com um json de GET de uma massa "ETL.LOGIN" ';
  RAISE INFO '    Entao: Eu obtenho um json delivery de retorno com codigo de status SUCESSO e um ID de massa valido';
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '';
  RAISE INFO '    Status: %', vStatus;
  RAISE INFO '    Evidencia: %', vEvidencia;
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '';
END first_block $$;


-- ----------------------------------------------------------------------------
\echo 'Fim do script!'
-- ----------------------------------------------------------------------------
