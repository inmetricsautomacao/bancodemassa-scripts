﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_03_function_stage.sql
-- Description:
--
-- Log-changes:
--              * 2018-02-14 JosemarSilva fx_load_files_etlhml add massa + dt-referencia + tipo_bandeira_newelo
--              * 2018-01-09 JosemarSilva ajustando nome das classes
--              * 2017-12-01 JosemarSilva Split transform-and-load into star, sec, redes, etc
--              * 2017-11-26 JosemarSilva bm_node_tipo_classe_massa and deprecating old functions
--              * 2017-11-19 JosemarSilva stage_bm_config_reserva_nu_customer, fx_node_load_star_ec()
--              * 2017-11-15 JosemarSilva add columns (config_tipo_reserva_id, cod_reserva) to stage tables fx_node_transform_star
--              * 2017-09-26 JosemarSilva separando objetos _stage
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_transform_star( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_transform_star( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_transform_star$
DECLARE
  vStmt          VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_transform_star(%) >>>',pAmbiente;
  --
  RAISE info '    a. Transform Cloning stage_bm_config_reserva_nu_customer ...';
  --
  DROP INDEX IF EXISTS fk_config_reserva_nu_customer_01;
  TRUNCATE TABLE stage_bm_config_reserva_nu_customer ;
  INSERT INTO stage_bm_config_reserva_nu_customer ( nu_customer, cod_reserva ) 
  SELECT m.nu_customer, MAX(m.cod_reserva) 
  FROM   bm_config_merchant m
  INNER  JOIN bm_config_tipo_ambiente ta
  ON     ta.id = m.config_tipo_ambiente_id
  WHERE  ta.cod_tipo_ambiente = UPPER(pAmbiente)
  GROUP  BY m.nu_customer;
  CREATE UNIQUE INDEX fk_config_reserva_nu_customer_01 ON stage_bm_config_reserva_nu_customer(nu_customer);
  -- 
  RAISE info '    b. Transform stage_star_tbctmr_terminal_type ...';
  --
  UPDATE stage_star_tbctmr_terminal_type
  SET    num_loja_redes = CASE
                            WHEN cd_terminal_type IN ( 'VM' ) THEN 0001
                            WHEN cd_terminal_type IN ( 'LK' ) THEN 9000
                            WHEN cd_terminal_type IN ( 'VX' ) THEN 9200
                            WHEN cd_terminal_type IN ( 'TF' ) THEN 9300
                            WHEN cd_terminal_type IN ( 'VX' ) THEN 9200
                            WHEN cd_terminal_type IN ( 'GX' ) THEN 9400
                            WHEN cd_terminal_type IN ( 'GZ' ) THEN 9400
                          END;
  -- 
  RAISE info '    c. Reindex ...';
  --
  --
  RAISE info '<<< fx_node_transform_star() <<<';
  --
END;
$fx_node_transform_star$ 
LANGUAGE plpgsql;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_transform_sec( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_transform_sec( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_transform_sec$
DECLARE
  vStmt          VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_transform_sec() >>>';
  --
  RAISE info '    a. Transform, index and foreign key on stage mainframe sec at01 ...';
  --
  DROP INDEX IF EXISTS fk_stage_mainframe_sec_at01_01;
  BEGIN ALTER TABLE stage_mainframe_sec_at01 ADD COLUMN fk_estab BIGINT; EXCEPTION WHEN OTHERS THEN NULL; END;
  UPDATE stage_mainframe_sec_at01 SET fk_estab = (CASE WHEN LTRIM(RTRIM(REPLACE(estab,' ', ''))) = '' THEN NULL ELSE LTRIM(RTRIM(REPLACE(estab,' ', ''))) END)::BIGINT;
  CREATE INDEX fk_stage_mainframe_sec_at01_01 ON stage_mainframe_sec_at01(fk_estab);
  --
  RAISE info '    b. Transform, index and foreign key on stage mainframe sec at02 ...';
  --
  DROP INDEX IF EXISTS fk_stage_mainframe_sec_at02_01;
  BEGIN ALTER TABLE stage_mainframe_sec_at02 ADD COLUMN fk_estab BIGINT; EXCEPTION WHEN OTHERS THEN NULL; END;
  UPDATE stage_mainframe_sec_at02 SET fk_estab = (CASE WHEN LTRIM(RTRIM(REPLACE(estab,' ', ''))) = '' THEN NULL ELSE LTRIM(RTRIM(REPLACE(estab,' ', ''))) END)::BIGINT;
  CREATE INDEX fk_stage_mainframe_sec_at02_01 ON stage_mainframe_sec_at02(fk_estab);
  --
  RAISE info '<<< fx_node_transform_sec() <<<';
  --
END;
$fx_node_transform_sec$ 
LANGUAGE plpgsql;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_transform_redes( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_transform_redes( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_transform_redes$
DECLARE
  vStmt          VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_transform_redes() >>>';
  --
  RAISE info '    a. Transform g124';
  DROP INDEX IF EXISTS fk_stage_mainframe_redes_g124_01;
  BEGIN ALTER TABLE stage_mainframe_redes_g124 ADD COLUMN fk_codestb_nu_customer   BIGINT; EXCEPTION WHEN OTHERS THEN NULL; END;
  BEGIN ALTER TABLE stage_mainframe_redes_g124 ADD COLUMN fk_codestb_numloja       INTEGER; EXCEPTION WHEN OTHERS THEN NULL; END;
  UPDATE stage_mainframe_redes_g124 SET fk_codestb_nu_customer = SUBSTR(codestb,01,12)::BIGINT;
  UPDATE stage_mainframe_redes_g124 SET fk_codestb_numloja     = SUBSTR(codestb,16,04)::INTEGER;
  CREATE INDEX fk_stage_mainframe_redes_g124_01 ON stage_mainframe_redes_g124(fk_codestb_nu_customer);
  --
  RAISE info '    b. Transform g125';
  DROP INDEX IF EXISTS fk_stage_mainframe_redes_g125_01;
  BEGIN ALTER TABLE stage_mainframe_redes_g125 ADD COLUMN fk_codestb_nu_customer   BIGINT; EXCEPTION WHEN OTHERS THEN NULL; END;
  BEGIN ALTER TABLE stage_mainframe_redes_g125 ADD COLUMN fk_codestb_numloja       INTEGER; EXCEPTION WHEN OTHERS THEN NULL; END;
  UPDATE stage_mainframe_redes_g125 SET fk_codestb_nu_customer = SUBSTR(codestb,01,12)::BIGINT;
  UPDATE stage_mainframe_redes_g125 SET fk_codestb_numloja     = SUBSTR(codestb,16,04)::INTEGER;
  CREATE INDEX fk_stage_mainframe_redes_g125_01 ON stage_mainframe_redes_g125(fk_codestb_nu_customer);
  --
  RAISE info '    b. Transform g126';
  DROP INDEX IF EXISTS fk_stage_mainframe_redes_g126_01;
  BEGIN ALTER TABLE stage_mainframe_redes_g126 ADD COLUMN fk_codestb_nu_customer   BIGINT; EXCEPTION WHEN OTHERS THEN NULL; END;
  BEGIN ALTER TABLE stage_mainframe_redes_g126 ADD COLUMN fk_codestb_numloja       INTEGER; EXCEPTION WHEN OTHERS THEN NULL; END;
  UPDATE stage_mainframe_redes_g126 SET fk_codestb_nu_customer = codestb::BIGINT;
  UPDATE stage_mainframe_redes_g126 SET fk_codestb_numloja     = codestbfilial::INTEGER;
  CREATE INDEX fk_stage_mainframe_redes_g126_01 ON stage_mainframe_redes_g126(fk_codestb_nu_customer);
  --
  RAISE info '<<< fx_node_transform_redes() <<<';
  --
END;
$fx_node_transform_redes$ 
LANGUAGE plpgsql;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_transform_simutermweb( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_transform_simutermweb( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_transform_simutermweb$
DECLARE
  vStmt          VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_transform_simutermweb() >>>';
  -- 
  RAISE info '    a. Transform, index and foreign key on stage_simuterm_lote_transacao ...';
  --
  UPDATE stage_simutermweb_lote_transacao
  SET    star_nu_customer = (SUBSTR(cod_estab, 2, length(cod_estab) - 5 ))::bigint;
  --
  DROP INDEX IF EXISTS idx_simutermlotetransacao_starnucustomer;
  DROP INDEX IF EXISTS fk_simutermlotetransacao_startransaction;
  --
  CREATE INDEX idx_simutermlotetransacao_starnucustomer ON stage_simutermweb_lote_transacao( star_nu_customer );
  --
  RAISE info '<<< fx_node_transform_simutermweb() <<<';
  --
END;
$fx_node_transform_simutermweb$ 
LANGUAGE plpgsql;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_load_star_ec( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_star_ec( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_load_star_ec$
DECLARE
  vClasseMassaId INTEGER;
  vClasseMassa   VARCHAR(200);
  vCount         INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_load_star_ec() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  -- Busca/Cria id da classe 'ETL.EC' ...
  --
  vClasseMassaId := NULL;
  vClasseMassa   := 'ETL.EC';
  --
  BEGIN
    INSERT INTO bm_node_classe_massa ( node_tipo_classe_massa_id, classe_massa, descricao, controle_uso_status_massa_id )
    VALUES ( 1 /* 1: Classe Base */, vClasseMassa, vClasseMassa, 1 /* 1: Disponivel para Reuso*/ );
  EXCEPTION
    WHEN unique_violation THEN
      NULL; -- OK!
  END;
  --
  SELECT id  
  INTO   vClasseMassaId 
  FROM   bm_node_classe_massa 
  WHERE  classe_massa = vClasseMassa;
  --
  RAISE info '    b. Clean-up tables: bm_instancia_massa (%) ...', vClasseMassa;
  --
  DELETE FROM bm_node_instancia_massa WHERE node_classe_massa_id = vClasseMassaId;
  --
  RAISE info '    c. Transforming Stage: n/a ...';
  --
  RAISE info '    d. Load into bm_node_instancia_massa ...';
  --
  INSERT INTO bm_node_instancia_massa 
  ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
  SELECT CASE
           WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') =  '' ) THEN 
                   0 /* 0: Disponível para Uso */
           WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') <> '' ) THEN 
                   4 /* 4: Reservado Uso restrito "Código Reserva" */
           ELSE
                   3 /* 3: Indisponível Config. Reserva	 */
         END node_status_massa_id,
         vClasseMassaId AS node_classe_massa_id,
         json_build_object
         (
           'tags',    json_build_object
                      (
                        'classe_massa',         'ETL.EC',
                        'cod_reserva',          CASE
                                                  WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') <> '' ) THEN 
                                                        config.cod_reserva
                                                  ELSE
                                                        null
                                                END
                      ),
           'massa', json_build_object
                      (
                        'nu_customer',          stage.nu_customer,
                        'cd_activity_status',   stage.cd_activity_status,
                        'cd_mcc',               stage.cd_mcc,
                        'nm_dba',               stage.nm_dba,
                        --'nm_legal',             stage.nm_legal,
                        'nu_root_cnpj_cpf',     stage.nu_root_cnpj_cpf,
                        'nu_branch_cnpj',       stage.nu_branch_cnpj,
                        'nu_control_cnpj_cpf',  stage.nu_control_cnpj_cpf,
                        'cd_amex_id',           stage.cd_amex_id,
                        'in_debit_balance',     stage.in_debit_balance,
                        'in_active',            stage.in_active,
                        'cd_arv_category',      stage.cd_arv_category,
                        'root_cnpj_node',       json_build_object
                                                (
                                                  'nu_hierarchical_node',   stage.nu_hierarchical_node,
                                                  'nu_root_cnpj_customer',  stage.nu_root_cnpj_customer,
                                                  'nu_root_cnpj_cpf_group', stage.nu_root_cnpj_cpf_group,
                                                  'nm_root_cnpj_cpf_group', stage.nm_root_cnpj_cpf_group,
                                                  'in_individual_customer', stage.in_individual_customer,
                                                  'nu_mod_customer',        stage.nu_mod_customer,
                                                  'sg_business_type',       stage.sg_business_type
                                                ),
                        'financial_chain_node', json_build_object
                                                (
                                                  'nu_main_customer',       stage.nu_main_customer,
                                                  'in_centralized_payment', stage.in_centralized_payment,
                                                  'nm_payment_group',       stage.nm_payment_group
                                                ),
                        'sec_at01', json_build_object
                                                (
                                                  'cd_status_at01',         stage.cd_status_at01,
                                                  'nm_status_at01',       stage.nm_status_at01
                                                ),
                        'config_pmt_group',     (
                                                  SELECT array_to_json(array_agg(row_to_json(t)))
                                                  FROM   (
                                                           SELECT cpg.nu_hierarchical_node,
                                                                  cpg.cd_product,
                                                                  pc.cd_payment_category,
                                                                  pc.dc_payment_category,
                                                                  pm.cd_payment_method,
                                                                  pm.dc_payment_method,
                                                                  ca.cd_card_association,
                                                                  ca.nm_card_association,
                                                                  cca.cd_contract_card_assoc,
                                                                  cca.nm_contract_card_assoc,
                                                                  mb.cd_bank,
                                                                  hb.nm_bank_bacen,
                                                                  mb.nu_agency,
                                                                  mb.nu_digit_control_agency,
                                                                  mb.nu_current_account,
                                                                  nu_digit_control_account,
                                                                  mb.cd_merchant_bank_type,
                                                                  mbt.dc_merchant_bank_type
                                                           FROM   stage_star_tbhrqr_financial_chain_node fcn
                                                           INNER  JOIN stage_star_tbctmr_config_pmt_group cpg
                                                           ON     cpg.nu_hierarchical_node = fcn.nu_hierarchical_node
                                                           INNER  JOIN stage_star_tbctmr_merchant_bank mb
                                                           ON     mb.nu_merchant_bank = cpg.nu_merchant_bank
                                                           INNER  JOIN stage_star_tbctmr_host_bank hb
                                                           ON     hb.cd_bank = mb.cd_bank
                                                           INNER  JOIN stage_star_tbctmr_merchant_bank_type mbt
                                                           ON     mbt.cd_merchant_bank_type = mb.cd_merchant_bank_type
                                                           INNER  JOIN stage_star_tbprdr_product p
                                                           ON     p.cd_product = cpg.cd_product
                                                           INNER  JOIN stage_star_tbprdr_payment_category pc
                                                           ON     pc.cd_payment_category = p.cd_payment_category
                                                           INNER  JOIN stage_star_tbprdr_payment_method pm
                                                           ON     pm.cd_payment_method = p.cd_payment_method
                                                           INNER  JOIN stage_star_tbprdr_card_association ca
                                                           ON     ca.cd_card_association = p.cd_card_association
                                                           INNER  JOIN stage_star_tbctmr_acquirer a
                                                           ON     a.cd_acquirer = p.cd_acquirer
                                                           INNER  JOIN stage_star_tbprdr_contract_card_assoc cca
                                                           ON     cca.cd_contract_card_assoc = p.cd_contract_card_assoc
                                                           WHERE  fcn.nu_main_customer = stage.nu_customer
                                                           AND    config.nu_customer IS NOT NULL
                                                         ) t
                                                ),
                        'config_pmt_merchant',  (
                                                  SELECT array_to_json(array_agg(row_to_json(t)))
                                                  FROM   (
                                                           SELECT cpm.nu_hierarchical_node,
                                                                  cpm.cd_product,
                                                                  pc.cd_payment_category,
                                                                  pc.dc_payment_category,
                                                                  pm.cd_payment_method,
                                                                  pm.dc_payment_method,
                                                                  ca.cd_card_association,
                                                                  ca.nm_card_association,
                                                                  cca.cd_contract_card_assoc,
                                                                  cca.nm_contract_card_assoc,
                                                                  mb.cd_bank,
                                                                  hb.nm_bank_bacen,
                                                                  mb.nu_agency,
                                                                  mb.nu_digit_control_agency,
                                                                  mb.nu_current_account,
                                                                  nu_digit_control_account,
                                                                  mb.cd_merchant_bank_type,
                                                                  mbt.dc_merchant_bank_type
                                                           FROM   stage_star_tbctmr_config_pmt_merchant cpm
                                                           INNER  JOIN stage_star_tbctmr_merchant_bank mb
                                                           ON     mb.nu_merchant_bank = cpm.nu_merchant_bank
                                                           INNER  JOIN stage_star_tbctmr_host_bank hb
                                                           ON     hb.cd_bank = mb.cd_bank
                                                           INNER  JOIN stage_star_tbctmr_merchant_bank_type mbt
                                                           ON     mbt.cd_merchant_bank_type = mb.cd_merchant_bank_type
                                                           INNER  JOIN stage_star_tbprdr_product p
                                                           ON     p.cd_product = cpm.cd_product
                                                           INNER  JOIN stage_star_tbprdr_payment_category pc
                                                           ON     pc.cd_payment_category = p.cd_payment_category
                                                           INNER  JOIN stage_star_tbprdr_payment_method pm
                                                           ON     pm.cd_payment_method = p.cd_payment_method
                                                           INNER  JOIN stage_star_tbprdr_card_association ca
                                                           ON     ca.cd_card_association = p.cd_card_association
                                                           INNER  JOIN stage_star_tbctmr_acquirer a
                                                           ON     a.cd_acquirer = p.cd_acquirer
                                                           INNER  JOIN stage_star_tbprdr_contract_card_assoc cca
                                                           ON     cca.cd_contract_card_assoc = p.cd_contract_card_assoc
                                                           WHERE  cpm.nu_customer = stage.nu_customer
                                                           AND    config.nu_customer IS NOT NULL
                                                         ) t
                                                )
                      )
         ) AS json_massa,
         current_timestamp AS datahora_carga
  FROM   (
           SELECT m.nu_customer,
                  m.cd_activity_status,
                  m.cd_mcc,
                  m.nm_dba,
                  m.nm_legal,
                  m.nu_root_cnpj_cpf,
                  m.nu_branch_cnpj,
                  m.nu_control_cnpj_cpf,
                  m.cd_amex_id,
                  m.in_debit_balance,
                  m.in_active,
                  m.cd_arv_category,
                  cc.nu_hierarchical_node,
                  cc.nu_customer AS nu_root_cnpj_customer,
                  cc.nu_root_cnpj_cpf AS nu_root_cnpj_cpf_group,
                  cc.nm_root_cnpj_cpf_group,
                  cc.in_individual_customer,
                  cc.nu_mod_customer,
                  cc.sg_business_type,
                  fc.nu_main_customer,
                  fc.in_centralized_payment,
                  fc.nm_payment_group,
                  SUBSTRING(RTRIM(LTRIM(SUBSTRING(at01.status,1,60))),1,7)  AS cd_status_at01,
                  SUBSTRING(RTRIM(LTRIM(SUBSTRING(at01.status,1,60))),9,60) AS nm_status_at01
           FROM   stage_star_tbctmr_merchant m
           INNER  JOIN stage_star_tbhrqr_merchant_node mn
           ON     m.nu_customer = mn.nu_customer
           INNER  JOIN stage_star_tbhrqr_hierarchy fch
           ON     mn.nu_hierarchical_node = fch.nu_child_hierarchical_node
           INNER  JOIN stage_star_tbhrqr_hierarchy cch
           ON     fch.nu_parent_hierarchical_node = cch.nu_child_hierarchical_node
           INNER  JOIN stage_star_tbhrqr_root_cnpj_node cc
           ON     cch.nu_parent_hierarchical_node = cc.nu_hierarchical_node
           INNER  JOIN stage_star_tbhrqr_financial_chain_node fc
           ON     fch.nu_parent_hierarchical_node = fc.nu_hierarchical_node
           LEFT   OUTER JOIN stage_mainframe_sec_at01 at01
           ON     m.nu_customer = at01.fk_estab
         ) AS stage
  LEFT   OUTER JOIN stage_bm_config_reserva_nu_customer config
  ON     stage.nu_customer = config.nu_customer
  WHERE  1=1;
  --
  RAISE info '<<< fx_node_load_star_ec() <<<';
  --
END;
$fx_node_load_star_ec$ 
LANGUAGE plpgsql ;



-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_load_sec_ec( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_sec_ec( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_load_sec_ec$
DECLARE
  vClasseMassaId INTEGER;
  vClasseMassa   VARCHAR(200);
  vCount         INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_load_sec_ec() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  -- Busca/Cria id da classe 'ETL.EC.SEC' ...
  --
  vClasseMassaId := NULL;
  vClasseMassa   := 'ETL.EC.SEC';
  --
  BEGIN
    INSERT INTO bm_node_classe_massa ( node_tipo_classe_massa_id, classe_massa, descricao, controle_uso_status_massa_id )
    VALUES ( 1 /* 1: Classe Base */, vClasseMassa, vClasseMassa, 1 /* 1: Disponivel para Reuso*/ );
  EXCEPTION
    WHEN unique_violation THEN
      NULL; -- OK!
  END;
  --
  SELECT id  
  INTO   vClasseMassaId 
  FROM   bm_node_classe_massa 
  WHERE  classe_massa = vClasseMassa;
  --
  RAISE info '    b. Clean-up tables: bm_instancia_massa (%) ...', vClasseMassa;
  --
  DELETE FROM bm_node_instancia_massa WHERE node_classe_massa_id = vClasseMassaId;
  --
  RAISE info '    c. Transforming Stage: n/a ...';
  --
  RAISE info '    d. Load into bm_node_instancia_massa ...';
  --
  INSERT INTO bm_node_instancia_massa 
  ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
  SELECT CASE
           WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') =  '' ) THEN 
                   0 /* 0: Disponível para Uso */
           WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') <> '' ) THEN 
                   4 /* 4: Reservado Uso restrito "Código Reserva" */
           ELSE
                   3 /* 3: Indisponível Config. Reserva	 */
         END node_status_massa_id,
         vClasseMassaId AS node_classe_massa_id,
         json_build_object
         (
           'tags',    json_build_object
                      (
                        'classe_massa',         'ETL.EC.SEC',
                        'cod_reserva',          CASE
                                                  WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') <> '' ) THEN 
                                                        config.cod_reserva
                                                  ELSE
                                                        null
                                                END
                      ),
           'massa', json_build_object
                      (
                        'nu_customer',      stage.nu_customer,
                        'matriz',           stage.matriz,
                        'prin',             stage.prin,
                        'ass',              stage.ass,
                        'cadeia',           stage.cadeia,
                        'piloto',           stage.piloto,
                        'estab',            stage.estab,
                        'tipodepgto',       stage.tipodepgto,
                        'cgc',              stage.cgc,
                        'statusec',         stage.statusec,
                        'statusativo',      stage.statusativo,
                        'rating',           stage.rating,
                        'classe',           stage.classe,
                        'nomefantasia',     stage.nomefantasia,
                        'dom_banc_bco',     stage.dom_banc_bco,
                        'dom_banc_ag',      stage.dom_banc_ag,
                        'dom_banc_cc',      stage.dom_banc_cc,
                        'ramoatividade',    stage.ramoatividade,
                        'multivan',         stage.multivan,
                        'mobpayment',       stage.mobpayment,
                        'cd_status_at01',   stage.cd_status_at01,
                        'nm_status_at01',   stage.nm_status_at01
                      )
         ) AS json_massa,
         current_timestamp AS datahora_carga
  FROM   (
           SELECT at01.fk_estab AS nu_customer,
                  at01.matriz,
                  at01.prin,
                  at01.ass,
                  at01.cadeia,
                  at01.piloto,
                  at01.estab,
                  at01.tipodepgto,
                  at01.cgc,
                  at01.statusec,
                  at01.statusativo,
                  at01.rating,
                  at01.classe,
                  at01.nomefantasia,
                  at01.bco AS dom_banc_bco,
                  at01.ag  AS dom_banc_ag,
                  at01.cc  AS dom_banc_cc,
                  at01.ramoatividade,
                  at01.multivan,
                  at01.mobpayment,
                  at01.status,
                  SUBSTRING(RTRIM(LTRIM(SUBSTRING(at01.status,1,60))),1,7)  AS cd_status_at01,
                  SUBSTRING(RTRIM(LTRIM(SUBSTRING(at01.status,1,60))),9,60) AS nm_status_at01
           FROM   stage_mainframe_sec_at01 at01
           WHERE  fk_estab IS NOT NULL
         ) AS stage
  LEFT   OUTER JOIN stage_bm_config_reserva_nu_customer config
  ON     stage.nu_customer = config.nu_customer
  WHERE  1=1;
  --
  RAISE info '<<< fx_node_load_sec_ec() <<<';
  --
END;
$fx_node_load_sec_ec$ 
LANGUAGE plpgsql ;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_load_redes_ec( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_redes_ec( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_load_redes_ec$
DECLARE
  vClasseMassaId INTEGER;
  vClasseMassa   VARCHAR(200);
  vCount         INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_load_redes_ec() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  -- Busca/Cria id da classe 'ETL.EC.REDES' ...
  --
  vClasseMassaId := NULL;
  vClasseMassa   := 'ETL.EC.REDES';
  --
  BEGIN
    INSERT INTO bm_node_classe_massa ( node_tipo_classe_massa_id, classe_massa, descricao, controle_uso_status_massa_id )
    VALUES ( 1 /* 1: Classe Base */, vClasseMassa, vClasseMassa, 1 /* 1: Disponivel para Reuso*/ );
  EXCEPTION
    WHEN unique_violation THEN
      NULL; -- OK!
  END;
  --
  SELECT id  
  INTO   vClasseMassaId 
  FROM   bm_node_classe_massa 
  WHERE  classe_massa = vClasseMassa;
  --
  RAISE info '    b. Clean-up tables: bm_instancia_massa (%) ...', vClasseMassa;
  --
  DELETE FROM bm_node_instancia_massa WHERE node_classe_massa_id = vClasseMassaId;
  --
  RAISE info '    c. Transforming Stage: n/a ...';
  --
  RAISE info '    d. Load into bm_node_instancia_massa ...';
  --
  INSERT INTO bm_node_instancia_massa 
  ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
  SELECT CASE
           WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') =  '' ) THEN 
                   0 /* 0: Disponível para Uso */
           WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') <> '' ) THEN 
                   4 /* 4: Reservado Uso restrito "Código Reserva" */
           ELSE
                   3 /* 3: Indisponível Config. Reserva	 */
         END node_status_massa_id,
         vClasseMassaId AS node_classe_massa_id,
         json_build_object
         (
           'tags',    json_build_object
                      (
                        'classe_massa',         'ETL.EC.REDES',
                        'cod_reserva',          CASE
                                                  WHEN ( config.nu_customer IS NOT NULL AND COALESCE(config.cod_reserva,'') <> '' ) THEN 
                                                        config.cod_reserva
                                                  ELSE
                                                        null
                                                END
                      ),
           'massa', json_build_object
                      (
                        'nu_customer', stage.nu_customer,
                        'numloja',     stage.numloja,
                        'situacao',    stage.situacao,
                        'nome',        stage.nome,
                        'cgccpf',      stage.cgccpf,
                        'tppessoa',    stage.tppessoa,
                        'mcccode',     stage.mcccode,
                        'multivan',    stage.multivan
                      )
         ) AS json_massa,
         current_timestamp AS datahora_carga
  FROM   (
           SELECT g124.fk_codestb_nu_customer AS nu_customer,
                  g124.fk_codestb_numloja AS numloja,
                  g124.situacao,
                  g124.nome,
                  g124.cgccpf,
                  g124.tppessoa,
                  g124.mcccode,
                  g124.multivan
           FROM   stage_mainframe_redes_g124 g124
           WHERE  fk_codestb_nu_customer IS NOT NULL
         ) AS stage
  LEFT   OUTER JOIN stage_bm_config_reserva_nu_customer config
  ON     stage.nu_customer = config.nu_customer
  WHERE  1=1;
  --
  RAISE info '<<< fx_node_load_redes_ec() <<<';
  --
END;
$fx_node_load_redes_ec$ 
LANGUAGE plpgsql ;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_load_config_login( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_config_login( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_load_config_login$
DECLARE
  vClasseMassaId INTEGER;
  vClasseMassa   VARCHAR(200);
  vCount         INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_load_config_login() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  -- Busca/Cria id da classe 'ETL.LOGIN' ...
  --
  vClasseMassaId := NULL;
  vClasseMassa   := 'ETL.LOGIN';
  --
  BEGIN
    INSERT INTO bm_node_classe_massa ( node_tipo_classe_massa_id, classe_massa, descricao, controle_uso_status_massa_id )
    VALUES ( 1 /* 1: Classe Base */, vClasseMassa, vClasseMassa, 1 /* 1: Disponivel para Reuso*/ );
  EXCEPTION
    WHEN unique_violation THEN
      NULL; -- OK!
  END;
  --
  SELECT id  
  INTO   vClasseMassaId 
  FROM   bm_node_classe_massa 
  WHERE  classe_massa = vClasseMassa;
  --
  RAISE info '    b. Clean-up tables: bm_instancia_massa (%) ...', vClasseMassa;
  --
  DELETE FROM bm_node_instancia_massa WHERE node_classe_massa_id = vClasseMassaId;
  --
  RAISE info '    c. Transforming Stage: n/a ...';
  --
  RAISE info '    d. Load into bm_node_instancia_massa (ETL.LOGIN)...';
  --
  INSERT INTO bm_node_instancia_massa 
  ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
  SELECT CASE
           WHEN ( stage.cod_reserva IS NULL OR stage.cod_reserva <> '' ) THEN 
                   0 /* 0: Disponível para Uso */
           WHEN ( stage.cod_reserva IS NOT NULL AND stage.cod_reserva <> '' ) THEN 
                   4 /* 4: Reservado Uso restrito "Código Reserva" */
           ELSE
                   3 /* 3: Indisponível Config. Reserva	 */
         END node_status_massa_id,
         vClasseMassaId AS node_classe_massa_id,
         json_build_object
         (
           'tags',    json_build_object
                      (
                        'classe_massa',         'ETL.LOGIN',
                        'cod_reserva',          CASE
                                                  WHEN ( stage.cod_reserva IS NOT NULL AND stage.cod_reserva <> '' ) THEN 
                                                        stage.cod_reserva
                                                  ELSE
                                                        null
                                                END
                      ),
           'massa', json_build_object
                      (
                        'cod_tipo_ambiente', stage.cod_tipo_ambiente,
                        'sistema',           stage.sistema,
                        'login_name',        stage.login_name,
                        'login_password',    stage.login_password,
                        'obs',               stage.obs
                      )
         ) AS json_massa,
         current_timestamp AS datahora_carga
  FROM   (
           SELECT ls.config_tipo_reserva_id,
                  ls.config_tipo_ambiente_id,
                  ta.cod_tipo_ambiente,
                  ls.sistema,
                  ls.login_name,
                  ls.login_password,
                  ls.cod_reserva,
                  ls.tags,
                  ls.obs
           FROM   bm_config_login_sistema ls
           INNER  JOIN bm_config_tipo_ambiente ta
           ON     ta.id = ls.config_tipo_ambiente_id
           WHERE  ta.cod_tipo_ambiente = 'HML'
           ) AS stage
  WHERE  1=1;
  --
  RAISE info '<<< fx_node_load_config_login() <<<';
  --
END;
$fx_node_load_config_login$ 
LANGUAGE plpgsql ;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_load_config_subclasses( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_config_subclasses( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_load_config_subclasses$
DECLARE
  vDummy            VARCHAR(1);
BEGIN
  --
  RAISE info '>>> fx_node_load_config_subclasses() >>>';
  --
  RAISE info '    a. Cloning subclasses into classes from configuration ...';
  --
  INSERT INTO bm_node_classe_massa
  (
    classe_massa,
    node_tipo_classe_massa_id,
    descricao,
    controle_uso_status_massa_id,
    json_config_classe_massa
  )
  SELECT ccm.classe_massa,
         COALESCE( ((ccm.json_config::json)->'config'->>'tipo_classe_massa_id')::int, 3 ) 
                                    AS node_tipo_classe_massa_id,
         SUBSTRING(ccm.obs,1,1000)  AS descricao,
         ((ccm.json_config::json)->'config'->>'controle_uso_status_massa_id')::int
                                    AS controle_uso_status_massa_id,
         (ccm.json_config::json)    AS json_config_classe_massa
  FROM   bm_config_classe_massa ccm
  LEFT   OUTER JOIN bm_node_tipo_classe_massa ntcm
  ON     ntcm.id::VARCHAR = ((ccm.json_config::json)->'config'->>'tipo_classe_massa_id')
  LEFT   OUTER JOIN bm_node_classe_massa ncm
  ON     ncm.classe_massa = ccm.json_config->'config'->>'classe_massa_base'
  WHERE  1=1
  AND    ccm.classe_massa NOT IN ( SELECT classe_massa FROM bm_node_classe_massa );
  --
  RAISE info '    b. Cloning json filters from classe base into subclasses ...';
  --
  INSERT INTO bm_node_json_filter ( node_classe_massa_id, node_json_key_datatype_id, json_filter_name, json_filter_key, lookup_table )
  SELECT scm.id AS node_classe_massa_id, node_json_key_datatype_id, json_filter_name, json_filter_key, lookup_table 
  FROM   bm_node_json_filter jf
  INNER  JOIN bm_node_classe_massa cm
  ON     cm.id = node_classe_massa_id
  LEFT   OUTER JOIN bm_node_classe_massa scm
  ON     1 = 1
  WHERE  cm.classe_massa = 'ETL.EC'
  AND    cm.node_tipo_classe_massa_id  = 1 /* Classe Base */
  AND    scm.node_tipo_classe_massa_id = 2 /* Subclasse Criterio Condicao */
  AND    NOT EXISTS
         (
           SELECT 'X' 
           FROM   bm_node_json_filter subquery 
           WHERE  subquery.node_classe_massa_id = scm.id
           AND    subquery.json_filter_name = json_filter_name
         );
  --
  RAISE info '<<< fx_node_load_config_subclasses() <<<';
  --
END;
$fx_node_load_config_subclasses$ 
LANGUAGE plpgsql;



-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_load_lookup_value( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_lookup_value( ) 
RETURNS void AS
$fx_node_load_lookup_value$
DECLARE
  rLoop            RECORD;
  vNodeClasseMassa VARCHAR := '';
  vLookupTable     VARCHAR := '';
  vStmt            VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_load_lookup_value() >>>';
  --
  RAISE info '    a. Clean-up table: bm_node_lookup_value ...';
  TRUNCATE TABLE bm_node_lookup_value;
  --
  RAISE info '    b. Loading Lookup Table values...';
  --
  INSERT INTO bm_node_lookup_value ( lookup_table, lookup_value, lookup_description ) VALUES ( 'STAR.in_*', 'S', 'Sim' );
  INSERT INTO bm_node_lookup_value ( lookup_table, lookup_value, lookup_description ) VALUES ( 'STAR.in_*', 'N', 'Nao' );
  --
  INSERT INTO bm_node_lookup_value ( lookup_table, lookup_value, lookup_description ) 
  SELECT DISTINCT 'STAR.cd_mcc', json_massa->'massa'->>'cd_mcc', json_massa->'massa'->>'cd_mcc' 
  FROM   bm_node_instancia_massa im 
  INNER  JOIN bm_node_classe_massa cm 
  ON     cm.id = im.node_classe_massa_id 
  WHERE  cm.classe_massa = 'ETL.EC';
  --
  INSERT INTO bm_node_lookup_value ( lookup_table, lookup_value, lookup_description ) 
  SELECT 'STAR.cd_activity_status', cd_activity_status, nm_status_activity FROM stage_star_tbctmr_status_activity;
  --
  INSERT INTO bm_node_lookup_value ( lookup_table, lookup_value, lookup_description ) 
  SELECT 'STAR.cd_activity_status', cd_activity_status, nm_status_activity FROM stage_star_tbctmr_status_activity;
  --
  INSERT INTO bm_node_lookup_value ( lookup_table, lookup_value, lookup_description ) 
  SELECT DISTINCT 'STAR.cd_arv_category', json_massa->'massa'->>'cd_arv_category', json_massa->'massa'->>'cd_arv_category' 
  FROM   bm_node_instancia_massa im 
  INNER  JOIN bm_node_classe_massa cm 
  ON     cm.id = im.node_classe_massa_id 
  WHERE  cm.classe_massa = 'ETL.EC';
  --
  INSERT INTO bm_node_lookup_value ( lookup_table, lookup_value, lookup_description ) 
  SELECT DISTINCT 'STAR.sg_business_type', json_massa->'massa'->'root_cnpj_node'->>'sg_business_type', json_massa->'massa'->'root_cnpj_node'->>'sg_business_type'
  FROM   bm_node_instancia_massa im 
  INNER  JOIN bm_node_classe_massa cm 
  ON     cm.id = im.node_classe_massa_id 
  WHERE  cm.classe_massa = 'ETL.EC';
  --
  RAISE info '<<< fx_node_load_lookup_value() <<<';
  --
END;
$fx_node_load_lookup_value$ 
LANGUAGE plpgsql;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_load_json_filter( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_json_filter( ) 
RETURNS void AS
$fx_node_load_json_filter$
DECLARE
  rLoop            RECORD;
  vNodeClasseMassa VARCHAR := '';
  vJsonFilterName  VARCHAR := '';
  vStmt            VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_load_json_filter() >>>';
  --
  RAISE info '    a. Clean-up table: bm_node_json_filter ...';
  TRUNCATE TABLE bm_node_json_filter;
  --
  RAISE info '    b. Loading JSON Filter keys values...';
  --
  INSERT INTO bm_node_json_filter ( node_classe_massa_id, node_json_key_datatype_id, json_filter_name, json_filter_key, lookup_table )
  SELECT cm.id AS node_classe_massa_id, 1 /* 1:Number; 2:String; 3:Boolean; 4:Array; 5:Value; 6:Object */ AS node_json_key_datatype_id, 
         'Numero do EC (nu_customer)' AS json_filter_name, 'json_massa->''massa''->''nu_customer' AS json_filter_key, '' AS lookup_table
  FROM   bm_node_classe_massa cm
  WHERE  classe_massa = 'ETL.EC';
  --
  INSERT INTO bm_node_json_filter ( node_classe_massa_id, node_json_key_datatype_id, json_filter_name, json_filter_key, lookup_table )
  SELECT cm.id AS node_classe_massa_id, 1 /* 1:Number; 2:String; 3:Boolean; 4:Array; 5:Value; 6:Object */ AS node_json_key_datatype_id, 
         'Codigo do MCC do EC (cd_mcc)' AS json_filter_name, 'json_massa->''massa''->''cd_mcc' AS json_filter_key, 'STAR.cd_mcc' AS lookup_table
  FROM   bm_node_classe_massa cm
  WHERE  classe_massa = 'ETL.EC';
  --
  RAISE info '<<< fx_node_load_json_filter() <<<';
  --
END;
$fx_node_load_json_filter$ 
LANGUAGE plpgsql;



-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_reindex( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_reindex() 
RETURNS void AS
$fx_node_post_load$
DECLARE
  vDummy INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_reindex() >>>';
  --
  PERFORM fx_node_create_indexes();
  --
  RAISE info '<<< fx_node_reindex() <<<';
  --  
END;
$fx_node_post_load$ 
LANGUAGE plpgsql ;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_load_files_etlhml( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_load_files_etlhml( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_load_files_etlhml$
DECLARE
  vStmt          VARCHAR(2000);
  vClasseMassaId INTEGER;
  vClasseMassa   VARCHAR(200);
BEGIN
  --
  RAISE info '>>> fx_load_files_etlhml(%) >>>',pAmbiente;
  --
  RAISE info '    a. Initialize ...';
  --
  -- Busca/Cria id da classe 'ETL.FILES.ETLHML' ...
  --
  vClasseMassaId := NULL;
  vClasseMassa   := 'ETL.FILES.ETLHML';
  --
  BEGIN
    INSERT INTO bm_node_classe_massa ( node_tipo_classe_massa_id, classe_massa, descricao, controle_uso_status_massa_id )
    VALUES ( 1 /* 1: Classe Base */, vClasseMassa, vClasseMassa, 1 /* 1: Disponivel para Reuso*/ );
  EXCEPTION
    WHEN unique_violation THEN
      NULL; -- OK!
  END;
  --
  SELECT id  
  INTO   vClasseMassaId 
  FROM   bm_node_classe_massa 
  WHERE  classe_massa = vClasseMassa;
  --
  RAISE info '    b. Clean-up tables: bm_instancia_massa (%) ...', vClasseMassa;
  --
  DELETE FROM bm_node_instancia_massa WHERE node_classe_massa_id = vClasseMassaId;
  --
  RAISE info '    c. Transforming Stage: n/a ...';
  --
  RAISE info '    d. Load into bm_node_instancia_massa ...';
  --
  INSERT INTO bm_node_instancia_massa 
  ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
  SELECT 0 /* 0: Disponível para Uso */ AS node_status_massa_id,
         vClasseMassaId                 AS node_classe_massa_id,
         json_build_object
         (
           'massa',
                   json_build_object
                   (
                     'dt_referencia',                    stage.dt_referencia,
                     'cod_environment',                  stage.cod_environment, 
                     'funcionalidade',                   stage.funcionalidade, 
                     'bandeira',                         stage.bandeira, 
                     'tipo_bandeira_newelo',             stage.tipo_bandeira_newelo,
                     'num_banco',                        stage.num_banco, 
                     'tipo_arquivo_banco_domicilio',     stage.tipo_arquivo_banco_domicilio, 
                     'tipo_remessa_retorno',             stage.tipo_remessa_retorno, 
                     'tipo_arquivo_relatorios_clientes', stage.tipo_arquivo_relatorios_clientes, 
                     'sistema',                          stage.sistema,
                     'stage_file_paths',                 (
                                                           SELECT array_to_json(array_agg(row_to_json(t)))
                                                           FROM   (
                                                                    SELECT stage_file_path
                                                                    FROM   stage_files_etlhml files
                                                                    WHERE  COALESCE(files.dt_referencia, '#null#')                    = COALESCE(stage.dt_referencia, '#null#')
                                                                    AND    COALESCE(files.cod_environment, '#null#')                  = COALESCE(stage.cod_environment, '#null#')
                                                                    AND    COALESCE(files.funcionalidade, '#null#')                   = COALESCE(stage.funcionalidade, '#null#')
                                                                    AND    COALESCE(files.bandeira, '#null#')                         = COALESCE(stage.bandeira, '#null#')
                                                                    AND    COALESCE(files.tipo_bandeira_newelo, '#null#')             = COALESCE(stage.tipo_bandeira_newelo, '#null#')
                                                                    AND    COALESCE(files.num_banco, '#null#')                        = COALESCE(stage.num_banco, '#null#')
                                                                    AND    COALESCE(files.tipo_arquivo_banco_domicilio, '#null#')     = COALESCE(stage.tipo_arquivo_banco_domicilio, '#null#')
                                                                    AND    COALESCE(files.tipo_remessa_retorno, '#null#')             = COALESCE(stage.tipo_remessa_retorno, '#null#')
                                                                    AND    COALESCE(files.tipo_arquivo_relatorios_clientes, '#null#') = COALESCE(stage.tipo_arquivo_relatorios_clientes, '#null#')
                                                                    AND    COALESCE(files.sistema, '#null#')                          = COALESCE(stage.sistema, '#null#')
                                                                  ) t
                                                         )
                   )
         ) AS json_massa,
         current_timestamp AS datahora_carga
  FROM   (
           SELECT dt_referencia, cod_environment, funcionalidade, bandeira, tipo_bandeira_newelo, num_banco, tipo_arquivo_banco_domicilio, tipo_remessa_retorno, tipo_arquivo_relatorios_clientes, sistema
           FROM   stage_files_etlhml
           GROUP  BY dt_referencia, cod_environment, funcionalidade, bandeira, tipo_bandeira_newelo, num_banco, tipo_arquivo_banco_domicilio, tipo_remessa_retorno, tipo_arquivo_relatorios_clientes, sistema
         ) AS stage;
  --
  RAISE info '<<< fx_load_files_etlhml() <<<';
  --  
END;
$fx_load_files_etlhml$ 
LANGUAGE plpgsql ;


-- ----------------------------------------------------------------------------
\echo '[RE]CREATE FUNCTION fx_node_load_config_capirdmdr( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_load_config_capirdmdr( pAmbiente VARCHAR DEFAULT 'HML') 
RETURNS void AS
$fx_node_load_config_capirdmdr$
DECLARE
  vClasseMassaId INTEGER;
  vClasseMassa   VARCHAR(200);
  vCount         INTEGER;
BEGIN
  --
  RAISE info '>>> fx_node_load_config_capirdmdr() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  -- Busca/Cria id da classe 'ETL.CAPIRDMDR' ...
  --
  vClasseMassaId := NULL;
  vClasseMassa   := 'ETL.CAPIRDMDR';
  --
  BEGIN
    INSERT INTO bm_node_classe_massa ( node_tipo_classe_massa_id, classe_massa, descricao, controle_uso_status_massa_id )
    VALUES ( 1 /* 1: Classe Base */, vClasseMassa, vClasseMassa, 1 /* 1: Disponivel para Reuso*/ );
  EXCEPTION
    WHEN unique_violation THEN
      NULL; -- OK!
  END;
  --
  SELECT id  
  INTO   vClasseMassaId 
  FROM   bm_node_classe_massa 
  WHERE  classe_massa = vClasseMassa;
  --
  RAISE info '    b. Clean-up tables: bm_instancia_massa (%) ...', vClasseMassa;
  --
  DELETE FROM bm_node_instancia_massa WHERE node_classe_massa_id = vClasseMassaId;
  --
  RAISE info '    c. Transforming Stage: n/a ...';
  --
  RAISE info '    d. Load into bm_node_instancia_massa (ETL.CAPIRDMDR)...';
  --
  INSERT INTO bm_node_instancia_massa 
  ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
  SELECT 
         0 /* 0: Disponível para Uso */ AS node_status_massa_id,
         vClasseMassaId AS node_classe_massa_id,
         json_build_object
         (
           'tags',    json_build_object
                      (
                        'classe_massa',         'ETL.CAPIRDMDR',
                        'cod_reserva',          NULL
                      ),
           'massa', (
                                    SELECT array_to_json(array_agg(row_to_json(t1)))
                                    FROM   (
                                             SELECT 
                                                    id AS config_capirdmdr_id,
                                                    '' AS cod_reserva,
                                                    test_case_id,
                                                    test_plan_id,
                                                    test_case_name,
                                                    cycle_folder_path_name,
                                                    dias_anterior,
                                                    nome_projeto,
                                                    nome_lote,
                                                    flag_enable,
                                                    flag_check_status_alm,
                                                    canal_venda,
                                                    tipo_capirdmdr,
                                                    bandeira,
                                                    tags,
                                                    'param',(
                                                              SELECT array_to_json(array_agg(row_to_json(t2)))
                                                              FROM   (
                                                                       SELECT b.chave,
                                                                              b.operador,
                                                                              b.valor_esperado
                                                                       FROM   bm_config_param_capirdmdr b
                                                                       WHERE  b.config_capirdmdr_id = a.id
                                                                     ) t2
                                                            )
                                             FROM   bm_config_capirdmdr a
                                           ) t1
                    )
		   ) AS json_massa,
         current_timestamp AS datahora_carga;
  --
  RAISE info '<<< fx_node_load_config_capirdmdr() <<<';
  --
END;
$fx_node_load_config_capirdmdr$ 
LANGUAGE plpgsql ;


-- ----------------------------------------------------------------------------
\echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
