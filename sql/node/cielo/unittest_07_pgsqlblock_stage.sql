-- ----------------------------------------------------------------------------
-- Filename:
--              unittest_07_pgsqlblock_stage.sql
-- Description:
--              
-- Log-changes:
--              * 2017-09-26 JosemarSilva Develop Unit Test Postgresql
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
\echo 'Início ...'
-- ----------------------------------------------------------------------------

DO $$ 
<<first_block>>
DECLARE
  bException BOOLEAN := FALSE;
  vMsgException VARCHAR := '';  -- SQLERRM, SQLSTATE
  vEvidencia    VARCHAR := '';
  vStatus       VARCHAR := '';
  vJsonResult   JSON;
BEGIN
  --
  BEGIN
    SELECT fx_node_test_data_post( ( '{}' )::json ) INTO vJsonResult;
  EXCEPTION
    WHEN OTHERS THEN
      bException := TRUE;
	  vMsgException := CONCAT('SQLERRM:', SQLERRM, ' - SQLSTATE ', SQLSTATE);
	  RAISE INFO '%', vMsgException;
  END;
  --
  IF NOT bException THEN 
    vStatus := 'Passed' ;
	--
	vEvidencia := vJsonResult;
	IF LENGTH(vEvidencia) > 500 THEN
      vEvidencia := SUBSTR(vEvidencia,1,500) || ' ... ' || SUBSTR(vEvidencia,LENGTH(vEvidencia)-200,200);
	END IF;
	--
	IF vJsonResult->'post_return'->>'status_code' <> '0' THEN
      vStatus := 'Failed - "status_code" <> 0 - {"post_return": "status_code": ... }' ;
	ELSIF vJsonResult->'post_return'->>'id' <> '' THEN
      vStatus := 'Failed - "id" must be null - {"post_return": "id": ... }' ;
	END IF;
  ELSE 
    vStatus := 'Failed' ;
  END IF ;
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO 'Funcionalidade: Objetivo testar a mecanica basica das chamadas API para o Banco de Massa';
  RAISE INFO '  Cenario: Testar a chamada a stored function create com um json VAZIO {}';
  RAISE INFO '    Dado: Um json VAZIO';
  RAISE INFO '    Quando: Eu fizer uma chamada a stored function create com um json VAZIO';
  RAISE INFO '    Entao: Eu obtenho com retorno um json com codigo de erro, mensagem de erro esperando atributos classe_massa e id nulo';
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '';
  RAISE INFO '    Status: %', vStatus;
  RAISE INFO '    Evidencia: %', vEvidencia;
  RAISE INFO '';
  RAISE INFO '-----------------------------------------------------------------';
  RAISE INFO '';
END first_block $$;


-- ----------------------------------------------------------------------------
\echo 'Fim do script!'
-- ----------------------------------------------------------------------------
