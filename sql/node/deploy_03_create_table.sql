﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_03_create_table.sql
-- Description:
--              
-- Log-changes:
--              * 2017-11-29 JosemarSilva bm_node_metrica
--              * 2017-11-26 JosemarSilva bm_node_lookup_value drop column node_classe_massa_id
--              * 2017-11-26 JosemarSilva bm_node_tipo_classe_massa
--              * 2017-10-25 JosemarSilva add column lookup_table
--              * 2017-07-11 JosemarSilva Ren bm_node_json_filter.json_key_datatype_id  node_json_key_datatype_id, lookup_value size
--              * 2017-06-25 JosemarSilva bm_node_log_falha, bm_node_lookup_value, bm_node_json_filter
--              * 2017-05-06 JosemarSilva prefixo node_
--              * 2017-05-01 JosemarSilva Drop bm_tipo_classe_massa
--              * 2017-04-20 JosemarSilva Drop column hastag
--              * 2017-04-19 JosemarSilva Hashtag cant be AK, must be only index 
--              * 2017-04-14 JosemarSilva length(classe_massa) = 300
--              * 2017-03-14 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_status_massa ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_status_massa (
  id SMALLINT NOT NULL,
  status_massa VARCHAR(100) NOT NULL,
  ind_disponibilidade SMALLINT NOT NULL,
  CONSTRAINT pk_status_massa PRIMARY KEY(id)
);

ALTER TABLE bm_node_status_massa ADD CONSTRAINT ak_status_massa UNIQUE (status_massa);


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_tipo_classe_massa ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_tipo_classe_massa (
  id SMALLINT NOT NULL,
  tipo_classe_massa VARCHAR(30) NOT NULL,
  CONSTRAINT pk_bm_node_tipo_classe_massa PRIMARY KEY(id)
);

ALTER TABLE bm_node_tipo_classe_massa ADD CONSTRAINT ak_tipo_classe_massa UNIQUE (tipo_classe_massa);


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_classe_massa ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_classe_massa (
  id SERIAL NOT NULL,
  node_tipo_classe_massa_id SMALLINT NOT NULL,
  classe_massa VARCHAR(200) NOT NULL,
  descricao VARCHAR(2000) NULL,
  controle_uso_status_massa_id SMALLINT NULL,
  json_config_classe_massa JSONB NULL,
  CONSTRAINT pk_classe_massa PRIMARY KEY(id)
);

ALTER TABLE bm_node_classe_massa ADD CONSTRAINT ak_classe_massa UNIQUE (classe_massa);


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_instancia_massa ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_instancia_massa (
  id SERIAL NOT NULL ,
  node_status_massa_id SMALLINT NOT NULL,
  node_classe_massa_id INTEGER NOT NULL,
  json_massa JSONB NULL,
  datahora_carga TIMESTAMP NOT NULL,
  qtde_uso INTEGER NULL,
  datahora_primeiro_uso TIMESTAMP NULL,
  datahora_ultimo_uso TIMESTAMP NULL,
  CONSTRAINT pk_instancia_massa PRIMARY KEY(id)
);


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_log_falha ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_log_falha (
  id SERIAL NOT NULL,
  datahora_solic_uso TIMESTAMP NOT NULL,
  json_solic_uso JSON NULL,
  CONSTRAINT pk_node_log_falha PRIMARY KEY(id)
);


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_lookup_value ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_lookup_value (
  id SERIAL NOT NULL,
  lookup_table  VARCHAR(30) NOT NULL,
  lookup_value  VARCHAR(200) NULL,
  lookup_description VARCHAR(1000) NULL,
  CONSTRAINT pk_node_lookup_value PRIMARY KEY(id)
);

ALTER TABLE bm_node_lookup_value ADD CONSTRAINT ak_node_lookup_value UNIQUE (lookup_table, lookup_value);


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_json_filter ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_json_filter (
  id SERIAL NOT NULL,
  node_classe_massa_id INTEGER NOT NULL,
  node_json_key_datatype_id SMALLINT NOT NULL,
  json_filter_name VARCHAR(200) NOT NULL,
  json_filter_key VARCHAR(200) NOT NULL,
  lookup_table  VARCHAR(30) NOT NULL,
  CONSTRAINT pk_node_json_filter PRIMARY KEY(id)
);

ALTER TABLE bm_node_json_filter ADD CONSTRAINT ak_node_json_filter UNIQUE (node_classe_massa_id, json_filter_name );


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_json_key_datatype ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_json_key_datatype (
  id SMALLINT NOT NULL,
  json_key_datatype VARCHAR(30) NOT NULL,
  obs VARCHAR(100) NULL,
  CONSTRAINT pk_node_json_key_datatype PRIMARY KEY(id)
);


-- ----------------------------------------------------------------------------
-- \echo 'bm_node_metrica ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_node_metrica (
  id                   SERIAL     NOT NULL,
  datahora             TIMESTAMP  NOT NULL,
  node_classe_massa_id INTEGER        NULL,
  acao                 VARCHAR(5) NOT NULL,  -- 'GET', 'POST', 'PUT'
  cod_status           SMALLINT   NOT NULL,
  qtde                 INTEGER    NOT NULL,
  CONSTRAINT pk_bm_node_metrica PRIMARY KEY(id)
);


-- ----------------------------------------------------------------------------
-- \echo 'Fim do script!'
-- ----------------------------------------------------------------------------
