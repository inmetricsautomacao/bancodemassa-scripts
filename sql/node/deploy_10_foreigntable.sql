﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_10_foreigntable.sql
-- Description:
--
-- Log-changes:
--              * 2017-11-26 JosemarSilva bm_config_classe_massa
--              * 2017-09-26 JosemarSilva separando _stage
--              * 2017-07-09 JosemarSilva ADD cod_reserva
--              * 2017-06-25 JosemarSilva ADD bm_config_reserva e bm_config_tipo_reserva
--              * 2017-06-18 JosemarSilva 
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
-- \echo 'bm_cluster_config ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_cluster_config (
  id INTEGER NOT NULL,
  cluster_content_type_id smallint NOT NULL,
  cluster_status_id smallint NOT NULL,
  cluster_environment_type_id smallint NOT NULL,
  url_rest_service character varying(100) NOT NULL,
  db_host character varying(100) NOT NULL,
  db_port integer NOT NULL,
  db_name character varying(30) NOT NULL,
  db_username character varying(30) NOT NULL,
  db_password character varying(30) NOT NULL,
  dt_created timestamp without time zone,
  dt_delivered timestamp without time zone,
  metric BIGINT,
  obs character varying(2000)
)
SERVER postgres_fdw_cluster_server;


-- ----------------------------------------------------------------------------
-- \echo 'bm_cluster_status ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_cluster_status (
  id smallint NOT NULL,
  cluster_status character varying(30) NOT NULL,
  description character varying(100)
)
SERVER postgres_fdw_cluster_server;



-- ----------------------------------------------------------------------------
-- \echo 'bm_cluster_environment_type ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_cluster_environment_type (
  id smallint NOT NULL,
  cluster_environment_type character varying(30) NOT NULL,
  description character varying(100)
)
SERVER postgres_fdw_cluster_server;


-- ----------------------------------------------------------------------------
-- \echo 'bm_cluster_content_type ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_cluster_content_type (
  id smallint NOT NULL,
  cluster_content_type character varying(30) NOT NULL,
  description character varying(100)
)
SERVER postgres_fdw_cluster_server;


-- ----------------------------------------------------------------------------
-- \echo 'bm_config_tipo_reserva ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_config_tipo_reserva (
  id SMALLINT NOT NULL,
  config_tipo_reserva VARCHAR(100) NULL,
  obs VARCHAR(1000) NULL
)
SERVER postgres_fdw_config_server;


-- ----------------------------------------------------------------------------
-- \echo 'bm_config_tipo_ambiente ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_config_tipo_ambiente (
  id INTEGER NOT NULL,
  cod_tipo_ambiente VARCHAR(10) NULL,
  tipo_ambiente VARCHAR(30) NULL,
  obs VARCHAR(1000) NULL
)
SERVER postgres_fdw_config_server;



-- ----------------------------------------------------------------------------
-- \echo 'bm_config_classe_massa ...'
-- ----------------------------------------------------------------------------
CREATE FOREIGN TABLE bm_config_classe_massa (
  id SERIAL NOT NULL,
  classe_massa VARCHAR(200) NOT NULL,
  obs VARCHAR(1000) NULL,
  json_config JSONB NULL
)
SERVER postgres_fdw_config_server;


-- ----------------------------------------------------------------------------
-- \echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
