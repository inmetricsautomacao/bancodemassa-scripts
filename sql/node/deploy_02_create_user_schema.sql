﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_02_create_user_schema.sql
-- Description:
--              
-- Log-changes:
--              * 2018-02-04 JosemarSilva Only one user application
--              * 2017-06-18 JosemarSilva role SUPERUSER to root to create extension, etc
--              * 2017-03-14 JosemarSilva
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

--
-- \echo 'Creating Users access ...'
--
CREATE USER bancodemassa  WITH PASSWORD 'bancodemassa';
--

DO
$body$
DECLARE
  num_users integer;
BEGIN
  SELECT COUNT(*) INTO num_users FROM pg_catalog.pg_user WHERE pg_user.usename = 'bancodemassa';
  --
  IF num_users = 0 THEN
    CREATE USER bancodemassa  WITH PASSWORD 'bancodemassa';
  END IF;
END
$body$;
