﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_08_view.sql
-- Description:
--
-- Log-changes:
--              * 2017-11-26 JosemarSilva drop views lookup_value
--              * 2017-10-25 JosemarSilva removendo duplicidade view vw_node_json_filter
--              * 2017-09-27 JosemarSilva ajustando contador status_0
--              * 2017-09-26 JosemarSilva separando _stage
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'vw_node_metric_instancia_massa ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE VIEW vw_node_metric_instancia_massa AS 
SELECT cm.id                   AS id,
       cm.classe_massa         AS classe_massa,
       -- _node_status_massa_*
       grp.qtde                AS qtde,
       grp.qtde_uso            AS qtde_uso,
       grp.datahora_primeiro_uso
                               AS datahora_primeiro_uso,
       grp.datahora_ultimo_uso
                               AS datahora_ultimo_uso,
       -- _node_status_massa_0
       grp.qtde_uso_node_status_massa_0
                               AS qtde_uso_node_status_massa_0,
       grp.datahora_primeiro_uso_node_status_massa_0
                               AS datahora_primeiro_uso_node_status_massa_0,
       grp.datahora_ultimo_uso_node_status_massa_0
                               AS datahora_ultimo_uso_node_status_massa_0,
       -- _node_status_massa_1
       grp.qtde_uso_node_status_massa_1
                               AS qtde_uso_node_status_massa_1,
       grp.datahora_primeiro_uso_node_status_massa_1
                               AS datahora_primeiro_uso_node_status_massa_1,
       grp.datahora_ultimo_uso_node_status_massa_1
                               AS datahora_ultimo_uso_node_status_massa_1,
       -- _node_status_massa_2
       grp.qtde_uso_node_status_massa_2
                               AS qtde_uso_node_status_massa_2,
       grp.datahora_primeiro_uso_node_status_massa_2
                               AS datahora_primeiro_uso_node_status_massa_2,
       grp.datahora_ultimo_uso_node_status_massa_2
                               AS datahora_ultimo_uso_node_status_massa_2,
       -- _node_status_massa_3
       grp.qtde_uso_node_status_massa_3
                               AS qtde_uso_node_status_massa_3,
       grp.datahora_primeiro_uso_node_status_massa_3
                               AS datahora_primeiro_uso_node_status_massa_3,
       grp.datahora_ultimo_uso_node_status_massa_3
                               AS datahora_ultimo_uso_node_status_massa_3,
       -- _node_status_massa_4
       grp.qtde_uso_node_status_massa_4
                               AS qtde_uso_node_status_massa_4,
       grp.datahora_primeiro_uso_node_status_massa_4
                               AS datahora_primeiro_uso_node_status_massa_4,
       grp.datahora_ultimo_uso_node_status_massa_4
                               AS datahora_ultimo_uso_node_status_massa_4
FROM   bm_node_classe_massa    cm
LEFT   OUTER JOIN 
       (
        SELECT im.node_classe_massa_id         AS node_classe_massa_id,
               COUNT( im.id )                  AS qtde,
               -- _node_status_massa_*
               SUM( COALESCE(im.qtde_uso,1) )  AS qtde_uso,
               MIN( im.datahora_primeiro_uso ) AS datahora_primeiro_uso,
               MAX( im.datahora_ultimo_uso )   AS datahora_ultimo_uso,
               -- _node_status_massa_0
               SUM(
                 CASE
                   WHEN node_status_massa_id = 0 THEN 
                        COALESCE(im.qtde_uso,1) 
                   ELSE 0
                 END
               )                               AS qtde_uso_node_status_massa_0,
               MIN(
                 CASE
                   WHEN node_status_massa_id = 0 THEN 
                        datahora_primeiro_uso
                   ELSE NULL
                 END
               )                               AS datahora_primeiro_uso_node_status_massa_0,
               MAX(
                 CASE
                   WHEN node_status_massa_id = 0 THEN 
                        datahora_ultimo_uso
                   ELSE NULL
                 END
               )                               AS datahora_ultimo_uso_node_status_massa_0,
               -- _node_status_massa_1
               SUM(
                 CASE
                   WHEN node_status_massa_id = 1 THEN 
                        COALESCE(im.qtde_uso,0) 
                   ELSE 0
                 END
               )                               AS qtde_uso_node_status_massa_1,
               MIN(
                 CASE
                   WHEN node_status_massa_id = 1 THEN 
                        datahora_primeiro_uso
                   ELSE NULL
                 END
               )                               AS datahora_primeiro_uso_node_status_massa_1,
               MAX(
                 CASE
                   WHEN node_status_massa_id = 1 THEN 
                        datahora_ultimo_uso
                   ELSE NULL
                 END
               )                               AS datahora_ultimo_uso_node_status_massa_1,
               -- _node_status_massa_2
               SUM(
                 CASE
                   WHEN node_status_massa_id = 2 THEN 
                        COALESCE(im.qtde_uso,0) 
                   ELSE 0
                 END
               )                               AS qtde_uso_node_status_massa_2,
               MIN(
                 CASE
                   WHEN node_status_massa_id = 2 THEN 
                        datahora_primeiro_uso
                   ELSE NULL
                 END
               )                               AS datahora_primeiro_uso_node_status_massa_2,
               MAX(
                 CASE
                   WHEN node_status_massa_id = 2 THEN 
                        datahora_ultimo_uso
                   ELSE NULL
                 END
               )                               AS datahora_ultimo_uso_node_status_massa_2,
               -- _node_status_massa_3
               SUM(
                 CASE
                   WHEN node_status_massa_id = 3 THEN 
                        COALESCE(im.qtde_uso,0) 
                   ELSE 0
                 END
               )                               AS qtde_uso_node_status_massa_3,
               MIN(
                 CASE
                   WHEN node_status_massa_id = 3 THEN 
                        datahora_primeiro_uso
                   ELSE NULL
                 END
               )                               AS datahora_primeiro_uso_node_status_massa_3,
               MAX(
                 CASE
                   WHEN node_status_massa_id = 3 THEN 
                        datahora_ultimo_uso
                   ELSE NULL
                 END
               )                               AS datahora_ultimo_uso_node_status_massa_3,
               -- _node_status_massa_4
               SUM(
                 CASE
                   WHEN node_status_massa_id = 4 THEN 
                        COALESCE(im.qtde_uso,0) 
                   ELSE 0
                 END
               )                               AS qtde_uso_node_status_massa_4,
               MIN(
                 CASE
                   WHEN node_status_massa_id = 4 THEN 
                        datahora_primeiro_uso
                   ELSE NULL
                 END
               )                               AS datahora_primeiro_uso_node_status_massa_4,
               MAX(
                 CASE
                   WHEN node_status_massa_id = 4 THEN 
                        datahora_ultimo_uso
                   ELSE NULL
                 END
               )                               AS datahora_ultimo_uso_node_status_massa_4,
               --
               NULL
        FROM   bm_node_instancia_massa im
        GROUP  BY im.node_classe_massa_id
       ) grp
ON     cm.id = grp.node_classe_massa_id
ORDER  BY cm.classe_massa
;



-- ----------------------------------------------------------------------------
-- \echo 'vw_node_json_filter ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE VIEW vw_node_json_filter AS 
SELECT jf.id AS id,
       jf.node_classe_massa_id         AS node_classe_massa_id,
       cm.classe_massa                 AS classe_massa,
       cm.descricao                    AS descricao,
       cm.controle_uso_status_massa_id AS controle_uso_status_massa_id,
       sm.status_massa                 AS status_massa,
       sm.ind_disponibilidade          AS ind_disponibilidade,
       jf.json_filter_name             AS json_filter_name,
       jf.node_json_key_datatype_id    AS node_json_key_datatype_id,
       kd.json_key_datatype            AS json_key_datatype,
       jf.json_filter_key              AS json_filter_key
FROM   bm_node_json_filter  jf,
       bm_node_classe_massa cm,
       bm_node_status_massa sm,
       bm_node_json_key_datatype kd
WHERE  cm.id = jf.node_classe_massa_id
AND    sm.id = cm.controle_uso_status_massa_id
AND    kd.id = node_json_key_datatype_id
;




-- ----------------------------------------------------------------------------
-- \echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
