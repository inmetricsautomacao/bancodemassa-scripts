﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_09_extension_server_user_mapping.sql
-- Description:
--
-- Log-changes:
--              * 2018-03-23 JosemarSilva Adapting script to Amazon RDS
--              * 2018-01-29 JosemarSilva Discontinued root
--              * 2017-10-25 JosemarSilva add mapping for postgres - windows
--              * 2017-09-26 JosemarSilva separando _stage
--              * 2017-06-25 JosemarSilva wrapper for bancodemassaconfig
--              * 2017-06-18 JosemarSilva 
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'Creating extension postgres_fdw ...'
-- ----------------------------------------------------------------------------
CREATE EXTENSION IF NOT EXISTS postgres_fdw ;


-- ----------------------------------------------------------------------------
-- \echo 'Creating server ( postgres_fdw_cluster_server, postgres_fdw_config_server) ...'
-- ----------------------------------------------------------------------------
-- CREATE SERVER postgres_fdw_cluster_server FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '127.0.0.1', dbname 'bmcluster', port '5432');
-- CREATE SERVER postgres_fdw_config_server  FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '127.0.0.1', dbname 'bmconfig',  port '5432');

-- ----------------------------------------------------------------------------
-- \echo 'Creating server ( postgres_fdw_cluster_server, postgres_fdw_config_server) - Amazon RDS ...'
-- ----------------------------------------------------------------------------
-- CREATE SERVER postgres_fdw_cluster_server FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'bmcantina.cfjmox99qzpa.us-east-1.rds.amazonaws.com', dbname 'bmcluster', port '5432');
-- CREATE SERVER postgres_fdw_config_server  FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'bmcantina.cfjmox99qzpa.us-east-1.rds.amazonaws.com', dbname 'bmconfig',  port '5432');


-- ----------------------------------------------------------------------------
-- \echo 'Creating server ( postgres_fdw_cluster_server, postgres_fdw_config_server) - Amazon RDS ...'
-- ----------------------------------------------------------------------------
CREATE SERVER postgres_fdw_cluster_server FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'bmcantina.cckkfv6j5nhc.sa-east-1.rds.amazonaws.com', dbname 'bmcluster', port '5432');
CREATE SERVER postgres_fdw_config_server  FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'bmcantina.cckkfv6j5nhc.sa-east-1.rds.amazonaws.com', dbname 'bmconfig',  port '5432');

-- ----------------------------------------------------------------------------
---- \echo 'Creating user mapping for root ...'
-- ----------------------------------------------------------------------------
--CREATE USER MAPPING FOR root SERVER postgres_fdw_cluster_server OPTIONS (user 'root', password 'root');
--CREATE USER MAPPING FOR root SERVER postgres_fdw_config_server  OPTIONS (user 'root', password 'root');


-- ----------------------------------------------------------------------------
-- \echo 'Creating user mapping for postgres ...'
-- ----------------------------------------------------------------------------
-- CREATE USER MAPPING FOR postgres SERVER postgres_fdw_cluster_server OPTIONS (user 'postgres', password 'postgres');
-- CREATE USER MAPPING FOR postgres SERVER postgres_fdw_config_server  OPTIONS (user 'postgres', password 'postgres');


-- ----------------------------------------------------------------------------
-- \echo 'Creating user mapping for postgres - AMAZON RDS ...'
-- ----------------------------------------------------------------------------
CREATE USER MAPPING FOR bmcantina SERVER postgres_fdw_cluster_server OPTIONS (user 'bmcantina', password 'bmcantina');
CREATE USER MAPPING FOR bmcantina SERVER postgres_fdw_config_server  OPTIONS (user 'bmcantina', password 'bmcantina');

ALTER USER bancodemassa WITH PASSWORD 'B@nc0D3M4ss4';
CREATE USER MAPPING FOR bancodemassa SERVER postgres_fdw_cluster_server OPTIONS (user 'bancodemassa', password 'B@nc0D3M4ss4');
CREATE USER MAPPING FOR bancodemassa SERVER postgres_fdw_config_server  OPTIONS (user 'bancodemassa', password 'B@nc0D3M4ss4');


-- ----------------------------------------------------------------------------
-- \echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
