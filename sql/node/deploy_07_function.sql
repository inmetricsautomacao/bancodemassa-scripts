﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_07_function.sql
-- Description:
--
-- Log-changes:
--              * 2018-05-05 JosemarSilva fx_node_test_data_adhocsqlquery() e fx_jsonb_merge() implementa AD-HOC-SQL 
--              * 2018-01-18 JosemarSilva Correcao bug quando condition = ''
--              * 2017-11-30 JosemarSilva 
--              * 2017-11-29 JosemarSilva metric
--              * 2017-09-26 JosemarSilva separando objetos _stage
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--



-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_truncate_instancia_massa( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_truncate_instancia_massa( ) 
RETURNS void AS
$fx_node_truncate_instancia_massa$
DECLARE
  vStmt          VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_truncate_instancia_massa() >>>';
  --
  RAISE info '    TRUNCATE TABLE bm_node_instancia_massa ...';
  --
  TRUNCATE TABLE bm_node_instancia_massa;
  --
  RAISE info '    DROP INDEX JSON object ...';
  --
  vStmt := 'DROP INDEX idx_json_massa_tags_classe_massa';
  BEGIN
    EXECUTE vStmt;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE info '       - Index already dropped!';
  END;
  --
  RAISE info '<<< fx_node_truncate_instancia_massa() <<<';
  --
END;
$fx_node_truncate_instancia_massa$ 
LANGUAGE plpgsql;



-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_create_indexes( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_create_indexes( ) 
RETURNS void AS
$fx_node_create_indexes$
DECLARE
  vStmt          VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_create_indexes() >>>';
  --
  RAISE info '    CREATE INDEX JSON object ...';
  vStmt := 'CREATE INDEX idx_json_massa_tags_classe_massa ON bm_node_instancia_massa(( json_massa->''' || 'tags' || '''->>''' || 'classe_massa''' || ' ))';
  BEGIN
    EXECUTE vStmt;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE info '       - Index already created!';
  END;
  --
  RAISE info '<<< fx_node_create_indexes() <<<';
  --
END;
$fx_node_create_indexes$ 
LANGUAGE plpgsql;



-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_metric( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_metric( pClasseMassaId INTEGER, pCodStatus SMALLINT, pAcao VARCHAR ) 
RETURNS void AS
$fx_node_metric$
DECLARE
  vDataHora TIMESTAMP;
BEGIN
  --
  RAISE info '>>> fx_node_metric() >>>';
  --
  vDataHora := MAKE_TIMESTAMP( EXTRACT(YEAR FROM NOW())::int, EXTRACT(MONTH FROM NOW())::int, EXTRACT(DAY FROM NOW())::int, EXTRACT(HOUR FROM NOW())::int, 0, 0 );
  --
  UPDATE bm_node_metrica
  SET    qtde = qtde + 1
  WHERE  datahora = vDataHora
  AND    COALESCE(node_classe_massa_id,0) = COALESCE(pClasseMassaId,0)
  AND    cod_status = pCodStatus
  AND    acao = pAcao;
  --
  IF NOT FOUND THEN
    INSERT INTO bm_node_metrica( datahora, node_classe_massa_id, cod_status, acao, qtde)
	VALUES ( vDataHora, pClasseMassaId, pCodStatus, pAcao, 1 /* qtde */ );
  END IF;
  --
  RAISE info '<<< fx_node_metric() <<<';
  --
END;
$fx_node_metric$ 
LANGUAGE plpgsql;




-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_test_data_get( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_test_data_get( pJsonMsg JSON ) 
RETURNS JSONB AS
$fx_node_test_data_get$
DECLARE
  vJsonResult               JSONB;
  vStatusCode               SMALLINT;
  vIdNodeInstanciaMassa     INTEGER;
  vControleUsoStatusMassaId SMALLINT;
  vStatusMessage            VARCHAR(100);
  vClasseMassaId            INTEGER;
  vSubclasseMassaId         INTEGER;
  vClasseMassa              VARCHAR;
  vCodReserva               VARCHAR;
  vForcarReuso              INTEGER;
  vCriterioMassa            VARCHAR;
  vCriterioMassaSubclasse   VARCHAR;
  vNodeTipoClasseMassaId    SMALLINT;
  vJsonMassa                JSONB;
  vJsonConfigClasseMassa    JSONB;
  vStmt                     VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_test_data_get() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  vJsonResult      := pJsonMsg;
  vClasseMassa     := pJsonMsg->'get'->>'classe_massa';
  vCodReserva      := pJsonMsg->'get'->>'cod_reserva';
  vForcarReuso     := pJsonMsg->'get'->>'reuso';
  vCriterioMassa   := pJsonMsg->'get'->>'criterio_massa';
  vStatusCode      := 1 /* 0:ERROR; 1: SUCCESS */;
  vStatusMessage   := 'Success';
  --
  RAISE info '       - ClasseMassa: %', vClasseMassa;
  RAISE info '       - vCodReserva: %', vCodReserva;
  RAISE info '       - vForcarReuso: %', vForcarReuso;
  RAISE info '       - vCriterioMassa: %', vCriterioMassa;
  --
  RAISE info '    b. Check pJsonMsg ( "get"->"classe_massa" ) ...';
  --
  BEGIN
    --
    SELECT id, controle_uso_status_massa_id, node_tipo_classe_massa_id, json_config_classe_massa
    INTO   vClasseMassaId, vControleUsoStatusMassaId, vNodeTipoClasseMassaId, vJsonConfigClasseMassa
    FROM   bm_node_classe_massa
    WHERE  classe_massa = vClasseMassa;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      vClasseMassaId := NULL;
    --
  END;
  --
  RAISE info '       - IdClasseMassa: %', vClasseMassaId;
  RAISE info '       - ControleUsoStatusMassaId: %', vControleUsoStatusMassaId;
  RAISE info '       - vNodeTipoClasseMassaId: %', vNodeTipoClasseMassaId;
  --
  IF vClasseMassaId IS NULL THEN
    RAISE info '       - ERROR: Unrecognized classe_massa';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: Unrecognized classe_massa: '|| COALESCE(vClasseMassa,'') ||'';
  END IF;
  --
  -- Continue if status NOT ERROR ...
  --
  IF vStatusCode != 0 /* 0: ERROR */ THEN
    --
    -- Subclasse ?
    --
    vSubclasseMassaId       := NULL;
    IF vNodeTipoClasseMassaId = 2 /* 2:Subclasse Criterio Condicao */ THEN
      vSubclasseMassaId       := vClasseMassaId;
      BEGIN
        --
        -- vCriterioMassaSubclasse ?
        --
        SELECT id
        INTO   vClasseMassaId
        FROM   bm_node_classe_massa
        WHERE  classe_massa = vJsonConfigClasseMassa->'config'->>'classe_massa_base' ;
        --
        RAISE info '       - vClasseMassaId(Base): %', vClasseMassaId;
        --
        vCriterioMassaSubclasse := COALESCE(vJsonConfigClasseMassa->'config'->>'criterio_massa','');
        RAISE info '       - vCriterioMassaSubclasse: %', vCriterioMassaSubclasse;
        --
        IF vCriterioMassa IS NULL OR vCriterioMassa = '' THEN
          vCriterioMassa = vCriterioMassaSubclasse;
        ELSE
          vCriterioMassa = vCriterioMassaSubclasse +  '  AND ( ' + vCriterioMassa + ')' ;
        END IF;
        --
        RAISE info '       - vCriterioMassa: %', vCriterioMassa;
        --
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          vClasseMassaId := NULL;
        --
      END;
    END IF;
    --
    RAISE info '    c. Query Instancia Massa ...';
    --
    vIdNodeInstanciaMassa := NULL;
    --
    IF vCodReserva IS NOT NULL THEN
      --
      RAISE info '       - lets Try - Reservado Uso restrito "Código Reserva"';
      --
      vStmt := 'SELECT id FROM bm_node_instancia_massa ' ||
                         ' WHERE node_status_massa_id IN ( 0, 1 ) /* 0: Disponível para Uso; 1: Disponível para Reuso */ ' ||
                         ' AND node_classe_massa_id = ' || vClasseMassaId ||
                         ' AND json_massa->''tags''->>''cod_reserva'' =  ''' || vCodReserva || ''' ';
      --
      IF vCriterioMassa IS NOT NULL AND vCriterioMassa != '' THEN
        RAISE info '         + vCriterioMassa is present';
        vStmt := vStmt || ' AND (' || vCriterioMassa || ')';
      END IF;
      vStmt := vStmt || ' ORDER  BY node_status_massa_id, qtde_uso LIMIT  1';
      --
      EXECUTE vStmt INTO vIdNodeInstanciaMassa;
      --
    END IF;
    --
    IF vIdNodeInstanciaMassa IS NULL THEN
      --
      RAISE info '       - 1 th Try - Disponível para Uso, Disponível para Reuso';
      --
      vStmt := 'SELECT id FROM bm_node_instancia_massa ' ||
                         ' WHERE node_status_massa_id IN ( 0, 1 ) /* 0: Disponível para Uso; 1: Disponível para Reuso */ ' ||
                         ' AND node_classe_massa_id = ' || vClasseMassaId;
      --
      IF vCriterioMassa IS NOT NULL AND vCriterioMassa != '' THEN
        RAISE info '         - vCriterioMassa is present';
        vStmt := vStmt || ' AND (' || vCriterioMassa || ')';
      END IF;
      vStmt := vStmt || ' ORDER BY node_status_massa_id, qtde_uso LIMIT  1';
      --
      RAISE info '         - vStmt: %', vStmt;
      --
      EXECUTE vStmt INTO vIdNodeInstanciaMassa;
      --
    END IF;
    --
    RAISE info '       - vIdNodeInstanciaMassa: %', vIdNodeInstanciaMassa;
    --
    IF vIdNodeInstanciaMassa IS NOT NULL THEN
      --
      SELECT json_massa
      INTO   vJsonMassa
      FROM   bm_node_instancia_massa im
      WHERE im.id = vIdNodeInstanciaMassa;
      --
      RAISE info '    d. Update Cluster Config metric ...';
      --
      UPDATE bm_node_instancia_massa
      SET    node_status_massa_id = CASE
                                      WHEN vControleUsoStatusMassaId IS NULL THEN
                                           1 /* 1: Disponível para Reuso */
                                      WHEN vControleUsoStatusMassaId = 0 /* 0: Disponível para Uso */ THEN
                                           -- Não pode! Usou tem que marcar.
                                           1 /* 1: Disponível para Reuso */
                                      ELSE
                                           vControleUsoStatusMassaId
                                    END,
             qtde_uso = COALESCE(qtde_uso,0) + 1,
             datahora_primeiro_uso = CASE 
                                       WHEN datahora_primeiro_uso IS NULL THEN
                                            current_timestamp
                                       ELSE 
                                            datahora_primeiro_uso
                                     END,
             datahora_ultimo_uso   = current_timestamp
      WHERE  id = vIdNodeInstanciaMassa;
      --
    ELSE
      --
      vStatusCode    := 0 /* 0:ERROR; 1: SUCCESS */;
      vStatusMessage := 'ERROR: No Instancia Massa enabled is availabe';
      --
    END IF;
    --
  END IF;
  --
  RAISE info '    e. Json Result  ...';
  --
  RAISE info '       - vStatusCode: %', vStatusCode;
  RAISE info '       - vStatusMessage: %', vStatusMessage;
  --
  vJsonResult = json_build_object
                (
                  'get_return', json_build_object
                                (
                                  'id',             vIdNodeInstanciaMassa,
                                  'status_code',    vStatusCode,
                                  'status_message', vStatusMessage,
                                  'md5',            md5(vIdNodeInstanciaMassa::VARCHAR),
                                  'data',           vJsonMassa
                                )
                );
  --
  -- Register metric ...
  --
  BEGIN
    PERFORM fx_node_metric( COALESCE(vSubclasseMassaId, vClasseMassaId), vStatusCode, 'GET'::VARCHAR );
  EXCEPTION WHEN OTHERS THEN NULL;
  END;
  --
  RAISE info '<<< fx_node_test_data_get() <<<';
  --
  RETURN vJsonResult;
  --
END;
$fx_node_test_data_get$ 
LANGUAGE plpgsql ;



-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_test_data_post( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_test_data_post( pJsonMsg JSON ) 
RETURNS JSONB AS
$fx_node_test_data_post$
DECLARE
  vJsonResult               JSONB;
  vNodeInstanciaMassaId     INTEGER;
  vStatusCode               SMALLINT;
  vStatusMessage            VARCHAR(100);
  vClasseMassaId            INTEGER;
  vClasseMassa              VARCHAR;
  vCodReserva               VARCHAR;
  vJsonMassa                JSONB;
  vStmt                     VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_test_data_post() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  vJsonResult      := pJsonMsg;
  vClasseMassa     := pJsonMsg->'post'->'tags'->>'classe_massa';
  vCodReserva      := pJsonMsg->'post'->'tags'->>'cod_reserva';
  vStatusCode      := 1 /* 0:ERROR; 1: SUCCESS */;
  vStatusMessage   := 'Success';
  --
  RAISE info '       - ClasseMassa: %', vClasseMassa;
  RAISE info '       - vCodReserva: %', vCodReserva;
  --
  RAISE info '    b. Check pJsonMsg ( "post"->"tags"->"classe_massa" ) ...';
  --
  BEGIN
    --
    SELECT id
    INTO   vClasseMassaId
    FROM   bm_node_classe_massa
    WHERE  classe_massa = vClasseMassa;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      vClasseMassaId := NULL;
    --
  END;
  --
  RAISE info '       - IdClasseMassa: %', vClasseMassaId;
  --
  IF vClasseMassaId IS NULL THEN 
    RAISE info '       - ERROR: Unrecognized classe_massa';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: Unrecognized JSON element {"post": {"classe_massa": "' || COALESCE(vClasseMassa,'') || '" }}';
  ELSIF pJsonMsg->'post'->>'massa' IS NULL THEN
    RAISE info '       - ERROR: json->''post''->''massa'' not found';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: JSON element {"post": {"massa": ... }} was not found';
  END IF;
  --
  --
  -- Continue if status NOT ERROR ...
  --
  IF vStatusCode != 0 /* 0: ERROR */ THEN
    --
    RAISE info '    c. Insert Instancia Massa ...';
    --
    -- Build Json Object ..
    --
    vJsonMassa = json_build_object
                  (
                    'tags', json_build_object
                                  (
                                    'classe_massa', vClasseMassa,
                                    'cod_reserva',  vCodReserva
                                  ),
                    'massa', pJsonMsg->'post'->'massa'
                  );
    --
	INSERT INTO bm_node_instancia_massa ( node_status_massa_id, node_classe_massa_id, json_massa, datahora_carga )
	VALUES ( 0 /* 0: Disponível para Uso */, vClasseMassaId, vJsonMassa, NOW() );
	--
	SELECT CURRVAL('bm_node_instancia_massa_id_seq') INTO vNodeInstanciaMassaId;
	--
  END IF;
  --
  RAISE info '    e. Json Result  ...';
  --
  RAISE info '       - vStatusCode: %', vStatusCode;
  RAISE info '       - vStatusMessage: %', vStatusMessage;
  --
  vJsonResult = json_build_object
                (
                  'post_return', json_build_object
                                (
                                  'id',             vNodeInstanciaMassaId,
                                  'status_code',    vStatusCode,
                                  'status_message', vStatusMessage
                                )
                );
  --
  -- Register metric ...
  --
  BEGIN
    PERFORM fx_node_metric( vClasseMassaId, vStatusCode, 'POST'::VARCHAR );
  EXCEPTION WHEN OTHERS THEN NULL;
  END;
  --
  RAISE info '<<< fx_node_test_data_post() <<<';
  --
  RETURN vJsonResult;
  --
END;
$fx_node_test_data_post$ 
LANGUAGE plpgsql ;



-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_test_data_put( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_test_data_put( pJsonMsg JSON ) 
RETURNS JSONB AS
$fx_node_test_data_put$
DECLARE
  vJsonResult               JSONB;
  vStatusCode               SMALLINT;
  vStatusMessage            VARCHAR(100);
  vIdNodeInstanciaMassa     INTEGER;
  vMd5                      VARCHAR;
  vReuso                    BOOLEAN;
  vCodReserva               VARCHAR;
  vJsonMassa                JSONB;
  vNodeStatusMassaId        SMALLINT := NULL::SMALLINT;
BEGIN
  --
  RAISE info '>>> fx_node_test_data_put() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  vJsonResult           := pJsonMsg;
  vIdNodeInstanciaMassa := pJsonMsg->'put'->>'id';
  vMd5                  := pJsonMsg->'put'->>'md5';
  vReuso                := pJsonMsg->'put'->'update'->>'reuso';
  vCodReserva           := pJsonMsg->'put'->'update'->>'cod_reserva';
  vJsonMassa            := pJsonMsg->'put'->'update'->'massa';
  --
  vStatusCode           := 1 /* 0:ERROR; 1: SUCCESS */;
  vStatusMessage        := 'Success';
  --
  RAISE info '       - vIdNodeInstanciaMassa: %', vIdNodeInstanciaMassa;
  RAISE info '       - vMd5: %', vMd5;
  RAISE info '       - vReuso: %', vReuso;
  RAISE info '       - vCodReserva: %', vCodReserva;
  --
  RAISE info '    b. Check Request ...';
  --
  IF vIdNodeInstanciaMassa IS NULL THEN
    RAISE info '       - ERROR: "put"->"id" is undefined';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: "put"->"id" is undefined';
  ELSIF vMd5 IS NULL THEN
    RAISE info '       - ERROR: "put"->"md5" is undefined';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: "put"->"md5" is undefined';
  ELSIF FALSE THEN -- md5(vIdNodeInstanciaMassa::VARCHAR) = vMd5
    RAISE info '       - ERROR: md5 check failure';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: md5 check failure';
  END IF;
  --
  -- Continue if status NOT ERROR ...
  --
  IF vStatusCode != 0 /* 0: ERROR */ THEN
    --
    RAISE info '    c. Update ...';
	--
    IF vReuso IS NULL THEN 
      vNodeStatusMassaId := NULL;
    ELSIF vReuso THEN 
      vNodeStatusMassaId := 1 /* 1: Disponivel para Reuso */;
    ELSE
      vNodeStatusMassaId := 2 /* 2: Indisponivel para Reuso */;
      IF vCodReserva IS NOT NULL AND vCodReserva <> '' THEN
        vNodeStatusMassaId := 3 /* 3: Indisponivel Config. Reserva */;
	  END IF;
	END IF;
    --
    UPDATE bm_node_instancia_massa
    SET    node_status_massa_id = COALESCE( vNodeStatusMassaId, node_status_massa_id )
    WHERE  id = vIdNodeInstanciaMassa;
	--
    IF NOT FOUND THEN
      RAISE info '       - ERROR: Update not found - Id %', vIdNodeInstanciaMassa ;
      vStatusCode    := 0 /* 0: ERROR */;
      vStatusMessage := 'ERROR: Update not found';
    END IF;
    --
  END IF;
  --
  RAISE info '    e. Json Result  ...';
  --
  RAISE info '       - vStatusCode: %', vStatusCode;
  RAISE info '       - vStatusMessage: %', vStatusMessage;
  --
  vJsonResult = json_build_object
                (
                  'put_return', json_build_object
                                (
                                  'id',             vIdNodeInstanciaMassa,
                                  'status_code',    vStatusCode,
                                  'status_message', vStatusMessage
                                )
                );
  --
  -- Register metric ...
  --
  BEGIN
    PERFORM fx_node_metric( COALESCE(vSubclasseMassaId, vClasseMassaId), vStatusCode, 'PUT'::VARCHAR );
  EXCEPTION WHEN OTHERS THEN NULL;
  END;
  --
  RAISE info '<<< fx_node_test_data_put() <<<';
  --
  RETURN vJsonResult;
  --
END;
$fx_node_test_data_put$ 
LANGUAGE plpgsql ;


-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_jsonb_merge( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_jsonb_merge(jsonb1 JSONB, jsonb2 JSONB)
RETURNS JSONB AS $fx_jsonb_merge$
DECLARE
  result JSONB;
  v RECORD;
BEGIN
  result = (
    SELECT json_object_agg(KEY,value)
    FROM
      (
        SELECT jsonb_object_keys(jsonb1) AS KEY,
              1::int AS jsb,
              jsonb1 -> jsonb_object_keys(jsonb1) AS value
        UNION SELECT jsonb_object_keys(jsonb2) AS KEY,
                    2::int AS jsb,
                    jsonb2 -> jsonb_object_keys(jsonb2) AS value ) AS t1
      );
  RETURN result;
END;
$fx_jsonb_merge$ 
LANGUAGE plpgsql;



-- ----------------------------------------------------------------------------
-- \echo '[RE]CREATE FUNCTION fx_node_test_data_adhocsqlquery( ) ...'
-- ----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fx_node_test_data_adhocsqlquery( pJsonMsg JSON ) 
RETURNS JSONB AS
$fx_node_test_data_adhocsqlquery$
DECLARE
  vJsonResult               JSONB;
  vStatusCode               SMALLINT;
  vStatusMessage            VARCHAR(100);
  vClasseMassaId            INTEGER;
  vClasseMassa              VARCHAR;
  vNodeTipoClasseMassaId    SMALLINT;
  vJsonConfigClasseMassa    JSONB;
  vStmt                     VARCHAR(2000);
BEGIN
  --
  RAISE info '>>> fx_node_test_data_adhocsqlquery() >>>';
  --
  RAISE info '    a. Initialize ...';
  --
  vJsonResult      := pJsonMsg;
  vClasseMassa     := pJsonMsg->'sql'->>'classe_massa';
  vStatusCode      := 1 /* 0:ERROR; 1: SUCCESS */;
  vStatusMessage   := 'Success';
  --
  RAISE info '       - ClasseMassa: %', vClasseMassa;
  --
  RAISE info '    b. Fetch classe_massa attributes ...';
  --
  BEGIN
    --
    SELECT id, node_tipo_classe_massa_id, json_config_classe_massa
    INTO   vClasseMassaId, vNodeTipoClasseMassaId, vJsonConfigClasseMassa
    FROM   bm_node_classe_massa
    WHERE  classe_massa = vClasseMassa;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      vClasseMassaId := NULL;
    --
  END;
  --
  RAISE info '       - IdClasseMassa: %', vClasseMassaId;
  RAISE info '       - vNodeTipoClasseMassaId: %', vNodeTipoClasseMassaId;
  RAISE info '       - vJsonConfigClasseMassa: %', vJsonConfigClasseMassa;
  --
  IF vClasseMassaId IS NULL THEN
    RAISE info '       - ERROR: Unrecognized "classe_massa"';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: Unrecognized "classe_massa": "'|| COALESCE(vClasseMassa,'') ||'" ';
  ELSIF vJsonConfigClasseMassa IS NULL THEN
    RAISE info '       - ERROR: Undefined or is null "json_config_classe_massa"';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: Undefined or is null "json_config_classe_massa"';
  ELSIF vNodeTipoClasseMassaId IS NULL THEN
    RAISE info '       - ERROR: Undefined "node_tipo_classe_massa_id"';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: Undefined "node_tipo_classe_massa_id"';
  ELSIF vNodeTipoClasseMassaId <> 4 /* 4: Ad hoc SQL */ THEN
    RAISE info '       - ERROR: "node_tipo_classe_massa_id" expected value is 4';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: "node_tipo_classe_massa_id" expected value is 4';
  ELSIF vJsonConfigClasseMassa->'config'->'sql'->>'query' IS NULL THEN
    RAISE info '       - ERROR: Undefined or is null "json_config_classe_massa->config->sql->>query"';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: Undefined or is null "json_config_classe_massa->config->sql->>query"';
  ELSIF POSITION( 'AD-HOC-SQL.POSTGRESQL' IN vClasseMassa ) = 0 AND
        POSITION( 'AD-HOC-SQL.ORACLE'     IN vClasseMassa ) = 0 AND
        POSITION( 'AD-HOC-SQL.SQLSERVER'  IN vClasseMassa ) = 0 THEN
    RAISE info '       - ERROR: "pJsonMsg->sql->>classe_massa" expected value are ("AD-HOC-SQL.POSTGRESQL.*", "AD-HOC-SQL.ORACLE.*","AD-HOC-SQL.SQLSERVER.*")';
    vStatusCode    := 0 /* 0: ERROR */;
    vStatusMessage := 'ERROR: "pJsonMsg->sql->>classe_massa" expected value are ("AD-HOC-SQL.POSTGRESQL.*", "AD-HOC-SQL.ORACLE.*","AD-HOC-SQL.SQLSERVER.*")';
  END IF;
  --
  RAISE info '    e. Json Result  ...';
  --
  RAISE info '       - vStatusCode: %', vStatusCode;
  RAISE info '       - vStatusMessage: %', vStatusMessage;
  --
  vJsonResult = fx_jsonb_merge (
                  vJsonResult,
                  json_build_object(
                    'sql_result', json_build_object(
                                    'id',             vClasseMassaId,
                                    'status_code',    vStatusCode,
                                    'status_message', vStatusMessage,
                                    'query',          vJsonConfigClasseMassa->'config'->'sql'->'query'
                                  )
                  )::jsonb
                );
  --
  RAISE info '<<< fx_node_test_data_adhocsqlquery() <<<';
  --
  RETURN vJsonResult;
  --
END;
$fx_node_test_data_adhocsqlquery$ 
LANGUAGE plpgsql ;



-- ----------------------------------------------------------------------------
-- \echo 'Fim do Script!'
-- ----------------------------------------------------------------------------
