﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_04_create_foreignkey.sql
-- Description:
--              
-- Log-changes:
--              * 2018-01-29 JosemarSilva bm_node_lookup_value.node_classe_massa_id discontinued
--              * 2017-11-26 JosemarSilva bm_node_tipo_classe_massa
--              * 2017-07-05 JosemarSilva Foreign Key: bm_node_lookup_value e bm_node_json_filter
--              * 2017-05-06 JosemarSilva prefixo "node_"
--              * 2017-05-01 JosemarSilva Drop bm_tipo_classe_massa
--              * 2017-03-14 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'create constraints foreign key ...'
-- ----------------------------------------------------------------------------
ALTER TABLE bm_node_classe_massa    ADD CONSTRAINT fk_node_statusmassa_classemassa            FOREIGN KEY(controle_uso_status_massa_id) REFERENCES bm_node_status_massa(id);
ALTER TABLE bm_node_classe_massa    ADD CONSTRAINT fk_nodetipoclassemassa_nodeclassemassa     FOREIGN KEY(node_tipo_classe_massa_id) REFERENCES bm_node_tipo_classe_massa(id);


ALTER TABLE bm_node_instancia_massa ADD CONSTRAINT fk_node_classe_massa_instancia_massa       FOREIGN KEY(node_classe_massa_id)              REFERENCES bm_node_classe_massa(id);
ALTER TABLE bm_node_instancia_massa ADD CONSTRAINT fk_node_status_massa_instancia_massa       FOREIGN KEY(node_status_massa_id)              REFERENCES bm_node_status_massa(id);

--ALTER TABLE bm_node_lookup_value    ADD CONSTRAINT fk_nodeclassemassa_nodelookupvalue         FOREIGN KEY (node_classe_massa_id)             REFERENCES bm_node_classe_massa(id);

ALTER TABLE bm_node_json_filter     ADD CONSTRAINT fk_nodeclassemassa_nodejsonfilter          FOREIGN KEY (node_classe_massa_id)             REFERENCES bm_node_classe_massa(id);
ALTER TABLE bm_node_json_filter     ADD CONSTRAINT fk_nodejsonkeydatatype_nodejsonfilter      FOREIGN KEY (node_json_key_datatype_id)             REFERENCES bm_node_json_key_datatype(id);


-- ----------------------------------------------------------------------------
-- \echo 'Fim do script!'
-- ----------------------------------------------------------------------------
