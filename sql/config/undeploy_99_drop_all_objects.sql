﻿-- ----------------------------------------------------------------------------
-- Filename:
--              undeploy_99_drop_all_objects.sql
-- Description:
--              
-- Log-changes:
--              * 2018-01-29 JosemarSilva bm_config_tipo_ambiente
--              * 2017-11-26 JosemarSilva bm_config_classe_massa
--              * 2017-06-25 JosemarSilva Creating
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

--
-- \echo 'List all objects ...'
--
\df
\d

--
-- \echo 'Drop functions ...'
--

--
-- \echo 'Drop tables ...'
--
DROP TABLE bm_config_reserva              CASCADE;
DROP TABLE bm_config_tipo_ambiente        CASCADE;
DROP TABLE bm_config_tipo_reserva         CASCADE;
DROP TABLE bm_config_classe_massa         CASCADE;

--
-- \echo 'List all remaining objects (should be nothing) ...'
--
\df
\d

--
-- \echo 'Fim!'
--
