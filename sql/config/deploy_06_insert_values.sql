﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_06_insert_values.sql
-- Description:
--              
-- Log-changes:
--              * 2017-06-25 JosemarSilva Creating
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'insert into bm_cluster_status values ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_tipo_reserva(id, config_tipo_reserva, obs ) VALUES ( 1, 'Disponivel p/ uso no Banco de Massa', NULL);
INSERT INTO bm_config_tipo_reserva(id, config_tipo_reserva, obs ) VALUES ( 2, 'Reservado p/ restrito "Codigo Reserva"', NULL);
INSERT INTO bm_config_tipo_reserva(id, config_tipo_reserva, obs ) VALUES ( 3, 'Reservado p/ uso Stage Pipeline', NULL);

INSERT INTO bm_config_tipo_ambiente( cod_tipo_ambiente, tipo_ambiente ) VALUES ( 'HML', 'Homologacao' );
INSERT INTO bm_config_tipo_ambiente( cod_tipo_ambiente, tipo_ambiente ) VALUES ( 'TI',  'Teste Integrado' );
INSERT INTO bm_config_tipo_ambiente( cod_tipo_ambiente, tipo_ambiente ) VALUES ( 'DEV', 'Desenvolvimento' );

