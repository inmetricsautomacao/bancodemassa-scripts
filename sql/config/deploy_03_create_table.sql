﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_03_create_table.sql
-- Description:
--              
-- Log-changes:
--              * 2017-12-13 JosemarSilva separando configuracoes de cliente
--              * 2017-06-25 JosemarSilva Creating
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'bm_config_tipo_reserva ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_tipo_reserva (
  id SMALLINT NOT NULL,
  config_tipo_reserva VARCHAR(100) NULL,
  obs VARCHAR(1000) NULL,
  PRIMARY KEY(id)
);

ALTER TABLE bm_config_tipo_reserva ADD CONSTRAINT ak_config_tipo_reserva UNIQUE (config_tipo_reserva);


-- ----------------------------------------------------------------------------
-- \echo 'bm_config_tipo_ambiente ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_tipo_ambiente (
  id SERIAL NOT NULL,
  cod_tipo_ambiente VARCHAR(10) NULL,
  tipo_ambiente VARCHAR(30) NULL,
  obs VARCHAR(1000) NULL,
  CONSTRAINT pk_config_tipo_ambiente PRIMARY KEY(id)
);

ALTER TABLE bm_config_tipo_ambiente ADD CONSTRAINT ak_config_tipo_ambiente UNIQUE (cod_tipo_ambiente);


-- ----------------------------------------------------------------------------
-- \echo 'bm_config_classe_massa ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_classe_massa (
  id SERIAL NOT NULL,
  classe_massa VARCHAR(200) NOT NULL,
  obs VARCHAR(1000) NULL,
  json_config JSONB NULL,
  CONSTRAINT pk_config_classe_massa PRIMARY KEY(id)
);

ALTER TABLE bm_config_classe_massa ADD CONSTRAINT ak_config_classe_massa UNIQUE (classe_massa);
