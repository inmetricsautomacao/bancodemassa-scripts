﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_06_insert_values.sql
-- Description:
--              
-- Log-changes:
--              * 2018-05-05 JosemarSilva bm_config_classe_massa ( AD-HOC-SQL.* )
--              * 2018-03-18 JosemarSilva Development
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'insert into bm_config_login_sistema values ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_login_sistema ( config_tipo_ambiente_id, config_tipo_reserva_id, sistema, login_name, login_password, role_name, cod_reserva, tags, obs )
SELECT (SELECT id FROM bm_config_tipo_ambiente WHERE cod_tipo_ambiente = 'HML'),
       1 /* Disponivel p/ uso no Banco de Massa */,
       'FACEBOOK'     sistema,
	   'josemarsilva' login_name,
	   'Secret@123'   login_password,
	   NULL           role_name,
	   NULL           cod_reserva,
	   NULL           tags,
	   ''             obs
;

INSERT INTO bm_config_login_sistema ( config_tipo_ambiente_id, config_tipo_reserva_id, sistema, login_name, login_password, role_name, cod_reserva, tags, obs )
SELECT (SELECT id FROM bm_config_tipo_ambiente WHERE cod_tipo_ambiente = 'HML'),
       1 /* Disponivel p/ uso no Banco de Massa */,
       'CRMPRO'     sistema,
	   'naveenk'    login_name,
	   'test@123'   login_password,
	   NULL         role_name,
	   NULL         cod_reserva,
	   NULL         tags,
	   ''           obs
;



-- ----------------------------------------------------------------------------
-- \echo 'insert into bm_config_classe_massa values ...'
-- ----------------------------------------------------------------------------

INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'AD-HOC-SQL.POSTGRESQL.CURRENT-CONFIG', 'PostgreSql database configuration',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 4 /* 4: Ad hoc SQL */ ,
        'tipo_classe_massa', 'Ad hoc SQL',
        'sql', 
        json_build_object( 
          'query', 'SELECT array_to_json(array_agg(row)) FROM (SELECT current_catalog AS current_database_name, current_user, now() AS current_datetime) row'
        )
      )
  )
);


INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'AD-HOC-SQL.SQLSERVER.CURRENT-CONFIG', 'SqlServer database configuration',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 4 /* 4: Ad hoc SQL */ ,
        'tipo_classe_massa', 'Ad hoc SQL',
        'sql', 
        json_build_object( 
          'query', 'SELECT DB_NAME()  AS [current_database_name], SYSTEM_USER as [current_user], GETDATE()  as [current_datetime]'
        )
      )
  )
);

INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'AD-HOC-SQL.POSTGRESQL.PG-TABLES', 'Postgresql database tables',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 4 /* 4: Ad hoc SQL */ ,
        'tipo_classe_massa', 'Ad hoc SQL',
        'sql', 
        json_build_object( 
          'query', 'SELECT array_to_json(array_agg(row)) FROM (SELECT schemaname, tableowner, tablename FROM pg_tables ORDER BY schemaname, tableowner, tablename) row'
        )
      )
  )
);


INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'AD-HOC-SQL.POSTGRESQL.NO-RESULT', 'Postgresql database no result',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 4 /* 4: Ad hoc SQL */ ,
        'tipo_classe_massa', 'Ad hoc SQL',
        'sql', 
        json_build_object( 
          'query', 'SELECT array_to_json(array_agg(row)) FROM (SELECT schemaname, tableowner, tablename FROM pg_tables WHERE 0 = 1) row'
        )
      )
  )
);


INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'AD-HOC-SQL.POSTGRESQL.SQL-ERROR', 'Postgresql database SQL Error',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 4 /* 4: Ad hoc SQL */ ,
        'tipo_classe_massa', 'Ad hoc SQL',
        'sql', 
        json_build_object( 
          'query', 'SELECT THIS MUST CAUSE AN ERROR - HAY DE HACER ERRORES'
        )
      )
  )
);


INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'AD-HOC-SQL.ORACLE.CURRENT-CONFIG', 'Oracle database configuration',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 4 /* 4: Ad hoc SQL */ ,
        'tipo_classe_massa', 'Ad hoc SQL',
        'sql', 
        json_build_object( 
          'query', 'SELECT sys_context(''userenv'',''instance_name'') current_database, user AS current_user, TO_CHAR(sysdate,''YYYY-MM-DD HH24:MI:SS'') AS current_datetime FROM dual'
        )
      )
  )
);


INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'AD-HOC-SQL.ORACLE.NO-RESULT', 'Oracle database no result',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 4 /* 4: Ad hoc SQL */ ,
        'tipo_classe_massa', 'Ad hoc SQL',
        'sql', 
        json_build_object( 
          'query', 'SELECT * FROM dual WHERE 0 = 1'
        )
      )
  )
);
