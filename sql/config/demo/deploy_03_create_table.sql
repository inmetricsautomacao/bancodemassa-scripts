﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_03_create_table.sql
-- Description:
--              
-- Log-changes:
--              * 2018-03-18 JosemarSilva Development
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
-- \echo 'bm_config_cliente ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_cliente (
  id SERIAL NOT NULL,
  config_tipo_ambiente_id SMALLINT NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  cod_reserva VARCHAR(100) NULL,
  tags  VARCHAR(100) NULL,
  datahora_ini_reserva TIMESTAMP NULL,
  datahora_fim_reserva TIMESTAMP NULL,
  solicitante_reserva VARCHAR(100) NULL,
  obs VARCHAR(100),
  --
  chave_num BIGINT NULL,
  chave_str VARCHAR(30) NULL,
  --
  nome_cliente VARCHAR(100),
  cnpj_cpf bigint NULL,
  CONSTRAINT pk_config_cliente PRIMARY KEY(id)
);

ALTER TABLE bm_config_cliente ADD CONSTRAINT ak_config_merchant UNIQUE (config_tipo_ambiente_id,chave_num,chave_str);


-- ----------------------------------------------------------------------------
-- \echo 'bm_config_login_sistema ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_login_sistema (
  id SERIAL NOT NULL,
  config_tipo_ambiente_id SMALLINT NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  sistema VARCHAR(30) NOT NULL,
  login_name VARCHAR(30) NOT NULL,
  login_password VARCHAR(30) NOT NULL,
  role_name VARCHAR(30) NULL,
  cod_reserva VARCHAR(100) NULL,
  tags  VARCHAR(100) NULL,
  obs VARCHAR(1000) NULL,
  CONSTRAINT pk_config_login_sistema PRIMARY KEY(id)
);

ALTER TABLE bm_config_login_sistema ADD CONSTRAINT ak_config_login_sistema UNIQUE (config_tipo_ambiente_id, sistema, login_name);
