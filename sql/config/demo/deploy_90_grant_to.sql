﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_07_grant_to.sql
-- Description:
--              
-- Log-changes:
--              * 2018-03-27 JosemarSilva Ajuste
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
-- \echo 'GRANT ALL ON ... TO bancodemassa ...'
-- ----------------------------------------------------------------------------
GRANT ALL ON bm_config_cliente           TO bancodemassa;
GRANT ALL ON bm_config_login_sistema     TO bancodemassa;
