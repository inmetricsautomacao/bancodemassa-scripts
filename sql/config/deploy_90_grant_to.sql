﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_07_grant_to.sql
-- Description:
--              
-- Log-changes:
--              * 2018-01-29 JosemarSilva move ./cielo bm_config_merchant e bm_config_login_sistema
--              * 2018-01-29 JosemarSilva discontinued root user
--              * 2017-04-19 JosemarSilva Created
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

-- ----------------------------------------------------------------------------
-- \echo 'GRANT ALL ON ... TO bancodemassa ...'
-- ----------------------------------------------------------------------------
GRANT ALL ON bm_config_tipo_reserva           TO bancodemassa;
GRANT ALL ON bm_config_tipo_ambiente          TO bancodemassa;
GRANT ALL ON bm_config_classe_massa           TO bancodemassa;
