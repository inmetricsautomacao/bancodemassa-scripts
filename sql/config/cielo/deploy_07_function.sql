-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_07_function.sql
-- Description:
--              
-- Log-changes:
--              * 2018-03-08 JosemarSilva Development
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

CREATE OR REPLACE FUNCTION fx_import_capirdmdr( ) RETURNS VARCHAR AS
$fx_import_capirdmdr$
DECLARE
vReturn            VARCHAR := 'fx_import_capirdmdr( ) completed!';
BEGIN
  --
  RAISE info '>>> fx_import_capirdmdr() >>>';
  --
  RAISE info '    cleaning ...';
  --
  DELETE FROM bm_config_param_capirdmdr;
  DELETE FROM bm_config_capirdmdr;
  TRUNCATE bm_config_param_capirdmdr;
  TRUNCATE  bm_config_capirdmdr;
  --
  RAISE info '    loading bm_config_capirdmdr ...';
  INSERT INTO bm_config_capirdmdr 
  (
    config_tipo_ambiente_id, 
    config_tipo_reserva_id, 
    test_case_id, 
    test_plan_id, 
    test_case_name, 
    cycle_folder_path_name, 
    dias_anterior, 
    nome_projeto, 
    nome_lote, 
    flag_enable, 
    flag_check_status_alm, 
    canal_venda, 
    bandeira, 
    tags
  )
  SELECT 
         1 /* HML */ config_tipo_ambiente_id, 
         1 /* Disponivel */ config_tipo_reserva_id, 
         test_case_id, 
         test_plan_id, 
         test_case_name, 
         cycle_folder_path_name, 
         dias_anterior, 
         nome_projeto, 
         nome_lote, 
         CASE WHEN UPPER( enable ) = 'TRUE' OR enable = '1' THEN 1 ELSE 0 END,
         CASE WHEN UPPER( check_status_alm ) = 'TRUE' OR enable = '1' THEN 1 ELSE 0 END,
         canal_de_venda, 
         null,
         null
  FROM   import_capirdmdr
  WHERE  filetype = 'CT' ;
  --
  RAISE info '    loading bm_config_param_capirdmdr ...';
  INSERT INTO bm_config_param_capirdmdr 
  (
    config_capirdmdr_id, 
    chave, 
    operador, 
    valor_esperado, 
    valor_capturado
  )
  SELECT 
         ccim.id,
         i.chave,
         i.operador,
         i.valor_esperado,
         i.valor_capturado
  FROM   import_capirdmdr i
  INNER  JOIN bm_config_capirdmdr ccim
  ON     ccim.test_case_name = i.test_case_name
  WHERE  i.filetype = 'CT-PARAM' ;
  --
  RAISE info '    gathering tipo e bandeira ...';
  UPDATE bm_config_capirdmdr
  SET    tipo_capirdmdr =
         CASE
           WHEN cycle_folder_path_name LIKE '%Captura de Intercambio%'
                THEN
                'IRD'
           WHEN cycle_folder_path_name LIKE '%Captura de MDR%'
                THEN
                'MDR'
           WHEN cycle_folder_path_name LIKE '%Captura de Venda%'
                THEN
                'CAP'
           ELSE
                NULL
         END,
         bandeira = 
         CASE
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\AMEX%'
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%AMEX%'
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%AMEX%'
                THEN
                'AMEX'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\BANESCARD%'
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%BANESCARD%'
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%BANESCARD%'
                THEN
                'BANESCARD'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\CABAL%'
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%CABAL%'
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%CABAL%'
                THEN
                'CABAL'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\CREDSYSTEM%'
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%CREDSYSTEM%'
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%CREDSYSTEM%'
                THEN
                'CREDSYSTEM'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\DINERS%' 
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%DINERS%' 
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%DINERS%' 
                THEN
                'DINERS'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\ELO%' 
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%ELO%' 
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%ELO%' 
                THEN
                'ELO'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\HIPERCARD%' 
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%HIPERCARD%' 
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%HIPERCARD%' 
                THEN
                'HIPERCARD'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\MASTERCARD%' 
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%MASTERCARD%' 
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%MASTERCARD%' 
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%_MASTER%' 
                THEN
                'MASTERCARD'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\SOROCRED%' 
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%SOROCRED%' 
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%SOROCRED%' 
                THEN
                'SOROCRED'
           WHEN UPPER(REPLACE(cycle_folder_path_name,' ', '')) LIKE '%\\VISA%' 
                OR UPPER(REPLACE(test_case_name,' ', '')) LIKE '%VISA%' 
                OR UPPER(REPLACE(nome_lote,' ', '')) LIKE '%VISA%' 
                THEN
                'VISA'
           ELSE
             NULL
         END;
  --
  RAISE info '<<< fx_import_capirdmdr() <<<';
  --
  return vReturn;
  --
END;
$fx_import_capirdmdr$ 
LANGUAGE plpgsql ;
