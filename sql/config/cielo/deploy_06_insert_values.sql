-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_06_insert_values.sql
-- Description:
--              
-- Log-changes:
--              * 2018-02-28 JosemarSilva ETL.FILES.ETLHML.%
--              * 2018-01-18 JosemarSilva Correcao do ->> em ETL.LOGIN.STAR e ETL.LOGIN.STC
--              * 2018-01-18 JosemarSilva Removendo classes ( 'ETL.AMEX.CREDITO.A.VISTA.MULTIPLO.BCO.341', 'ETL.EC.AMEX.CREDITO.A.VISTA.MULTIPLO', 'ETL.EC.BCO-237.AG-1512.CC-000284044.DVCC-8')
--              * 2018-01-09 JosemarSilva ajustando nome das classes
--              * 2017-11-26 JosemarSilva bm_config_classe_massa
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
\echo 'insert into bm_config_classe_massa values ETL.EC.BCO-* ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-001', 'Subclasse ECs domicilio bancario: 001 - Banco do Brasil S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "1" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-021', 'Subclasse ECs domicilio bancario: 021 - BANESTES S.A. Banco do Estado do Espirito Santo',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "21" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-025', 'Subclasse ECs domicilio bancario: 025 - Banco Alfa S.A.

',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "25" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-033', 'Subclasse ECs domicilio bancario: 033 - Banco Santander (Brasil) S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "33" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-041', 'Subclasse ECs domicilio bancario: 041 - Banco do Estado do Rio Grande do Sul S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "41" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-044', 'Subclasse ECs domicilio bancario: 044 - Banco BVA S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "44" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-070', 'Subclasse ECs domicilio bancario: 070 - BRB',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "70" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-082', 'Subclasse ECs domicilio bancario: 082 - Topazio',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "82" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-104', 'Subclasse ECs domicilio bancario: 104 - Caixa Economica Federal',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "104" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-218', 'Subclasse ECs domicilio bancario: 218 - Banco Bonsucesso S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "218" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-237', 'Subclasse ECs domicilio bancario: 237 - Banco Bradesco S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "237" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-246', 'Subclasse ECs domicilio bancario: 246 - Banco ABC Brasil S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "246" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-341', 'Subclasse ECs domicilio bancario: 341 - Itau Unibanco S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "341" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-389', 'Subclasse ECs domicilio bancario: 389 - Banco Mercantil do Brasil S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "389" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-399', 'Subclasse ECs domicilio bancario: 399 - HSBC Bank Brasil S.A. - Banco Múltiplo',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "399" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-422', 'Subclasse ECs domicilio bancario: 422 - Banco Safra S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "422" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-453', 'Subclasse ECs domicilio bancario: 453 - Banco Rural S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "453" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-604', 'Subclasse ECs domicilio bancario: 604 - Banco Industrial do Brasil S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "604" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-634', 'Subclasse ECs domicilio bancario: 634 - Banco Triangulo S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "634" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-655', 'Subclasse ECs domicilio bancario: 655 - Banco Votorantim S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "655" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-707', 'Subclasse ECs domicilio bancario: 707 - Banco Daycoval S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "707" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-745', 'Subclasse ECs domicilio bancario: 745 - Banco Citibank S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "745" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-748', 'Subclasse ECs domicilio bancario: 748 - Banco Cooperativo Sicredi S.A.',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "748" }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BCO-756', 'Subclasse ECs domicilio bancario: 756 - Banco Cooperativo do Brasil S.A. - BANCOOB',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_bank": "756" }]''::jsonb'
      )
  )
);



-- ----------------------------------------------------------------------------
\echo 'insert into bm_config_classe_massa values ETL.LOGIN.* ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.LOGIN.STAR', 'Subclasse login do sistema STAR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.LOGIN',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''sistema'' = ''STAR'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.LOGIN.STC', 'Subclasse login do sistema STC',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.LOGIN',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''sistema'' = ''STC'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.LOGIN.SEC', 'Subclasse login do sistema SEC',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.LOGIN',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''sistema'' = ''SEC'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.LOGIN.REDES', 'Subclasse login do sistema REDES',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.LOGIN',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''sistema'' = ''REDES'''
      )
  )
);



-- ----------------------------------------------------------------------------
\echo 'insert into bm_config_classe_massa values [ETL.EC.BTYPE.*, ETL.EC.ROOT.*, ETL.EC.FINCHAIN, ETL.EC.CONV.* ] ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BTYPE-J', 'Subclasse EC - Business Type - PESSOA JURIDICA',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''root_cnpj_node''->>''sg_business_type'' =  ''J'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.BTYPE-F', 'Subclasse EC - Business Type - PESSOA FISICA',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''root_cnpj_node''->>''sg_business_type'' =  ''F'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.ROOT-GRP-COM', 'Subclasse EC - Root (Raiz) - Grupo Comercial',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''root_cnpj_node''->>''in_individual_customer''  =  ''N'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.ROOT-CLI-INDIV', 'Subclasse EC - Root (Raiz) - Cliente Individual',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''root_cnpj_node''->>''in_individual_customer''  =  ''S'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.FINCHAIN-CENTRALIZ-PMT', 'Subclasse EC - Financial Chain (Cadeia Pagamento) - Centralizada',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''financial_chain_node''->>''in_centralized_payment''  =  ''S'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.FINCHAIN-NOTCENTRALIZ-PMT', 'Subclasse EC - Financial Chain (Cadeia Pagamento) - Nao Centralizada',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''financial_chain_node''->>''in_centralized_payment''  =  ''N'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.CONV-FULLSTAR', 'Subclasse EC - Convivencia - Full STAR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''sec_at01''->>''cd_status_at01''  =  ''AT01-02'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.CONV-FINANC', 'Subclasse EC - Convivencia - Financeira',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''sec_at01''->>''cd_status_at01''  =  ''AT01-99'' AND json_massa->''massa''->''sec_at01''->>''nm_status_at01'' LIKE ''%ESTABELECIMENTO%EM%CONVIVENCIA%FINANCEIRA%'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.CONV-CADASTRAL', 'Subclasse EC - Convivencia - Cadastral',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''sec_at01''->>''cd_status_at01'' = ''AT01-99'' AND json_massa->''massa''->''sec_at01''->>''nm_status_at01'' LIKE ''%ESTABELECIMENTO%MIGRADO%PARA%STAR%'''
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.CONV-UNKNOWN', 'Subclasse EC - Convivencia - Nao determinada',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''sec_at01''->>''cd_status_at01'' IS NULL'
      )
  )
);


-- ----------------------------------------------------------------------------
\echo 'insert into bm_config_classe_massa values [ETL.EC.PRD-*] ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO', 'Subclasse EC - Produto Payment Category - Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO', 'Subclasse EC - Produto Payment Category - Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP', 'Subclasse EC - Produto Payment Category - Credito Especilizado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PM-AVISTA', 'Subclasse EC - Produto Payment Method - A Vista',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PM-PARC-LOJA', 'Subclasse EC - Produto Payment Method - Parcelado Loja',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PM-PARC-BCO', 'Subclasse EC - Produto Payment Method - Parcelado Banco',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-VISA', 'Subclasse EC - Produto Card Association - VISA',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-MASTERCARD', 'Subclasse EC - Produto Card Association - MASTERCARD',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-AMEX', 'Subclasse EC - Produto Card Association - AMEX',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-SOROCRED', 'Subclasse EC - Produto Card Association - SOROCRED',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-ELO', 'Subclasse EC - Produto Card Association - ELO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-DINERS', 'Subclasse EC - Produto Card Association - DINERS',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-BANESCARD', 'Subclasse EC - Produto Card Association - BANESCARD',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-CABAL', 'Subclasse EC - Produto Card Association - CABAL',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-CREDSYSTEM', 'Subclasse EC - Produto Card Association - CREDSYSTEM',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CA-HIPERCARD', 'Subclasse EC - Produto Card Association - HIPERCARD',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CCA-VAN', 'Subclasse EC - Produto Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-ADQUIR', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - ADQUIR',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 1 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-VAN', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - VAN',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 2 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CREDITO.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-DEBITO.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Debito, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-AVISTA.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - A Vista, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-LOJA.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Loja, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-VISA.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - VISA, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 1 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-MASTERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - MASTERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 2 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-AMEX.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - AMEX, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-SOROCRED.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - SOROCRED, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 6 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-ELO.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - ELO, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 7 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-DINERS.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - DINERS, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 9 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-BANESCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - BANESCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 15 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-CABAL.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - CABAL, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 23 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-CREDSYSTEM.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - CREDSYSTEM, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 29 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.EC.PRD-PC-CRED-ESP.PRD-PM-PARC-BCO.PRD-CA-HIPERCARD.PRD-CCA-MULTIPLO', 'Subclasse EC - Produto Payment Category - Credito Especilizado, Payment Method - Parcelado Banco, Card Association - HIPERCARD, Contract Card Association - MULTIPLO',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.EC',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_category": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_payment_method": 3 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_card_association": 40 }]''::jsonb AND json_massa->''massa''->''config_pmt_merchant'' @> ''[{"cd_contract_card_assoc": 3 }]''::jsonb'
      )
  )
);



-- ----------------------------------------------------------------------------
\echo 'PIPELINE.FLUXOCHB.ID186414.STEP'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'PIPELINE.FLUXOCHB.ID186414.STEP', 'Pipeline do Fluxo de CHB (ID186414) - "Processamento de Bandeiras"', null
);

-- ----------------------------------------------------------------------------
\echo 'PIPELINE.FLUXOCHB.ID186626.STEP'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'PIPELINE.FLUXOCHB.ID186626.STEP', 'Pipeline do Fluxo de CHB (ID186626) - "Processamento de Bandeiras"', null
);

-- ----------------------------------------------------------------------------
\echo 'PIPELINE.FLUXOCHB.ID186685.STEP'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'PIPELINE.FLUXOCHB.ID186685.STEP', 'Pipeline do Fluxo de CHB (ID186685) - "Processamento de Bandeiras"', null
);

-- ----------------------------------------------------------------------------
\echo 'PIPELINE.FLUXOCHB.ID186642.STEP'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'PIPELINE.FLUXOCHB.ID186642.STEP', 'Pipeline do Fluxo de CHB (ID186642) - "Processamento de Bandeiras"', null
);

-- ----------------------------------------------------------------------------
\echo 'insert into bm_config_login_sistema values ...'
-- ----------------------------------------------------------------------------
INSERT INTO bm_config_login_sistema ( config_tipo_ambiente_id, config_tipo_reserva_id, sistema, login_name, login_password, obs )
SELECT (SELECT id FROM bm_config_tipo_ambiente WHERE cod_tipo_ambiente = 'HML'),
       1 /* Disponivel p/ uso no Banco de Massa */,
       'STAR'       sistema,
	   't2607thg'   login_name,
	   'C!elo_1234' login_password,
	   ''           obs
;
INSERT INTO bm_config_login_sistema ( config_tipo_ambiente_id, config_tipo_reserva_id, sistema, login_name, login_password, obs )
SELECT (SELECT id FROM bm_config_tipo_ambiente WHERE cod_tipo_ambiente = 'HML'),
       1 /* Disponivel p/ uso no Banco de Massa */,
       'STAR'       sistema,
	   't2609crd'   login_name,
	   'C!elo_1234' login_password,
	   ''           obs
;
INSERT INTO bm_config_login_sistema ( config_tipo_ambiente_id, config_tipo_reserva_id, sistema, login_name, login_password, obs )
SELECT (SELECT id FROM bm_config_tipo_ambiente WHERE cod_tipo_ambiente = 'HML'),
       1 /* Disponivel p/ uso no Banco de Massa */,
       'STAR'       sistema,
	   't0407lzf'   login_name,
	   'C!elo_1234' login_password,
	   ''           obs
;

INSERT INTO bm_config_login_sistema ( config_tipo_ambiente_id, config_tipo_reserva_id, sistema, login_name, login_password, obs )
VALUES (1, 1, 'STC', 'eyveif', 'C!elo_1234');

INSERT INTO bm_config_login_sistema ( config_tipo_ambiente_id, config_tipo_reserva_id, sistema, login_name, login_password, obs )
VALUES (1, 1, 'SEC', 'eyvdmd', 'cielo030');


-- INICIO ETL.FILES.ETLHML.%
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.001.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 001, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''001'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.001.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 001, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''001'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.001.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 001, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''001'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.001.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 001, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''001'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.001.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 001, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''001'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.021.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 021, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''021'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.021.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 021, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''021'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.021.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 021, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''021'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.021.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 021, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''021'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.021.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 021, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''021'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.021.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 021, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''021'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.025.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 025, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''025'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.025.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 025, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''025'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.025.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 025, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''025'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.025.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 025, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''025'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.025.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 025, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''025'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.025.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 025, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''025'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.033.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 033, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''033'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.033.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 033, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''033'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.033.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 033, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''033'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.033.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 033, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''033'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.033.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 033, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''033'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.033.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 033, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''033'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.041.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 041, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''041'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.041.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 041, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''041'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.041.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 041, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''041'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.041.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 041, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''041'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.041.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 041, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''041'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.041.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 041, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''041'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.044.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 044, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''044'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.044.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 044, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''044'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.044.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 044, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''044'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.044.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 044, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''044'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.044.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 044, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''044'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.044.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 044, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''044'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.070.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 070, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''070'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.070.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 070, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''070'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.070.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 070, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''070'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.070.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 070, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''070'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.070.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 070, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''070'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.070.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 070, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''070'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.104.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 104, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''104'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.104.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 104, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''104'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.104.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 104, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''104'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.104.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 104, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''104'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.104.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 104, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''104'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.104.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 104, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''104'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.218.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 218, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''218'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.218.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 218, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''218'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.218.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 218, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''218'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.218.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 218, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''218'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.218.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 218, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''218'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.218.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 218, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''218'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.237.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 237, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''237'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.237.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 237, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''237'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.237.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 237, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''237'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.237.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 237, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''237'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.237.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 237, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''237'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.237.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 237, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''237'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.246.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 246, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''246'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.246.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 246, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''246'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.246.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 246, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''246'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.246.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 246, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''246'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.246.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 246, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''246'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.246.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 246, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''246'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.341.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 341, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''341'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.341.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 341, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''341'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.341.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 341, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''341'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.341.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 341, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''341'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.341.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 341, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''341'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.341.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 341, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''341'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.389.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 389, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''389'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.389.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 389, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''389'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.389.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 389, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''389'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.389.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 389, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''389'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.389.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 389, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''389'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.389.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 389, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''389'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.399.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 399, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''399'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.399.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 399, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''399'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.399.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 399, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''399'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.399.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 399, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''399'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.399.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 399, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''399'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.399.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 399, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''399'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.422.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 422, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''422'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.422.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 422, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''422'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.422.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 422, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''422'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.422.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 422, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''422'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.422.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 422, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''422'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.422.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 422, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''422'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.453.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 453, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''453'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.453.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 453, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''453'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.453.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 453, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''453'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.453.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 453, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''453'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.453.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 453, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''453'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.453.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 453, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''453'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.604.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 604, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''604'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.604.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 604, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''604'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.604.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 604, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''604'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.604.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 604, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''604'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.604.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 604, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''604'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.604.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 604, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''604'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.634.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 634, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''634'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.634.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 634, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''634'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.634.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 634, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''634'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.634.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 634, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''634'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.634.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 634, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''634'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.634.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 634, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''634'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.655.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 655, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''655'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.655.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 655, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''655'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.655.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 655, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''655'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.655.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 655, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''655'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.655.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 655, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''655'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.655.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 655, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''655'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.707.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 707, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''707'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.707.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 707, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''707'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.707.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 707, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''707'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.707.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 707, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''707'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.707.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 707, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''707'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.707.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 707, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''707'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.745.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 745, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''745'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.745.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 745, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''745'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.745.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 745, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''745'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.745.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 745, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''745'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.745.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 745, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''745'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.745.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 745, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''745'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.748.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 748, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''748'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.748.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 748, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''748'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.748.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 748, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''748'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.748.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 748, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''748'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.748.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 748, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''748'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.748.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 748, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''748'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.756.AGENDA', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 756, Agenda',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''756'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Agenda'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.756.BASE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 756, Base',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''756'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Base'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.756.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 756, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''756'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.756.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 756, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''756'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.756.GRADE', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 756, Grade',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''756'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Grade'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.FUNDING.REMESSA.756.PARCELADO', 'Subclasse de ETL.FILES.ETLHML - Funding, Remessa, 756, Parcelado',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Funding'' AND json_massa->''massa''->>''tipo_remessa_retorno'' = ''remessa'' AND json_massa->''massa''->>''num_banco'' = ''756'' AND json_massa->''massa''->>''tipo_arquivo_banco_domicilio'' = ''Parcelado'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.INCOMING.DINERS', 'Subclasse de ETL.FILES.ETLHML - Incoming Diners',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Incoming'' AND json_massa->''massa''->>''bandeira'' = ''Diners'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.INCOMING.ELO', 'Subclasse de ETL.FILES.ETLHML - Incoming Elo',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Incoming'' AND json_massa->''massa''->>''bandeira'' = ''Elo'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.INCOMING.HIPERCARD', 'Subclasse de ETL.FILES.ETLHML - Incoming Hipercard',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Incoming'' AND json_massa->''massa''->>''bandeira'' = ''Hipercard'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.INCOMING.MASTERCARD', 'Subclasse de ETL.FILES.ETLHML - Incoming Mastercard',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Incoming'' AND json_massa->''massa''->>''bandeira'' = ''Mastercard'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.INCOMING.NEWELO.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Incoming, NewElo, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Incoming'' AND json_massa->''massa''->>''bandeira'' = ''NewElo'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.INCOMING.NEWELO.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Incoming, NewElo, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Incoming'' AND json_massa->''massa''->>''bandeira'' = ''NewElo'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.INCOMING.VISA', 'Subclasse de ETL.FILES.ETLHML - Incoming Visa',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Incoming'' AND json_massa->''massa''->>''bandeira'' = ''Visa'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.AMEX', 'Subclasse de ETL.FILES.ETLHML - Outgoing Amex',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Amex'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.BANESCARD.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Banescard, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Banescard'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.BANESCARD.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Banescard, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Banescard'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.CABAL.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Cabal, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Cabal'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.CABAL.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Cabal, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Cabal'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.CREDSYSTEM.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Credsystem, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Credsystem'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.CREDSYSTEM.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Credsystem, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Credsystem'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.DINERS', 'Subclasse de ETL.FILES.ETLHML - Outgoing Diners',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Diners'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.ELO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Elo',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Elo'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.HIPERCARD', 'Subclasse de ETL.FILES.ETLHML - Outgoing Hipercard',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Hipercard'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.MASTERCARD', 'Subclasse de ETL.FILES.ETLHML - Outgoing Mastercard',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Mastercard'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.NEWELO.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing, NewElo, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''NewElo'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.NEWELO.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing, NewElo, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''NewElo'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.SOROCRED.CREDITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Sorocred, Credito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Sorocred'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Credito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.SOROCRED.DEBITO', 'Subclasse de ETL.FILES.ETLHML - Outgoing Sorocred, Debito',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Sorocred'' AND json_massa->''massa''->>''tipo_bandeira_newelo'' = ''Debito'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);
INSERT INTO bm_config_classe_massa ( classe_massa, obs, json_config )
VALUES 
( 
  'ETL.FILES.ETLHML.OUTGOING.VISA', 'Subclasse de ETL.FILES.ETLHML - Outgoing Visa',
  json_build_object(
    'config',
      json_build_object(
        'tipo_classe_massa_id', 2 /* 2: Subclasse massa */ ,
        'tipo_classe_massa', 'Subclasse Criterio Condicao',
        'classe_massa_base', 'ETL.FILES.ETLHML',
        'controle_uso_massa_id', 1 /* 1: Disponivel para Reuso */,
        'controle_uso_massa', 'Disponivel para Reuso',
        'criterio_massa', 'json_massa->''massa''->>''funcionalidade'' = ''Outgoing'' AND json_massa->''massa''->>''bandeira'' = ''Visa'' AND json_massa->''massa''->>''dt_referencia'' = TO_CHAR(CURRENT_DATE,''YYYY-MM-DD'')'
      )
  )
);

-- FIM    ETL.FILES.ETLHML.%

