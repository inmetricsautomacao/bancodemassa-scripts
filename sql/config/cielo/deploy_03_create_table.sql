-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_03_create_table.sql
-- Description:
--              
-- Log-changes:
--              * 2017-11-30 JosemarSilva bm_config_usuario_sistema
--              * 2017-11-26 JosemarSilva bm_config_classe_massa
--              * 2017-07-09 JosemarSilva ADD cod_reserva
--              * 2017-06-25 JosemarSilva bm_config_reserva
--              * 2017-06-25 JosemarSilva Creating
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
\echo 'bm_config_merchant ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_merchant (
  id SERIAL NOT NULL,
  config_tipo_ambiente_id SMALLINT NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  nu_customer BIGINT NOT NULL,
  cod_reserva VARCHAR(100) NULL,
  nu_root_cnpj_cpf BIGINT NULL,
  nu_branch_cnpj INTEGER NULL,
  nu_control_cnpj INTEGER NULL,
  tags  VARCHAR(100) NULL,
  datahora_ini_reserva TIMESTAMP NULL,
  datahora_fim_reserva TIMESTAMP NULL,
  solicitante_reserva VARCHAR(100) NULL,
  obs VARCHAR(100),
  CONSTRAINT pk_config_merchant PRIMARY KEY(id)
);

ALTER TABLE bm_config_merchant ADD CONSTRAINT ak_config_merchant UNIQUE (config_tipo_ambiente_id,nu_customer);


-- ----------------------------------------------------------------------------
\echo 'bm_config_login_sistema ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_login_sistema (
  id SERIAL NOT NULL,
  config_tipo_ambiente_id SMALLINT NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  sistema VARCHAR(30) NOT NULL,
  login_name VARCHAR(30) NOT NULL,
  login_password VARCHAR(30) NOT NULL,
  role_name VARCHAR(30) NULL,
  cod_reserva VARCHAR(100) NULL,
  tags  VARCHAR(100) NULL,
  obs VARCHAR(1000) NULL,
  CONSTRAINT pk_config_login_sistema PRIMARY KEY(id)
);

ALTER TABLE bm_config_login_sistema ADD CONSTRAINT ak_config_login_sistema UNIQUE (config_tipo_ambiente_id, sistema, login_name);


-- ----------------------------------------------------------------------------
\echo 'bm_config_capirdmdr ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_capirdmdr (
  id SERIAL NOT NULL,
  config_tipo_ambiente_id INTEGER NOT NULL,
  config_tipo_reserva_id SMALLINT NOT NULL,
  test_case_id INTEGER NOT NULL,
  test_plan_id INTEGER NOT NULL,
  test_case_name VARCHAR(250) NOT NULL,
  cycle_folder_path_name VARCHAR(250) NOT NULL,
  dias_anterior SMALLINT NOT NULL,
  nome_projeto VARCHAR(30) NOT NULL,
  nome_lote VARCHAR(100) NOT NULL,
  flag_enable SMALLINT NOT NULL,
  flag_check_status_alm SMALLINT NOT NULL,
  canal_venda VARCHAR(30) NULL,
  bandeira VARCHAR(30) NULL,
  tags VARCHAR(200) NULL,
  tipo_capirdmdr VARCHAR(30) NULL,
  CONSTRAINT pk_bm_config_capirdmdr PRIMARY KEY(id)
);

ALTER TABLE bm_config_capirdmdr ADD CONSTRAINT ak_config_capirdmdr UNIQUE (config_tipo_ambiente_id, config_tipo_reserva_id, test_case_name);

CREATE INDEX fk_configcapirdmdr_configtiporeserva  ON bm_config_capirdmdr (config_tipo_reserva_id);
CREATE INDEX fk_configcapirdmdr_configtipoambiente ON bm_config_capirdmdr (config_tipo_ambiente_id)


-- ----------------------------------------------------------------------------
\echo 'bm_config_param_capirdmdr ...'
-- ----------------------------------------------------------------------------
CREATE TABLE bm_config_param_capirdmdr (
  id SERIAL NOT NULL,
  config_capirdmdr_id INTEGER NOT NULL,
  chave VARCHAR(100) NOT NULL,
  operador VARCHAR(30) NOT NULL,
  valor_esperado VARCHAR(100) NOT NULL,
  valor_capturado VARCHAR(100) NULL,
  CONSTRAINT pk_bm_config_param_capirdmdr PRIMARY KEY(id)
);

CREATE INDEX fk_configparamcapirdmdr_configcapirdmdr ON bm_config_param_capirdmdr (config_capirdmdr_id)
