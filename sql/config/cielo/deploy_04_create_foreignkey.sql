-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_04_create_foreignkey.sql
-- Description:
--              
-- Log-changes:
--              * 2017-06-25 JosemarSilva Creating
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--


-- ----------------------------------------------------------------------------
\echo 'create constraints foreign key ...'
-- ----------------------------------------------------------------------------
ALTER TABLE bm_config_merchant      ADD CONSTRAINT fk_config_merchant_config_tipo_reserva       FOREIGN KEY (config_tipo_reserva_id)           REFERENCES bm_config_tipo_reserva(id);
ALTER TABLE bm_config_merchant      ADD CONSTRAINT fk_config_merchant_config_tipo_ambiente      FOREIGN KEY (config_tipo_ambiente_id)          REFERENCES bm_config_tipo_ambiente(id);

ALTER TABLE bm_config_login_sistema ADD CONSTRAINT fk_config_login_sistema_config_tipo_reserva  FOREIGN KEY (config_tipo_reserva_id)           REFERENCES bm_config_tipo_reserva(id);
ALTER TABLE bm_config_login_sistema ADD CONSTRAINT fk_config_login_sistema_config_tipo_ambiente FOREIGN KEY (config_tipo_ambiente_id)          REFERENCES bm_config_tipo_ambiente(id);
