﻿-- ----------------------------------------------------------------------------
-- Filename:
--              deploy_01_create_database.sql
-- Description:
--              
-- Log-changes:
--              * 2017-10-24 JosemarSilva Reuzindo nome do database
--              * 2017-06-25 JosemarSilva Creating
-- Pre-reqs:
--              * n/a
-- ----------------------------------------------------------------------------
--

--
-- \echo 'Create database ...'
--
CREATE DATABASE bmconfig;
