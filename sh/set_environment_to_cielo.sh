###############################################################################
# filename : set_environment_to_cielo.sh
# parameter:
#            n/a
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-10-13 JosemarSilva Development
###############################################################################
#

rm /root/rails-home/bancodemassa-cantina-railsapp/lib/scripts/bin/etl-stage-steps.sh
ln -s /root/rails-home/bancodemassa-cantina-railsapp/lib/scripts/bin/cielo/etl-stage-steps-cielo.sh /root/rails-home/bancodemassa-cantina-railsapp/lib/scripts/bin/etl-stage-steps.sh
chmod +x /root/rails-home/bancodemassa-cantina-railsapp/lib/scripts/bin/etl-stage-steps.sh

