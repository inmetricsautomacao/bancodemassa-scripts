#!/usr/bin/bash
echo off
###############################################################################
# filename : deploy-node-db-cielo.sh
# parameter: 
#            n/a
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-26 JosemarSilva Desenvolvimento
###############################################################################
#
echo ""
echo "1. Setting up ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
HOME_ETL_BM=/root/rails-home/bancodemassa-cantina-railsapp/lib/scripts
POSTGRES_HOST=127.0.0.1
POSTGRES_PORT=5432
# Super privileges: Default database ...
POSTGRES_DBNAME_SUPERUSER=postgres
POSTGRES_USERNAME_SUPERUSER=root
POSTGRES_PASSWORD_SUPERUSER=root
# Normal privileges: Deploy database ...
POSTGRES_DBNAME_DEPLOY=bancodemassanode
POSTGRES_USERNAME_DEPLOY=root
POSTGRES_PASSWORD_SUPERUSER=root

echo ""
echo "2. Executing Deploy Node Stage Cielo scripts ..."
echo ""

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/node/deploy_07_create_table_stage.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/node/deploy_08_insert_values_stage.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/node/deploy_09_function_stage.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/node/deploy_10_view_stage.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/node/deploy_90_grant_to_stage.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

echo ""
echo "99. Finish ..."
echo ""
date +%Y-%m-%d:%H:%M:%S

