#!/usr/bin/bash
echo off
###############################################################################
# filename : etl-extract-mainframe-sec.sh
# parameter: 
#            n/a
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-19 JosemarSilva merge do projeto script dentro do Rails WebApp
#            * 2017-07-20 JosemarSilva Initialize
###############################################################################
#
CURRENT_DIR=`pwd`

# Setting config ...
HOME_ETL_BM=/root/rails-home/bancodemassa-cantina-railsapp/lib/scripts
HOME_TMP_DIR=/tmp/bancodemassa
mkdir $HOME_TMP_DIR  # Create if not exists

# Copy config to tmp ...
rm -f $HOME_TMP_DIR/log4j.properties
rm -f $HOME_TMP_DIR/mainframeRedes.properties
cp    $HOME_ETL_BM/bin/log4j.properties        $HOME_TMP_DIR
cp    $HOME_ETL_BM/bin/mainframeRedes.properties $HOME_TMP_DIR
cp -R $HOME_ETL_BM/bin/resource                $HOME_TMP_DIR

# Change directory to tmp ...
cd $HOME_TMP_DIR
pwd
date +%Y-%m-%d:%H:%M:%S
java -jar /root/scripts-home/bancodemassa-etl-scripts/bin/devops.jar -a extracaoAT01AT02

# Go back last 'current directory' ...
date +%Y-%m-%d:%H:%M:%S
cd $CURRENT_DIR
#
