#!/usr/bin/bash
echo off
###############################################################################
# filename : etl-extract-mainframe-redes.sh
# parameter: 
#            n/a
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-19 JosemarSilva merge do projeto script dentro do Rails WebApp
#            * 2017-07-20 JosemarSilva Initialize
###############################################################################
#
CURRENT_DIR=`pwd`

HOME_ETL_BM=/root/rails-home/bancodemassa-cantina-railsapp/lib/scripts
HOME_TMP_DIR=/tmp/bancodemassa
mkdir $HOME_TMP_DIR  # Create if not exists

rm -f $HOME_TMP_DIR/log4j.properties
rm -f $HOME_TMP_DIR/mainframeSec.properties
cp    $HOME_ETL_BM/bin/log4j.properties        $HOME_TMP_DIR
cp    $HOME_ETL_BM/bin/mainframeSec.properties $HOME_TMP_DIR
cp -R $HOME_ETL_BM/bin/resource                $HOME_TMP_DIR

cd $HOME_TMP_DIR
pwd
date +%Y-%m-%d:%H:%M:%S
java -jar $HOME_ETL_BM/bin/devops.jar -a extracaoConfigProdutoEstabelecimentoa

date +%Y-%m-%d:%H:%M:%S
java -jar $HOME_ETL_BM/bin/devops.jar -a extracaoTerminalNumeroAcesso

# Go back last 'current directory' ...
date +%Y-%m-%d:%H:%M:%S
cd $CURRENT_DIR
#

