#!/usr/bin/bash
echo off
###############################################################################
# filename : etl-stage-cielo.sh
# parameter: P1 - Environment Type
#            [
#              0: HML - Homologacao (default)
#              1: TI  - Teste Integrado
#              2: DEV - Desenvolvimento
#            ]
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-19 JosemarSilva merge do projeto script dentro do Rails WebApp
#            * 2017-08-08 AliomarJunior Inclusão de EC_CONVIVENCIA, EC_REDES, EC_CONFIG_REDES e EC_CANAL_REDES
#            * 2017-05-18 JosemarSilva Stage Settlement Transaction
#            * 2017-04-19 JosemarSilva Only payment hierarchy product
###############################################################################
#
echo ""
echo "1. Setting up ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
ps -f | grep sh
# Default
if [ "$1" == "" ]
  then
    ENVIRONMENT_TYPE_CODE=0
else
    ENVIRONMENT_TYPE_CODE=$1
fi
ENVIRONMENT_TYPE_NAME="null"
# HML
ORACLE_HOST_HML=10.64.113.16
ORACLE_PORT_HML=1521
ORACLE_SID_HML=BDTHBOB
ORACLE_USERNAME_HML=eyveij
ORACLE_PASSWORD_HML=IN_201612
# TI
ORACLE_HOST_TI=10.64.113.16
ORACLE_PORT_TI=1521
ORACLE_SID_TI=BDTQBOB
ORACLE_USERNAME_TI=eyveij
ORACLE_PASSWORD_TI=IN_201612
# DEV
ORACLE_HOST_DEV=10.64.113.16
ORACLE_PORT_DEV=1521
ORACLE_SID_DEV=BDTQBOB
ORACLE_USERNAME_DEV=eyveij
ORACLE_PASSWORD_DEV=IN_201612
# Default
ORACLE_HOST=null
ORACLE_PORT=null
ORACLE_SID=null
ORACLE_USERNAME=null
ORACLE_PASSWORD=null
# Switch case ( Environment Type )
if [ $ENVIRONMENT_TYPE_CODE -eq 0  ]
  then
    ENVIRONMENT_TYPE_NAME="HML"
    ORACLE_HOST=$ORACLE_HOST_HML
    ORACLE_PORT=$ORACLE_PORT_HML
    ORACLE_SID=$ORACLE_SID_HML
    ORACLE_USERNAME=$ORACLE_USERNAME_HML
    ORACLE_PASSWORD=$ORACLE_PASSWORD_HML
elif [ $ENVIRONMENT_TYPE_CODE -eq 1 ]
  then
    ENVIRONMENT_TYPE_NAME="TI"
    ORACLE_HOST=$ORACLE_HOST_TI
    ORACLE_PORT=$ORACLE_PORT_TI
    ORACLE_SID=$ORACLE_SID_TI
    ORACLE_USERNAME=$ORACLE_USERNAME_TI
    ORACLE_PASSWORD=$ORACLE_PASSWORD_TI
elif [ $ENVIRONMENT_TYPE_CODE -eq 2 ]
  then
    ENVIRONMENT_TYPE_NAME="DEV"
    ORACLE_HOST=$ORACLE_HOST_DEV
    ORACLE_PORT=$ORACLE_PORT_DEV
    ORACLE_SID=$ORACLE_SID_DEV
    ORACLE_USERNAME=$ORACLE_USERNAME_DEV
    ORACLE_PASSWORD=$ORACLE_PASSWORD_DEV
else
    ENVIRONMENT_TYPE_CODE=0
    ENVIRONMENT_TYPE_NAME="HML"
    ORACLE_HOST=$ORACLE_HOST_HML
    ORACLE_PORT=$ORACLE_PORT_HML
    ORACLE_SID=$ORACLE_SID_HML
    ORACLE_USERNAME=$ORACLE_USERNAME_HML
    ORACLE_PASSWORD=$ORACLE_PASSWORD_HML
fi
# Setting variables for command line arguments ...
HOME_ETL_BM=/root/rails-home/bancodemassa-cantina-railsapp/lib/scripts
HOME_TMP_DIR=/tmp/bancodemassa
ORACLE_JDBC_URL="jdbc:oracle:thin:${ORACLE_USERNAME}/${ORACLE_PASSWORD}@${ORACLE_HOST}:${ORACLE_PORT}:${ORACLE_SID}"
SQLSERVER_JDBC_URL="jdbc:sqlserver://10.230.122.11:1433;user=simuiso;password=simuiso;databaseName=simutermweb"
POSTGRES_HOST=127.0.0.1
POSTGRES_PORT=5432
POSTGRES_DBNAME=bancodemassanode
POSTGRES_USERNAME=root #bancodemassa
POSTGRES_PASSWORD=root #bancodemassa
POSTGRES_JDBC_URL="jdbc:postgresql://${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DBNAME}?user=${POSTGRES_USERNAME}&password=${POSTGRES_PASSWORD}&ssl=false"
POSTGRES_CLEANUP_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-01-postgres-cleanup.sql
PGPASSWORD=${POSTGRES_PASSWORD}


#
echo ""
echo "2. Clean-up stage ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -f ${POSTGRES_CLEANUP_FILENAME}


#
echo ""
echo "3. Copy Oracle tables To Postgres tables ..."
echo ""

#
echo ""
echo "3.1. stage_star_transaction ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
QUERY_SELECT_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-02d-oracle-queryselect.sql
QUERY_INSERT_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-02d-postgres-queryinsert.sql
java -jar ${HOME_ETL_BM}/bin/copyoracletopostgres.jar -O ${ORACLE_JDBC_URL} -P ${POSTGRES_JDBC_URL}  -i ${QUERY_INSERT_FILENAME} -s ${QUERY_SELECT_FILENAME}

#
echo ""
echo "3.2. stage_simuterm_lote_transacao ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
QUERY_SELECT_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-02e-sqlserver-queryselect.sql
QUERY_INSERT_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-02e-postgres-queryinsert.sql
java -jar ${HOME_ETL_BM}/bin/copysqlservertopostgres.jar -S ${SQLSERVER_JDBC_URL} -P ${POSTGRES_JDBC_URL}  -i ${QUERY_INSERT_FILENAME} -s ${QUERY_SELECT_FILENAME}

#
echo ""
echo "3.3. stage_payment_hierarchy_product ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
QUERY_SELECT_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-02b-oracle-queryselect.sql
QUERY_INSERT_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-02b-postgres-queryinsert.sql
echo java -jar ${HOME_ETL_BM}/bin/copyoracletopostgres.jar -O ${ORACLE_JDBC_URL} -P ${POSTGRES_JDBC_URL}  -i ${QUERY_INSERT_FILENAME} -s ${QUERY_SELECT_FILENAME}

#
echo ""
echo "3.4. stage_tipo_pagamento_cliente ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
QUERY_SELECT_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-02f-oracle-queryselect.sql
QUERY_INSERT_FILENAME=${HOME_ETL_BM}/sql/etl/cielo/script-02f-postgres-queryinsert.sql
java -jar ${HOME_ETL_BM}/bin/copyoracletopostgres.jar -O ${ORACLE_JDBC_URL} -P ${POSTGRES_JDBC_URL}  -i ${QUERY_INSERT_FILENAME} -s ${QUERY_SELECT_FILENAME}

#
echo ""
echo "4. Clean-up Mainframe Stage Area ..."
echo ""

echo ""
echo "4.1. Clean-up Mainframe Stage Area ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
mkdir  $HOME_TMP_DIR  # Create if not exists
rm -f  $HOME_TMP_DIR/logs/mainframeRedes.properties
rm -f  $HOME_TMP_DIR/logs/mainframeSec.properties
rm -f  $HOME_TMP_DIR/logs/log4j.properties
rm -rf $HOME_TMP_DIR/logs/logs
rm -rf $HOME_TMP_DIR/logs/resource
rm -rf $HOME_TMP_DIR/logs/Evidences


echo ""
echo "4.2. Refresh Mainframe SEC configurations ..."
echo ""
cp    $HOME_ETL_BM/bin/log4j.properties          $HOME_TMP_DIR/logs
cp    $HOME_ETL_BM/bin/mainframeRedes.properties $HOME_TMP_DIR/logs
cp    $HOME_ETL_BM/bin/mainframeSec.properties   $HOME_TMP_DIR/logs
mkdir $HOME_TMP_DIR/logs/logs
cp -R $HOME_ETL_BM/bin/resource                  $HOME_TMP_DIR/logs/
mkdir $HOME_TMP_DIR/logs/Evidences


echo ""
echo "4.3. Execute Mainframe Extracao SEC ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
cd $HOME_TMP_DIR/logs
java -jar $HOME_ETL_BM/bin/devops.jar -a extracaoAT01AT02 | tail -n 5

echo ""
echo "4.4. Copy SEC *.csv TO PostgreSql database ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
AT01_SEC_CSV_FILE=`find . -name mainframeSec_09_MainframeSec_Extracao_AT01_Estabelecimento.csv        | tail -n 1`
AT02_SEC_CSV_FILE=`find . -name mainframeSec_08_MainframeSec_Extracao_AT02_EstabelecimentoProduto.csv | tail -n 1`
rm -f /tmp/AT01_SEC.csv /tmp/AT02_SEC.csv
#
date +%Y-%m-%d:%H:%M:%S
if [ "$AT01_SEC_CSV_FILE" == "" ]
	then
	echo "AT01_SEC_CSV_FILE | Error: Não foi localizado o arquivo mainframeSec_09_MainframeSec_Extracao_AT01_Estabelecimento.csv"
else
	echo "AT01_SEC_CSV_FILE | Arquivo $AT01_SEC_CSV_FILE localizado"
	sed 's/;$//g' $AT01_SEC_CSV_FILE | sed 's/";"/;/g' | sed 's/"$//g' | sed 's/^"//g' > /tmp/AT01_SEC.csv
	POSTGRES_COPY_COMMAND="DROP TABLE IF EXISTS stage_mainframe_sec_at01; CREATE TABLE stage_mainframe_sec_at01( `cat /tmp/AT01_SEC.csv | head -n 1 | sed 's/;/ VARCHAR, /g' | sed 's/$/ VARCHAR/g'` ); COPY stage_mainframe_sec_at01 FROM '/tmp/AT01_SEC.csv' DELIMITER ';' ;"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
	POSTGRES_COPY_COMMAND="ALTER TABLE stage_mainframe_sec_at01 ADD config_tipo_reserva_id SMALLINT; ALTER TABLE stage_mainframe_sec_at01 ADD cod_reserva VARCHAR(100);"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
fi

if [ "$AT02_SEC_CSV_FILE" == "" ]
	then
	echo "AT02_SEC_CSV_FILE | Error: Não foi localizado o arquivo mainframeSec_08_MainframeSec_Extracao_AT02_EstabelecimentoProduto.csv"
else
	echo "AT02_SEC_CSV_FILE | Arquivo $AT02_SEC_CSV_FILE localizado"
	sed 's/;$//g' $AT02_SEC_CSV_FILE | sed 's/";"/;/g' | sed 's/"$//g' | sed 's/^"//g' > /tmp/AT02_SEC.csv
	POSTGRES_COPY_COMMAND="DROP TABLE IF EXISTS stage_mainframe_sec_at02; CREATE TABLE stage_mainframe_sec_at02( `cat /tmp/AT02_SEC.csv | head -n 1 | sed 's/;/ VARCHAR, /g' | sed 's/$/ VARCHAR/g'` ); COPY stage_mainframe_sec_at02 FROM '/tmp/AT02_SEC.csv' DELIMITER ';' ;"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
	POSTGRES_COPY_COMMAND="ALTER TABLE stage_mainframe_sec_at02 ADD config_tipo_reserva_id SMALLINT; ALTER TABLE stage_mainframe_sec_at02 ADD cod_reserva VARCHAR(100);"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
fi

echo ""
echo "4.5. Execute Mainframe Extracao Redes ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
cd $HOME_TMP_DIR/logs
date +%Y-%m-%d:%H:%M:%S
java -jar $HOME_ETL_BM/bin/devops.jar -a extracaoConfigProdutoEstabelecimento | tail -n 5
date +%Y-%m-%d:%H:%M:%S
java -jar $HOME_ETL_BM/bin/devops.jar -a extracaoTerminalNumeroAcesso | tail -n 5
date +%Y-%m-%d:%H:%M:%S


echo ""
echo "4.6. Copy REDES *.csv TO PostgreSql database ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
G124_REDES_CSV_FILE=`find . -name mainframeRedes_99\(a\)_MainframeRedes-Extracao-EstabelecimentoG124.csv        | tail -n 1`
G125_REDES_CSV_FILE=`find . -name mainframeRedes_99\(b\)_MainframeRedes-Extracao-ProdutoEstabelecimentoG125.csv | tail -n 1`
G126_REDES_CSV_FILE=`find . -name mainframeRedes_99\(1\)_MainframeRedes-Extracao-TerminalNumeroAcessoG126.csv   | tail -n 1`
rm -f /tmp/G124_REDES.csv /tmp/G125_REDES.csv /tmp/G126_REDES.csv
#
date +%Y-%m-%d:%H:%M:%S
if [ "$G124_REDES_CSV_FILE" == "" ] 
	then
	echo "G124_REDES_CSV_FILE | Error: Não foi localizado o arquivo mainframeRedes_99\(a\)_MainframeRedes-Extracao-EstabelecimentoG124.csv" 
else
	echo "G124_REDES_CSV_FILE | Arquivo $G124_REDES_CSV_FILE localizado"
	sed 's/;$//g' $G124_REDES_CSV_FILE | sed 's/";"/;/g' | sed 's/"$//g' | sed 's/^"//g' > /tmp/G124_REDES.csv	
	POSTGRES_COPY_COMMAND="DROP TABLE IF EXISTS stage_mainframe_redes_g124; CREATE TABLE stage_mainframe_redes_g124( `cat /tmp/G124_REDES.csv | head -n 1 | sed 's/;/ VARCHAR, /g' | sed 's/$/ VARCHAR/g'` ); COPY stage_mainframe_redes_g124 FROM '/tmp/G124_REDES.csv' DELIMITER ';' ;"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
	POSTGRES_COPY_COMMAND="ALTER TABLE stage_mainframe_redes_g124 ADD config_tipo_reserva_id SMALLINT; ALTER TABLE stage_mainframe_redes_g124 ADD cod_reserva VARCHAR(100);"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
	POSTGRES_COPY_COMMAND="ALTER TABLE stage_mainframe_redes_g124 ADD matriz VARCHAR(100);ALTER TABLE stage_mainframe_redes_g124 ADD loja VARCHAR(100);"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
fi
#
date +%Y-%m-%d:%H:%M:%S
if [ "$G125_REDES_CSV_FILE" == "" ] 
	then
	echo "G125_REDES_CSV_FILE | Error: Não foi localizado o arquivo mainframeRedes_99\(b\)_MainframeRedes-Extracao-ProdutoEstabelecimentoG125.csv"
	
else
	echo "G125_REDES_CSV_FILE | Arquivo $G125_REDES_CSV_FILE localizado"
	sed 's/;$//g' $G125_REDES_CSV_FILE | sed 's/";"/;/g' | sed 's/"$//g' | sed 's/^"//g' > /tmp/G125_REDES.csv
	POSTGRES_COPY_COMMAND="DROP TABLE IF EXISTS stage_mainframe_redes_g125; CREATE TABLE stage_mainframe_redes_g125( `cat /tmp/G125_REDES.csv | head -n 1 | sed 's/;/ VARCHAR, /g' | sed 's/$/ VARCHAR/g'` ); COPY stage_mainframe_redes_g125 FROM '/tmp/G125_REDES.csv' DELIMITER ';' ;"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
	POSTGRES_COPY_COMMAND="ALTER TABLE stage_mainframe_redes_g125 ADD config_tipo_reserva_id SMALLINT; ALTER TABLE stage_mainframe_redes_g125 ADD cod_reserva VARCHAR(100);"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
	POSTGRES_COPY_COMMAND="ALTER TABLE stage_mainframe_redes_g125 ADD matriz VARCHAR(100);ALTER TABLE stage_mainframe_redes_g125 ADD loja VARCHAR(100);"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
fi
#
date +%Y-%m-%d:%H:%M:%S
if [ "$G126_REDES_CSV_FILE" == "" ] 
	then
	echo "G126_REDES_CSV_FILE | Error: Não foi localizado o arquivo mainframeRedes_99\(1\)_MainframeRedes-Extracao-TerminalNumeroAcessoG126.csv" 
else
	echo "G126_REDES_CSV_FILE | Arquivo $G126_REDES_CSV_FILE localizado"
	sed 's/;$//g' $G126_REDES_CSV_FILE | sed 's/";"/;/g' | sed 's/"$//g' | sed 's/^"//g' > /tmp/G126_REDES.csv
	POSTGRES_COPY_COMMAND="DROP TABLE IF EXISTS stage_mainframe_redes_g126; CREATE TABLE stage_mainframe_redes_g126( `cat /tmp/G126_REDES.csv | head -n 1 | sed 's/;/ VARCHAR, /g' | sed 's/$/ VARCHAR/g'` ); COPY stage_mainframe_redes_g126 FROM '/tmp/G126_REDES.csv' DELIMITER ';' ;"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
	POSTGRES_COPY_COMMAND="ALTER TABLE stage_mainframe_redes_g126 ADD config_tipo_reserva_id SMALLINT; ALTER TABLE stage_mainframe_redes_g126 ADD cod_reserva VARCHAR(100);"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
	POSTGRES_COPY_COMMAND="ALTER TABLE stage_mainframe_redes_g126 ADD matriz VARCHAR(100);ALTER TABLE stage_mainframe_redes_g126 ADD loja VARCHAR(100);"
	psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
fi

#
echo ""
echo "5. Loading ..."
echo ""

echo ""
echo "5.1. Loading massa: ( EC.CONFIG.STAR ) ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
POSTGRES_LOAD_NODE_SCRIPT_FUNCTION="SELECT fx_node_execute_etl('${ENVIRONMENT_TYPE_NAME}')"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_LOAD_NODE_SCRIPT_FUNCTION}"

echo ""
echo "5.99. Loading ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
POSTGRES_LOAD_NODE_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/etl/cielo/script-03-postgres-load-node-status.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -f ${POSTGRES_LOAD_NODE_SCRIPT_FILENAME}

#
echo ""
echo "6. Finish ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
#
