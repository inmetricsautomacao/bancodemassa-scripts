#!/usr/bin/bash
echo off
###############################################################################
# filename : deploy-cluster-db.sh
# parameter: 
#            n/a
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-19 JosemarSilva merge do projeto script dentro do Rails WebApp
#            * 2017-06-17 JosemarSilva Desenvolvimento
###############################################################################
#
echo ""
echo "1. Setting up ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
HOME_ETL_BM=/root/rails-home/bancodemassa-cantina-railsapp/lib/scripts
POSTGRES_HOST=127.0.0.1
POSTGRES_PORT=5432
# Super privileges: Default database ...
POSTGRES_DBNAME_SUPERUSER=postgres
POSTGRES_USERNAME_SUPERUSER=root
POSTGRES_PASSWORD_SUPERUSER=root
# Normal privileges: Deploy database ...
POSTGRES_DBNAME_DEPLOY=bancodemassacluster
POSTGRES_USERNAME_DEPLOY=root
POSTGRES_PASSWORD_SUPERUSER=root

echo ""
echo "2. Executing Deploy Cluster scripts ..."
echo ""
POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/deploy_01_create_database.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_SUPERUSER} -U ${POSTGRES_USERNAME_SUPERUSER} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/deploy_02_create_user_schema.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_SUPERUSER} -U ${POSTGRES_USERNAME_SUPERUSER} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/deploy_03_create_table.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/deploy_04_create_foreignkey.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/deploy_05_index.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/deploy_06_insert_values.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/deploy_07_function.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/deploy_90_grant_to.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_DEPLOY} -U ${POSTGRES_USERNAME_DEPLOY} -f ${POSTGRES_SCRIPT_FILENAME}

echo ""
echo "99. Finish ..."
echo ""
date +%Y-%m-%d:%H:%M:%S

