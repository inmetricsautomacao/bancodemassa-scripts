#!/usr/bin/bash
echo off
###############################################################################
# filename : init-db.sh
# parameter: 
#            n/a
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-19 JosemarSilva Development
###############################################################################
#
echo ""
echo "1. Setting up ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
HOME_ETL_BM=/root/rails-home/bancodemassa-cantina-railsapp/lib/scripts
POSTGRES_HOST=127.0.0.1
POSTGRES_PORT=5432
# Normal privileges: Deploy database ...
POSTGRES_DBNAME_CLUSTER=bancodemassacluster
POSTGRES_DBNAME_CONFIG=bancodemassaconfig
POSTGRES_DBNAME_NODE=bancodemassanode
POSTGRES_USERNAME_INIT=root
#POSTGRES_PASSWORD_INIT=root

echo ""
echo "2. Executing Initialization scripts ..."
echo ""
POSTGRES_SCRIPT_FILENAME="${HOME_ETL_BM}/sql/cluster/init-db-01.sql"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME_CLUSTER} -U ${POSTGRES_USERNAME_INIT} -f ${POSTGRES_SCRIPT_FILENAME}


echo ""
echo "99. Finish ..."
echo ""
date +%Y-%m-%d:%H:%M:%S

