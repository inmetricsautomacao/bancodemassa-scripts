#!/usr/bin/bash
echo off
###############################################################################
# filename : export-csv.sh
# parameter: 
#            n/a
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-19 JosemarSilva merge do projeto script dentro do Rails WebApp
#            * 2017-07-26 JosemarSilva Development
###############################################################################
#
echo ""
echo "1. Setting up ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
ps -f | grep sh
# Setting variables for command line arguments ...
POSTGRES_HOST=127.0.0.1
POSTGRES_PORT=5432
POSTGRES_DBNAME=bancodemassanode
POSTGRES_USERNAME=root #bancodemassa
POSTGRES_PASSWORD=root #bancodemassa
POSTGRES_JDBC_URL="jdbc:postgresql://${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DBNAME}?user=${POSTGRES_USERNAME}&password=${POSTGRES_PASSWORD}&ssl=false"
PGPASSWORD=${POSTGRES_PASSWORD}


echo ""
echo "2. Clean-up last execution ..."
echo ""
rm -f /tmp/bm_config_reserva.csv*
rm -f /tmp/bm_cluster_config.csv*
rm -f /tmp/bm_node_instancia_massa.csv*
rm -f /tmp/bm_node_log_falha.csv*


echo ""
echo "3. Exporting (.csv) ..."
echo ""
POSTGRES_COPY_COMMAND="COPY (SELECT * FROM bm_config_reserva cr INNER JOIN bm_config_tipo_reserva ctr ON ctr.id = cr.config_tipo_reserva_id) TO  '/tmp/bm_config_reserva.csv' CSV HEADER ;"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
POSTGRES_COPY_COMMAND="COPY (SELECT * FROM bm_cluster_config cc INNER JOIN bm_cluster_content_type cct ON cct.id = cc.cluster_content_type_id INNER JOIN bm_cluster_environment_type cet ON cet.id = cc.cluster_environment_type_id INNER JOIN bm_cluster_status cs ON cs.id = cc.cluster_status_id) TO  '/tmp/bm_cluster_config.csv' CSV HEADER ;"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
POSTGRES_COPY_COMMAND="COPY (SELECT * FROM bm_node_instancia_massa nim INNER JOIN bm_node_status_massa nsm ON nsm.id = nim.node_status_massa_id INNER JOIN bm_node_classe_massa ncm ON ncm.id = nim.node_classe_massa_id) TO  '/tmp/bm_node_instancia_massa.csv' CSV HEADER ;"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"
POSTGRES_COPY_COMMAND="COPY (SELECT * FROM bm_node_log_falha) TO  '/tmp/bm_node_log_falha.csv' CSV HEADER ;"
psql -h ${POSTGRES_HOST} -d ${POSTGRES_DBNAME} -U ${POSTGRES_USERNAME} -a -c "${POSTGRES_COPY_COMMAND}"

echo ""
echo "4. Zipping (.csv) ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
zip /tmp/bm_config_reserva.csv.zip       /tmp/bm_config_reserva.csv
zip /tmp/bm_cluster_config.csv.zip       /tmp/bm_cluster_config.csv
zip /tmp/bm_node_instancia_massa.csv.zip /tmp/bm_node_instancia_massa.csv
zip /tmp/bm_node_log_falha.csv.zip       /tmp/bm_node_log_falha.csv
chmod 777 /tmp/bm*.zip



#
echo ""
echo "5. Finish ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
#

