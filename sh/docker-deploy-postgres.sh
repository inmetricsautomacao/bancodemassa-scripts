#!/usr/bin/bash
echo off
###############################################################################
# filename : docker-deploy-postgres.sh
# parameter: n/a
# author   : Josemar F. A. Silva
# remarks  :
#            * 2017-09-19 JosemarSilva merge do projeto script dentro do Rails WebApp
#            * 2017-06-03 JosemarSilva Development
###############################################################################
#
echo ""
echo "1. Setting up ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
# Setting variables for command line arguments ...
HOME_DOCKER_SCRIPT=/root/rails-home/bancodemassa-cantina-railsapp/lib/scripts
HOME_LOG=${HOME_DOCKER_SCRIPT}/log

#
echo ""
echo "2. Ensure Docker and Docker Compose are working ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
# Checking ...
docker --version
docker-compose --version
echo Trying to start docker ...
systemctl start docker
ps -ef | grep docker

#
echo ""
echo "3. Pull Docker Images ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
# Pull docker images
docker pull postgres:9.6.3-alpine

#
echo ""
echo "4. Run PostgreSQL Docker Image to startup database server ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
# Run Docker Images
DOCKER_NAME_PREFIX=postgres-01
DOCKER_NAME_SUFIX=`date +%Y-%m-%d` # date +%Y-%m-%d_%H-%M
DOCKER_NAME=${DOCKER_NAME_PREFIX}_${DOCKER_NAME_SUFIX}
echo Trying to stop and remove docker \'${DOCKER_NAME}\' ...
docker stop ${DOCKER_NAME}
docker rm ${DOCKER_NAME}
echo Trying to run docker \'${DOCKER_NAME}\' ...
docker run --name postgres-01 -e POSTGRES_PASSWORD=mysecretpassword -d postgres:9.6.3-alpine
docker ps

#
echo ""
echo "5. Run PostgreSQL Docker Image to execute command ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
# Run Docker Images



#
echo ""
echo "5. Finish ..."
echo ""
date +%Y-%m-%d:%H:%M:%S
#
