# ##############################################################################
# filename : deploy-bm.ps1
# arguments: 
#            -env   <environment-name>
#            -step  <step-name>
#            -debug <debug-level>
#            where
#              * <environment-name>
#              * <step-name>
# author   : Josemar F. A. Silva
# pre-reqs :
#            * n/a
# remarks  :
#            * 2018-01-24 JosemarSilva Development
# ##############################################################################
#


# ##############################################################################
# BEGIN Functions
# ##############################################################################

function show-usage([String] $pMsgError) {
    Write-Output "Usage: deploy-bm <parameters-arguments>"
    Write-Output "where parameters arguments are:"
    Write-Output "  -step  <step-name>    Step name to execute. Required. List of values:"
    Write-Output "         * 'create-databases'    : Create databases [bmcluster,bmconfig,bmnode]"
    Write-Output "         * 'create-users-schemas': Create users and schemas"
    Write-Output "         * 'deploy-cluster'      : Create database objects [bmcluster]"
    Write-Output "         * 'deploy-config'       : Create database objects [bmconfig]"
    Write-Output "         * 'deploy-node'         : Create database objects [bmnode]"
    Write-Output "         * 'undeploy-cluster'    : Drop database [bmcluster]"
    Write-Output "         * 'undeploy-config'     : Drop database [bmconfig]"
    Write-Output "         * 'undeploy-node'       : Drop database [bmnode]"
    Write-Output "  -debug <debug-level> Debug Level. List of values:"
    Write-Output "         * 0:  None, nothing debuged (Default)"
    Write-Output "         * 1:  Low debug level"
    Write-Output "         * 2:  Medium debug level"
    Write-Output "         * 3:  High debug level"
    Write-Output ""        
    Write-Output "Error message: $pMsgError"
    Write-Output ""
    #
    exit(1) # return error
}

function get-parameter-argument([String] $pParamArg) {
    $numOfArgs = $PARAM_ARGS.Length
    for ($i=0; $i -lt $numOfArgs; $i++)
    {
        if ( $pParamArg.ToLower() -eq $PARAM_ARGS[$i].ToLower() -and $i -lt $numOfArgs -1 ) {
            return $PARAM_ARGS[($i+1)].ToLower()
        }
    }
    return ""
}

function run-sql-script([String] $pScriptFilename, [String] $pHostname, [String] $pDbname, [String] $pUsername, [String] $pPassword ){
  [String]$strPathToProgram = "C:\Program Files (x86)\pgAdmin III\1.22\psql.exe";  # "C:\Program Files (x86)\pgAdmin III\1.22\psql.exe"  "C:\Program Files (x86)\pgAdmin 4\v2\runtime\psql.exe"
  [Array]$arguments = "-h", "$pHostname", "-d", "$pDbname", "-U", "$pUsername", "-a", "-f", "$pScriptFilename";
  $env:PGPASSWORD = "$pPassword" # Set
  If ($DEBUG_LEVEL -ge 1 ) { # DEBUG_LEVEL_LOW
  Write-Output "$strPathToProgram $arguments";
  }
  & $strPathToProgram $arguments;
  $env:PGPASSWORD = "" # Reset
}

function debug-global-variables( ) {
  # Debug
  Write-Output "PARAM_ARGS                 $PARAM_ARGS"
  Write-Output "DEBUG_LEVEL                $DEBUG_LEVEL";
  Write-Output "POSTGRES_HOST              $POSTGRES_HOST"
  Write-Output "POSTGRES_PORT              $POSTGRES_PORT"
  Write-Output "POSTGRES_DBNAME            $POSTGRES_DBNAME"
  Write-Output "POSTGRES_USERNAME          $POSTGRES_USERNAME"
  Write-Output "POSTGRES_PASSWORD          $POSTGRES_PASSWORD"
  Write-Output "PGPASSWORD                 $PGPASSWORD"
  
}



# ##############################################################################
# END Functions
# ##############################################################################


# ##############################################################################
# BEGIN main()
# ##############################################################################

Write-Output ""
Write-Output "1. Setting up global variables ..."
Write-Output ""
Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
Write-Output ""

#
# Get command line parameters arguments ...
#
$PARAM_ARGS=$args
[int]$DEBUG_LEVEL=0 # 0:No debug; 1=Low; 2=Medium; 3=High
$DEBUG_LEVEL = (get-parameter-argument "-debug")
[String]$STEP = (get-parameter-argument "-step").ToLower()
 
#
# Parse and validade command line parameters arguments ...
#
if (
    $STEP -ne "create-databases" -and $STEP -ne "create-users-schemas" -and
    $STEP -ne "deploy-cluster"   -and $STEP -ne "deploy-config" -and $STEP -ne "deploy-node" -and 
    $STEP -ne "undeploy-cluster" -and $STEP -ne "undeploy-config" -and $STEP -ne "undeploy-node"
   ) {
    show-usage "Invalid '-step' parameter argument"
}

# Setting variables for command line arguments ...
[String]$HOME_SQL_SCRIPT_DIR='C:\My Git\workspace-inmetrics-selenium\bancodemassa-rails\lib\scripts\sql'
[String]$POSTGRES_HOST="localhost"    # 
[String]$POSTGRES_PORT="5432"         #
[String]$POSTGRES_DBNAME="postgres"   # overwritten later ...
[String]$POSTGRES_USERNAME="postgres" #
[String]$POSTGRES_PASSWORD="postgres" #
[String]$PGPASSWORD="${POSTGRES_PASSWORD}"

# ##############################################################################
# STEP-BY-STEP EXECUTION ...
# ##############################################################################

if ($DEBUG_LEVEL -gt 0 ) {
    debug-global-variables
}


Write-Output ""
Write-Output "2. Step-by-step execution ..."
Write-Output ""

if ($STEP -eq "create-databases") {
    Write-Output ""
    Write-Output "2.1. Create databases ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    [String]$POSTGRES_DBNAME="postgres"   # ... overwritten!
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\deploy_01_create_database.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\config\deploy_01_create_database.sql  ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_01_create_database.sql    ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
}

if ($STEP -eq "create-users-schemas") {
    Write-Output ""
    Write-Output "2.2. Create users and schemas  ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\deploy_02_create_user_schema.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\config\deploy_02_create_user_schema.sql  ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_02_create_user_schema.sql    ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
}

if ($STEP -eq "deploy-cluster") {
    Write-Output ""
    Write-Output "2.3. Deploy bmcluster ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    [String]$POSTGRES_DBNAME="bmcluster"   # ... overwritten!
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\deploy_03_create_table.sql       ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\deploy_04_create_foreignkey.sql  ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\deploy_05_index.sql              ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\deploy_06_insert_values.sql      ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\deploy_07_function.sql           ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\deploy_90_grant_to.sql           ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\init-db-01.sql                   ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

}

if ($STEP -eq "deploy-config") {
    Write-Output ""
    Write-Output "2.4. Deploy bmconfig ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    [String]$POSTGRES_DBNAME="bmconfig"   # ... overwritten!
    run-sql-script $HOME_SQL_SCRIPT_DIR\config\deploy_03_create_table.sql       ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\config\deploy_04_create_foreignkey.sql  ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\config\deploy_05_index.sql              ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\config\deploy_06_insert_values.sql      ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\config\deploy_07_function.sql           ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\config\deploy_90_grant_to.sql           ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
	
}

if ($STEP -eq "deploy-node") {
    Write-Output ""
    Write-Output "2.5. Deploy bmnode ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    [String]$POSTGRES_DBNAME="bmnode"   # ... overwritten!
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_03_create_table.sql                  ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_04_create_foreignkey.sql             ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_05_index.sql                         ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_06_insert_values.sql                 ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_07_function.sql                      ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_08_view.sql                          ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_09_extension_server_user_mapping.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_10_foreigntable.sql                  ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\deploy_90_grant_to.sql                      ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
	
}

if ($STEP -eq "undeploy-cluster") {
    Write-Output ""
    Write-Output "2.6. UNDeploy bmconfig ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    [String]$POSTGRES_DBNAME="bmcluster"   # ... overwritten!
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\undeploy_99_drop_all_objects.sql    ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

}

if ($STEP -eq "undeploy-config") {
    Write-Output ""
    Write-Output "2.7. UNDeploy bmconfig ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    [String]$POSTGRES_DBNAME="bmconfig"   # ... overwritten!
    run-sql-script $HOME_SQL_SCRIPT_DIR\cluster\undeploy_99_drop_all_objects.sql    ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
}

if ($STEP -eq "undeploy-node") {
    Write-Output ""
    Write-Output "2.8. UNDeploy bmnode ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    [String]$POSTGRES_DBNAME="bmnode"   # ... overwritten!
    run-sql-script $HOME_SQL_SCRIPT_DIR\node\undeploy_99_drop_all_objects.sql    ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
}



Write-Output ""
Write-Output "3. End of execution ..."
Write-Output ""
Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
Write-Output ""

# ##############################################################################
# END main()
# ##############################################################################
