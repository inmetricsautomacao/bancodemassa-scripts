# ##############################################################################
# filename : etl-files-bm.ps1
# arguments: 
#            -env   <environment-name>
#            -step  <step-name>
#            -debug <debug-level>
#            where
#              * <environment-name>
#              * <step-name>
# author   : Josemar F. A. Silva
# pre-reqs :
#            * n/a
# remarks  :
#            * 2018-02-28 JosemarSilva etlhml-winscp_script
#            * 2018-02-23 JosemarSilva Develop check-status-batch
#            * 2018-02-14 JosemarSilva Config para ambiente Cielo
#            * 2018-02-09 JosemarSilva Config
#            * 2018-01-24 JosemarSilva Development
# ##############################################################################
#


# ##############################################################################
# BEGIN Functions
# ##############################################################################

# ##############################################################################
#
# ##############################################################################
function show-usage([String] $pMsgError) {
    Write-Output "Usage: etl-files-bm <parameters-arguments>"
    Write-Output "where parameters arguments are:"
    Write-Output "  -env   <environment-name> Environment name of source informantion:"
    Write-Output "         * 'HML': Homologacao (Default)"
    Write-Output "         * 'TI' : Teste Integrado"
    Write-Output "         * 'DEV': Desenvolvimento."
    Write-Output "  -step  <step-name> Step name to execute. Required. List of values:"
    Write-Output "         * 'check-status-batch' : Check Status batch"
    Write-Output "         * 'etlhml'   : Download, stage, transform and load Linux files"
    Write-Output "         * 'mainframe': Download, stage, transform and load Mainframe files"
    Write-Output "  -debug <debug-level>     Debug Level. List of values:"
    Write-Output "         0:  None, nothing debuged (Default)"
    Write-Output "         1:  Low debug level"
    Write-Output "         2:  Medium debug level"
    Write-Output "         3:  High debug level"
    Write-Output ""        
    Write-Output "Error message: $pMsgError"
    Write-Output ""
    #
    exit(1) # return error
}

# ##############################################################################
#
# ##############################################################################
function get-parameter-argument([String] $pParamArg) {
    $numOfArgs = $PARAM_ARGS.Length
    for ($i=0; $i -lt $numOfArgs; $i++)
    {
        if ( $pParamArg.ToLower() -eq $PARAM_ARGS[$i].ToLower() -and $i -lt $numOfArgs -1 ) {
            return $PARAM_ARGS[($i+1)].ToLower()
        }
    }
    return ""
}


# ##############################################################################
#
# ##############################################################################
function run-postgres-sql-script([String] $pScriptFilename, [String] $pHostname, [String] $pDbname, [String] $pUsername, [String] $pPassword ){
  [String]$strPathToProgram = "C:\Program Files (x86)\pgAdmin III\1.22\psql.exe";  # "C:\Program Files (x86)\pgAdmin III\1.22\psql.exe"  "C:\Program Files (x86)\pgAdmin 4\v2\runtime\psql.exe"
  [Array]$arguments = "-h", "$pHostname", "-d", "$pDbname", "-U", "$pUsername", "-a", "-f", "$pScriptFilename";
  $env:PGPASSWORD = "$pPassword" # Set
  If ($DEBUG_LEVEL -ge 1 ) { # DEBUG_LEVEL_LOW
  Write-Output "$strPathToProgram $arguments";
  }
  & $strPathToProgram $arguments;
  $env:PGPASSWORD = "" # Reset
}


# ##############################################################################
#
# ##############################################################################
function run-oracle-sql-script([String]$pScriptFilename, [String]$pHostname, [String]$pPort, [String]$pDbname, [String]$pUsername, [String]$pPassword ){
  [String]$strPathToProgram = "E:\tools-inmetrics\sqldeveloper\sqldeveloper\bin\sql.exe"; # "C:\sqldeveloper\sqldeveloper\bin\sql.exe";
  [Array]$arguments =  "$pUsername/${pPassword}@//${pHostname}:$pPort/$pDbname", "`"@$pScriptFilename`"";
  If ($DEBUG_LEVEL -ge 1 ) { # DEBUG_LEVEL_LOW
    Write-Output "$strPathToProgram $arguments";
  }
  & $strPathToProgram $arguments;
}


# ##############################################################################
#
# ##############################################################################
function run-winscp-script([String] $pScriptFilename ){
  [String]$strPathToProgram = "C:\Program Files (x86)\WinSCP\WinSCP.com"
  [Array]$arguments = "/script=$pScriptFilename";
  If ($DEBUG_LEVEL -ge 1 ) { # DEBUG_LEVEL_LOW
  Write-Output "$strPathToProgram $arguments";
  }
  & $strPathToProgram $arguments;
}


# ##############################################################################
#
# ##############################################################################
function debug-global-variables( ) {
  # Debug
  Write-Output "PARAM_ARGS                 $PARAM_ARGS"
  Write-Output "DEBUG_LEVEL                $DEBUG_LEVEL";
  Write-Output "ENVIRONMENT_TYPE_NAME      $ENVIRONMENT_TYPE_NAME"
  Write-Output "HOME_SCRIPT_DIR            $HOME_SCRIPT_DIR"
  Write-Output "HOME_TMP_DIR               $HOME_TMP_DIR"
  Write-Output "HOME_SHARE_DIR             $HOME_SHARE_DIR"
  Write-Output "HOME_CSV2PG_DIR            $HOME_CSV2PG_DIR"
  Write-Output "POSTGRES_HOST              $POSTGRES_HOST"
  Write-Output "POSTGRES_PORT              $POSTGRES_PORT"
  Write-Output "POSTGRES_DBNAME            $POSTGRES_DBNAME"
  Write-Output "POSTGRES_USERNAME          $POSTGRES_USERNAME"
  Write-Output "POSTGRES_PASSWORD          $POSTGRES_PASSWORD"
  Write-Output "PGPASSWORD                 $PGPASSWORD"
  Write-Output "ORACLE_HOST                $ORACLE_HOST"
  Write-Output "ORACLE_PORT                $ORACLE_PORT"
  Write-Output "ORACLE_DBNAME              $ORACLE_DBNAME"
  Write-Output "ORACLE_USERNAME            $ORACLE_USERNAME"
  Write-Output "ORACLE_PASSWORD            $ORACLE_PASSWORD"
  Write-Output "CSV_LINUX_FILES            $CSV_LINUX_FILES"
  Write-Output "WINSCP_SCRIPT_FILE         $WINSCP_SCRIPT_FILE"
  Write-Output "SQLLOADFILESDB_SCRIPT_FILE $SQLLOADFILESDB_SCRIPT_FILE"
  Write-Output "LINUX_HOST                 $LINUX_HOST"
  Write-Output "LINUX_USERNAME             $LINUX_USERNAME"
  Write-Output "LINUX_PASSWORD             $LINUX_PASSWORD"
  
}


# ##############################################################################
#
# ##############################################################################
function get-dateReference() {
  [String]$strReturn = Get-Date -UFormat "%Y-%m-%d"
  return $strReturn
}


# ##############################################################################
#
# ##############################################################################
function print-status-processing-row([int] $pRowNumber) {
      if ( $pRowNumber % 100 -eq 0 ) {
        Write-Host -NoNewline "|"
      } elseif ( $pRowNumber % 50 -eq 0 ) {
        Write-Host -NoNewline ":"
      } elseif ( $pRowNumber % 10 -eq 0 ) {
        Write-Host -NoNewline ":"
      } else {
        Write-Host -NoNewline "."
      }
}


# ##############################################################################
#
# ##############################################################################
function check-status-batch() {

    # Get DateReference ...
    [String]$dateReference = get-dateReference # YYYY-MM-DD
    [String]$dtRefYYYYMMDD = $dateReference.Replace("-", "")
    #Write-Output "dateReference: " $dateReference
    #Write-Output "dtRefYYYYMMDD: " $dtRefYYYYMMDD
    [String]$queryStatusMalha = ""

    # Run Oracle Script Query Status Batch ...
    Write-Output "SPOOL $ORACLE_STATUS_BATCH_RESULT" | Out-File $ORACLE_STATUS_BATCH_SCRIPT -Encoding default # -Append 
    Write-Output "$ORACLE_STATUS_BATCH_QUERY"        | Out-File $ORACLE_STATUS_BATCH_SCRIPT -Encoding default   -Append 
    Write-Output "SPOOL OFF"                         | Out-File $ORACLE_STATUS_BATCH_SCRIPT -Encoding default   -Append 
    run-oracle-sql-script "$ORACLE_STATUS_BATCH_SCRIPT" "$ORACLE_HOST" "$ORACLE_PORT" "$ORACLE_DBNAME" "$ORACLE_USERNAME" "$ORACLE_PASSWORD"
    # Check status batch execution ...
    [String]$status = Select-String -Pattern "SUCESSO" -Path $ORACLE_STATUS_BATCH_RESULT
    If ( "$status" -eq "" ) {
      Write-Output "Check Status Batch FAIL! See status on $ORACLE_STATUS_BATCH_RESULT"
      Exit -1  # FAIL !
    }

}


# ##############################################################################
#
# ##############################################################################
function etl-files-etlhml-script-builder() {

    # Get DateReference ...
    [String]$dateReference = get-dateReference
    [String]$dtRefYYYYMMDD = $dateReference.Replace("-", "")
    #Write-Output "dateReference: " $dateReference
    #Write-Output "dtRefYYYYMMDD: " $dtRefYYYYMMDD

    # Initialize directory structure <home_tmp_dir>/etlhml/<yyyy-mm-dd> ...
    New-Item -ItemType Directory -Force -Path $HOME_TMP_DIR/etlhml                | Out-Null
    New-Item -ItemType Directory -Force -Path $HOME_TMP_DIR/etlhml/$dateReference | Out-Null
    ForEach ( $removeItem IN (
         "$HOME_TMP_DIR/etlhml/$dateReference/*"
      )) { 
      If (Test-Path $removeItem ) { Remove-Item -Recurse -Force $removeItem }
    }

    # WinSCP script file - Open connection ...
    Write-Output "open $($LINUX_USERNAME):$($LINUX_PASSWORD)@$($LINUX_HOST)" | Out-File $WINSCP_SCRIPT_FILE -Encoding default # -Append 
    Write-Output ""                                                          | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 
    Write-Output "option confirm off"                                        | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 
    Write-Output "option batch continue"                                     | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 
    Write-Output ""                                                          | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 

    # Loop etlhml files ...
    [int]$rowNumber = 0
    $csvFile = Import-Csv -Delimiter ";" $CSV_LINUX_FILES
    ForEach ($csvRecord in $csvFile){ 
      
      # Print row symbol ...
      $rowNumber = $rowNumber + 1
      print-status-processing-row($rowNumber)
      
      # Get ( src_server;cod_environment;funcionalidade;bandeira;num_banco;tipo_arquivo_banco_domicilio;tipo_remessa_retorno;sistema;src_sftp_file_path;src_sftp_file_name_pattern;stage_file_path;obs ) ...
      [String]$stage_file_path = $csvRecord.stage_file_path.Replace("`${HOME-FILE-STAGE}", "$HOME_TMP_DIR/etlhml/$dateReference").Replace("/", "\")
      [String]$src_sftp_file_path = $csvRecord.src_sftp_file_path
      [String]$src_sftp_file_name_pattern = $csvRecord.src_sftp_file_name_pattern.Replace("`${YYYYMMDD}", $dtRefYYYYMMDD)
      # Create local directory if not exists
      New-Item -ItemType Directory -Force -Path $stage_file_path | Out-Null
      # Create local directory if not exists
      Write-Output "lcd  $stage_file_path"            | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 
      Write-Output "cd   $src_sftp_file_path"         | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 
      Write-Output "mget $src_sftp_file_name_pattern" | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 
      Write-Output ""                                 | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 
      #
      
    }

    # WinSCP script file - Close connection ...
    Write-Output "exit" | Out-File $WINSCP_SCRIPT_FILE -Encoding default -Append 
    # Run WinSCP script file ...
    run-winscp-script( $WINSCP_SCRIPT_FILE )

}


# ##############################################################################
#
# ##############################################################################
function etl-files-etlhml-load-database() {

    # Get DateReference ...
    [String]$dateReference = get-dateReference
    [String]$dtRefYYYYMMDD = $dateReference.Replace("-", "")
    
    # SQL load files into database script file - Stage files from local directory into database ...
    Write-Output "TRUNCATE TABLE stage_files_etlhml;" | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default # -Append 
    Write-Output ""                                   | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default -Append 

    # Loop etlhml files ...
    [int]$rowNumber = 0
    $csvFile = Import-Csv -Delimiter ";" $CSV_LINUX_FILES
    ForEach ($csvRecord in $csvFile){ 
      
      # Print row symbol ...
      $rowNumber = $rowNumber + 1
      print-status-processing-row($rowNumber)
      
      # Get ( src_server;cod_environment;funcionalidade;bandeira;tipo_bandeira_newelo,num_banco;tipo_arquivo_banco_domicilio;tipo_remessa_retorno;tipo_arquivo_relatorios_clientes;sistema;src_sftp_file_path;src_sftp_file_name_pattern;stage_file_path;obs )
      [String]$codEnvironment = $csvRecord.cod_environment
      [String]$funcionalidade = $csvRecord.funcionalidade
      [String]$bandeira = $csvRecord.bandeira
	  [String]$tipo_bandeira_newelo = $csvRecord.tipo_bandeira_newelo
      [String]$numBanco = $csvRecord.num_banco
      [String]$tipoArquivoBancoDomicilio = $csvRecord.tipo_arquivo_banco_domicilio
      [String]$tipoRemessaRetorno = $csvRecord.tipo_remessa_retorno
      [String]$tipoArquivoRelatoriosClientes = $csvRecord.tipo_arquivo_relatorios_clientes
      [String]$sistema = $csvRecord.sistema
      [String]$srcSftpFilePath = $csvRecord.src_sftp_file_path
      [String]$srcSftpFileNamePattern = $csvRecord.src_sftp_file_name_pattern.Replace("`${YYYYMMDD}", $dtRefYYYYMMDD)
      [String]$stage_file_path = $csvRecord.stage_file_path.Replace("`${HOME-FILE-STAGE}", "$HOME_TMP_DIR/etlhml/$dateReference").Replace("/", "\")
      # Create local directory if not exists
      New-Item -ItemType Directory -Force -Path $stage_file_path | Out-Null

      # Get files filtered by filename pattern ....
      ForEach ( $fileStage IN ( 
        Get-ChildItem $stage_file_path -filter $srcSftpFileNamePattern
      )) { 
        [String]$share_file_path = $stage_file_path.Replace($HOME_TMP_DIR, $HOME_SHARE_DIR)
        Write-Output "INSERT INTO stage_files_etlhml"            | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default -Append 
        Write-Output "( dt_referencia, cod_environment, funcionalidade, bandeira, tipo_bandeira_newelo, num_banco, tipo_arquivo_banco_domicilio, tipo_remessa_retorno, tipo_arquivo_relatorios_clientes, sistema, stage_file_path )"                             | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default -Append 
        Write-Output "VALUES"                                   | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default -Append 
        Write-Output "( '$dateReference', '$codEnvironment', '$funcionalidade', '$bandeira', '$tipo_bandeira_newelo', '$numBanco', '$tipoArquivoBancoDomicilio', '$tipoRemessaRetorno', '$tipoArquivoRelatoriosClientes', '$sistema', '$share_file_path\$fileStage' );"  | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default -Append 
        Write-Output ""                                         | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default -Append 
      }
    }

    # SQL load files into database script file - Load staged files ...
    Write-Output ""
    Write-Output "SELECT fx_load_files_etlhml();" | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default -Append 
    Write-Output ""                               | Out-File $SQLLOADFILESDB_SCRIPT_FILE -Encoding default -Append 
    # Run SQL load files script ...
    run-postgres-sql-script ${SQLLOADFILESDB_SCRIPT_FILE} ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

}



# ##############################################################################
# END Functions
# ##############################################################################


# ##############################################################################
# BEGIN main()
# ##############################################################################

Write-Output ""
Write-Output "1. Setting up global variables ..."
Write-Output ""
Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
Write-Output ""

#
# Get command line parameters arguments ...
#
$PARAM_ARGS=$args
[int]$DEBUG_LEVEL=0 # 0:No debug; 1=Low; 2=Medium; 3=High
$DEBUG_LEVEL = (get-parameter-argument "-debug")
[String]$ENVIRONMENT_TYPE_NAME  =(get-parameter-argument "-env").ToUpper()
if ("$ENVIRONMENT_TYPE_NAME" -eq "") { $ENVIRONMENT_TYPE_NAME = "HML" } #default
[String]$STEP = (get-parameter-argument "-step").ToLower()
 
#
# Parse and validade command line parameters arguments ...
#
if ($ENVIRONMENT_TYPE_NAME -ne "HML" -and $ENVIRONMENT_TYPE_NAME -ne "TI" -and $ENVIRONMENT_TYPE_NAME -ne "DEV") {
    show-usage "Invalid '-env' parameter argument"
}
if (
    $STEP -ne "etlhml" -and $STEP -ne "mainframe" -and $STEP -ne "check-status-batch"
   ) {
    show-usage "Invalid '-step' parameter argument"
}

# Setting variables for command line arguments ...
[String]$HOME_SCRIPT_DIR="E:\bancodemassa_scripts\lib\scripts"
[String]$HOME_TMP_RESTRICT_DIR="E:\bancodemassa_scripts\tmp"
[String]$HOME_TMP_DIR="E:\Shared_bancodemassa"
[String]$HOME_SHARE_DIR="\\10.82.248.107\Shared_bancodemassa"
[String]$HOME_CSV2PG_DIR="\\10.82.248.107\shared_jenkins\bancodemassa"
[String]$POSTGRES_HOST="10.82.252.152"
[String]$POSTGRES_PORT="5432"
[String]$POSTGRES_DBNAME="bmnode"
[String]$POSTGRES_USERNAME="postgres"     # (root, bancodemassa)
[String]$POSTGRES_PASSWORD="automacaoimt" # "automacaoimt", "postgres"
[String]$PGPASSWORD="${POSTGRES_PASSWORD}"
[String]$ORACLE_HOST="10.64.113.16"
[String]$ORACLE_PORT="1521"
[String]$ORACLE_DBNAME="BDTHBOB"
[String]$ORACLE_USERNAME="starstausr"
[String]$ORACLE_PASSWORD="starstausr"
[String]$CSV_LINUX_FILES="$HOME_SCRIPT_DIR\powershell\cielo\etl-files-bm-star.csv"
[String]$WINSCP_SCRIPT_FILE = "$HOME_TMP_RESTRICT_DIR\etlhml-winscp_script.txt"
[String]$SQLLOADFILESDB_SCRIPT_FILE = "$HOME_TMP_DIR\etlhml-sqlloadfilesdb_script.sql"
[String]$ORACLE_STATUS_BATCH_SCRIPT = "$HOME_TMP_DIR\star-query-status-batch.sql"
[String]$ORACLE_STATUS_BATCH_RESULT = "$HOME_TMP_DIR\star-query-status-batch.txt"
[String]$ORACLE_STATUS_BATCH_QUERY = "SELECT CASE WHEN mdp.cd_process_type = 0 AND mdp.cd_movement_status = 'P' THEN 'SUCESSO' ELSE  ''  END AS status,  mdp.dt_movement,  mdp.cd_process_type, mdp.cd_movement_status FROM   STARR4.TBSHRR_MOVEMENT_DATE_PROCESS mdp WHERE  mdp.dt_movement = TRUNC(SYSDATE); "
[String]$LINUX_HOST="10.64.114.13"
[String]$LINUX_USERNAME="eyveg2"
[String]$LINUX_PASSWORD="Cielo_2017"

# ##############################################################################
# STEP-BY-STEP EXECUTION ...
# ##############################################################################

if ($DEBUG_LEVEL -gt 0 ) {
    debug-global-variables
}


Write-Output ""
Write-Output "2. Stage step-by-step execution ..."
Write-Output ""

if ($STEP -eq "check-status-batch") {
    Write-Output ""
    Write-Output "2.1. Check status batch ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    check-status-batch

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "etlhml") {
    Write-Output ""
    Write-Output "2.2. Download, stage, transform and load Linux files to BancoDeMassa ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    # Build script to Copy from Linux to Local ...
    Write-Output "a) Build script to Copy from Linux to Local ..."
    Write-Output ""
    etl-files-etlhml-script-builder
    Write-Output ""
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    # Build script to Load file-directory into database  ...
    Write-Output "b) Build script to Load file-directory into database ..."
    Write-Output ""
    etl-files-etlhml-load-database
    Write-Output ""
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    

}


if ($STEP -eq "mainframe") {
    Write-Output ""
    Write-Output "2.3. Download, stage, transform and load Mainframe files to BancoDeMassa ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}



Write-Output ""
Write-Output "3. End of execution ..."
Write-Output ""
Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
Write-Output ""

# ##############################################################################
# END main()
# ##############################################################################
