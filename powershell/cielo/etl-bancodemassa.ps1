# ##############################################################################
# filename : etl-bancodemassa.ps1
# arguments: 
#            -env   <environment-name>
#            -step  <step-name>
#            -task  <task-name>  (optional)
#            -debug <debug-level> (optional)
#            where
#              * <environment-name>
#              * <step-name>
# author   : Josemar F. A. Silva
# pre-reqs :
#            * n/a
# remarks  :
#            * 2018-03-04 JosemarSilva mv sql/etl/cielo/* /sql/etl/cielo/*
#            * 2018-03-01 JosemarSilva HOME directory D: para E:
#            * 2017-12-19 JosemarSilva rename de bancodemassa_scripts para bancodemassa_scripts
#            * 2017-12-12 JosemarSilva Compare "$variable" to string ""
#            * 2017-12-01 JosemarSilva Spliting script into star, sec, redes, etc
#            * 2017-11-02 JosemarSilva Convert BashScript to Powershell
# ##############################################################################
#


# ##############################################################################
# BEGIN Functions
# ##############################################################################

function show-usage([String] $pMsgError) {
    Write-Output "Usage: etl-bancodemassa <parameters-arguments>"
    Write-Output "where parameters arguments are:"
    Write-Output "  -env <environment-name>  Environment name of source informantion. List of values:"
    Write-Output "                             * 'HML': Homologacao (Default)"
    Write-Output "                             * 'TI' : Teste Integrado"
    Write-Output "                             * 'DEV': Desenvolvimento."
    Write-Output "  -step <step-name>        Step name to execute. Required. List of values:"
    Write-Output "                             * 'all':                   Executed all step-by-step"
    Write-Output "                             * 'stage-star':            Stage copy of data from STAR"
    Write-Output "                             * 'stage-sec':             Stage copy of data from SEC"
    Write-Output "                             * 'stage-redes':           Stage copy of data from REDES"
    Write-Output "                             * 'stage-simuTermWeb':     Stage copy of data from SimuTermWeb"
    Write-Output "                             * 'transform-star':        Transform copy of data from STAR"
    Write-Output "                             * 'transform-sec':         Transform copy of data from SEC"
    Write-Output "                             * 'transform-redes':       Transform copy of data from REDES"
    Write-Output "                             * 'transform-simuTermWeb': Transform copy of data from SimuTermWeb"
    Write-Output "                             * 'load-star':             Load copy of data from STAR"
    Write-Output "                             * 'load-sec':              Load copy of data from SEC"
    Write-Output "                             * 'load-redes':            Load copy of data from REDES"
    Write-Output "                             * 'load-simuTermWeb':      Load copy of data from SimuTermWeb"
    Write-Output "                             * 'load-config':           Load copy of data from BMConfig"
    Write-Output "                             * 'load-node-lookupvalue': Load list of lookup values"
    Write-Output "                             * 'load-node-jsonfilter':  Load list of json filters"
    Write-Output "  -task <task-name>        Step Task name. Optional. List of values:"
    Write-Output "                             * '<task-name>':           Task Name"
    Write-Output "  -debug <debug-level>     Debug Level. List of values:"
    Write-Output "                             * 0:  None, nothing debuged (Default)"
    Write-Output "                             * 1:  Low debug level"
    Write-Output "                             * 2:  Medium debug level"
    Write-Output "                             * 3:  High debug level"
    Write-Output ""
    Write-Output "Error message: $pMsgError"
    Write-Output ""
    #
    exit(1) # return error
}

function get-parameter-argument([String] $pParamArg) {
    $numOfArgs = $PARAM_ARGS.Length
    for ($i=0; $i -lt $numOfArgs; $i++)
    {
        if ( $pParamArg.ToLower() -eq $PARAM_ARGS[$i].ToLower() -and $i -lt $numOfArgs -1 ) {
            return $PARAM_ARGS[($i+1)].ToLower()
        }
    }
    return ""
}

function run-sql-script([String] $pScriptFilename, [String] $pHostname, [String] $pDbname, [String] $pUsername, [String] $pPassword ){
  [String]$strPathToProgram = "C:\Program Files (x86)\pgAdmin III\1.22\psql.exe";  # "C:\Program Files (x86)\pgAdmin III\1.22\psql.exe"  "C:\Program Files (x86)\pgAdmin 4\v2\runtime\psql.exe"
  [Array]$arguments = "-h", "$pHostname", "-d", "$pDbname", "-U", "$pUsername", "-a", "-f", "$pScriptFilename";
  $env:PGPASSWORD = "$pPassword" # Set
  If ($DEBUG_LEVEL -ge 1 ) { # DEBUG_LEVEL_LOW
  Write-Output "$strPathToProgram $arguments";
  }
  & $strPathToProgram $arguments;
  $env:PGPASSWORD = "" # Reset
}

function run-copy-oracle-postgres([String] $pScriptCode ){
  Write-Output ""
  Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
  Write-Output ""
  $QUERY_TRUNCATE_FILENAME="${HOME_SCRIPT_DIR}/sql/node/cielo/etl/script-${pScriptCode}-postgres-truncate.sql"
  $QUERY_SELECT_FILENAME="${HOME_SCRIPT_DIR}/sql/node/cielo/etl/script-${pScriptCode}-oracle-queryselect.sql"
  $QUERY_INSERT_FILENAME="${HOME_SCRIPT_DIR}/sql/node/cielo/etl/script-${pScriptCode}-postgres-queryinsert.sql"
  $QUERY_AFTERCOPY_FILENAME="${HOME_SCRIPT_DIR}/sql/node/cielo/etl/script-${pScriptCode}-postgres-aftercopy.sql"
  run-sql-script ${QUERY_TRUNCATE_FILENAME} ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
  Write-Output ""
  Write-Output "java -jar ${HOME_SCRIPT_DIR}/bin/copyoracletopostgres.jar -O ${ORACLE_JDBC_URL} -P ${POSTGRES_JDBC_URL}  -i ${QUERY_INSERT_FILENAME} -s ${QUERY_SELECT_FILENAME}"
  Write-Output ""
  java -jar ${HOME_SCRIPT_DIR}/bin/copyoracletopostgres.jar -O ${ORACLE_JDBC_URL} -P ${POSTGRES_JDBC_URL}  -i ${QUERY_INSERT_FILENAME} -s ${QUERY_SELECT_FILENAME}
  Write-Output ""
  run-sql-script ${QUERY_AFTERCOPY_FILENAME} ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
  Write-Output ""
  Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
  Write-Output ""
}


function debug-global-variables( ) {
  # Debug
  Write-Output "PARAM_ARGS                 $PARAM_ARGS"
  Write-Output "DEBUG_LEVEL                $DEBUG_LEVEL";
  Write-Output "ENVIRONMENT_TYPE_NAME      $ENVIRONMENT_TYPE_NAME"
  Write-Output "ORACLE_HOST                $ORACLE_HOST"
  Write-Output "ORACLE_PORT                $ORACLE_PORT"
  Write-Output "ORACLE_SID                 $ORACLE_SID"
  Write-Output "ORACLE_USERNAME            $ORACLE_USERNAME"
  Write-Output "ORACLE_PASSWORD            $ORACLE_PASSWORD"
  Write-Output "HOME_SCRIPT_DIR            $HOME_SCRIPT_DIR"
  Write-Output "HOME_TMP_DIR               $HOME_TMP_DIR"
  Write-Output "HOME_CSV2PG_DIR            $HOME_CSV2PG_DIR"
  Write-Output "ORACLE_JDBC_URL            $ORACLE_JDBC_URL"
  Write-Output "SQLSERVER_JDBC_URL         $SQLSERVER_JDBC_URL"
  Write-Output "POSTGRES_HOST              $POSTGRES_HOST"
  Write-Output "POSTGRES_PORT              $POSTGRES_PORT"
  Write-Output "POSTGRES_DBNAME            $POSTGRES_DBNAME"
  Write-Output "POSTGRES_USERNAME          $POSTGRES_USERNAME"
  Write-Output "POSTGRES_PASSWORD          $POSTGRES_PASSWORD"
  Write-Output "POSTGRES_JDBC_URL          $POSTGRES_JDBC_URL"
  Write-Output "PGPASSWORD                 $PGPASSWORD"
}

# ##############################################################################
# END Functions
# ##############################################################################


# ##############################################################################
# BEGIN main()
# ##############################################################################

Write-Output ""
Write-Output "1. Setting up global variables ..."
Write-Output ""
Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
Write-Output ""

#
# Get command line parameters arguments ...
#
$PARAM_ARGS=$args
[int]$DEBUG_LEVEL=0 # 0:No debug; 1=Low; 2=Medium; 3=High
$DEBUG_LEVEL = (get-parameter-argument "-debug")
[String]$ENVIRONMENT_TYPE_NAME  =(get-parameter-argument "-env").ToUpper()
if ("$ENVIRONMENT_TYPE_NAME" -eq "") { $ENVIRONMENT_TYPE_NAME = "HML" } #default
[String]$STEP = (get-parameter-argument "-step").ToLower()
[String]$TASK = (get-parameter-argument "-task").ToLower()
 
#
# Parse and validade command line parameters arguments ...
#
if ($ENVIRONMENT_TYPE_NAME -ne "HML" -and $ENVIRONMENT_TYPE_NAME -ne "TI" -and $ENVIRONMENT_TYPE_NAME -ne "DEV") {
    show-usage "Invalid -env parameter argument"
}
if (
    $STEP -ne "stage-star"           -and $STEP -ne "stage-sec"     -and $STEP -ne "stage-redes"     -and $STEP -ne "stage-simuTermWeb" -and
    $STEP -ne "transform-star"       -and $STEP -ne "transform-sec" -and $STEP -ne "transform-redes" -and $STEP -ne "transform-simuTermWeb" -and
    $STEP -ne "load-star"            -and $STEP -ne "load-sec"      -and $STEP -ne "load-redes"      -and $STEP -ne "load-simuTermWeb" -and $STEP -ne "load-config" -and
    $STEP -ne "load-node-jsonfilter" -and $STEP -ne "load-node-lookupvalue" -and
    $STEP -ne "all"
   ) {
    show-usage "Invalid -step parameter argument"
}


# HML
$ORACLE_HOST_HML="10.64.113.16"
$ORACLE_PORT_HML="1521"
$ORACLE_SID_HML="BDTHBOB"
$ORACLE_USERNAME_HML="eyveij"
$ORACLE_PASSWORD_HML="IN_201612"
# TI
$ORACLE_HOST_TI="10.64.113.16"
$ORACLE_PORT_TI="1521"
$ORACLE_SID_TI="BDTQBOB"
$ORACLE_USERNAME_TI="eyveij"
$ORACLE_PASSWORD_TI="IN_201612"
# DEV
$ORACLE_HOST_DEV="10.64.113.16"
$ORACLE_PORT_DEV="1521"
$ORACLE_SID_DEV="BDTQBOB"
$ORACLE_USERNAME_DEV="eyveij"
$ORACLE_PASSWORD_DEV="IN_201612"
# Default
$ORACLE_HOST="null"
$ORACLE_PORT="null"
$ORACLE_SID="null"
$ORACLE_USERNAME="null"
$ORACLE_PASSWORD="null"

# Switch case ( Environment Type )
if ( $ENVIRONMENT_TYPE_NAME -eq "HML" ) {
    $ORACLE_HOST=$ORACLE_HOST_HML
    $ORACLE_PORT=$ORACLE_PORT_HML
    $ORACLE_SID=$ORACLE_SID_HML
    $ORACLE_USERNAME=$ORACLE_USERNAME_HML
    $ORACLE_PASSWORD=$ORACLE_PASSWORD_HML
} elseif ( $ENVIRONMENT_TYPE_NAME -eq "TI" ) {
    $ORACLE_HOST=$ORACLE_HOST_TI
    $ORACLE_PORT=$ORACLE_PORT_TI
    $ORACLE_SID=$ORACLE_SID_TI
    $ORACLE_USERNAME=$ORACLE_USERNAME_TI
    $ORACLE_PASSWORD=$ORACLE_PASSWORD_TI
} elseif ( $ENVIRONMENT_TYPE_NAME -eq "DEV" ) {
    $ORACLE_HOST=$ORACLE_HOST_DEV
    $ORACLE_PORT=$ORACLE_PORT_DEV
    $ORACLE_SID=$ORACLE_SID_DEV
    $ORACLE_USERNAME=$ORACLE_USERNAME_DEV
    $ORACLE_PASSWORD=$ORACLE_PASSWORD_DEV
} else {
    show-usage "Invalid -env parameter argument"

}


# Setting variables for command line arguments ...
$HOME_SCRIPT_DIR='E:\bancodemassa_scripts\lib\scripts'
$HOME_TMP_DIR="E:\bancodemassa_scripts\tmp"
$HOME_CSV2PG_DIR="\\10.82.248.107\shared_jenkins\bancodemassa"
$ORACLE_JDBC_URL="jdbc:oracle:thin:${ORACLE_USERNAME}/${ORACLE_PASSWORD}@${ORACLE_HOST}:${ORACLE_PORT}:${ORACLE_SID}"
$SQLSERVER_JDBC_URL="jdbc:sqlserver://10.230.122.11:1433;user=simuiso;password=simuiso;databaseName=simutermweb"
$POSTGRES_HOST="10.82.252.152"
$POSTGRES_PORT="5432"
$POSTGRES_DBNAME="bmnode"
$POSTGRES_USERNAME="postgres" # (root, bancodemassa)
$POSTGRES_PASSWORD="automacaoimt" # (root, bancodemassa)
$POSTGRES_JDBC_URL="jdbc:postgresql://${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DBNAME}?user=${POSTGRES_USERNAME}&password=${POSTGRES_PASSWORD}&ssl=false"
$PGPASSWORD="${POSTGRES_PASSWORD}"


# ##############################################################################
# STEP-BY-STEP EXECUTION ...
# ##############################################################################

if ($DEBUG_LEVEL -gt 0 ) {
    debug-global-variables
}


Write-Output ""
Write-Output "2. Stage step-by-step execution ..."
Write-Output ""

if ($STEP -eq "all" -or $STEP -eq "stage-star") {
    Write-Output ""
    Write-Output "2.1. Stage STAR - Oracle database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    #if ($TASK -eq "" or $TASK -contains "tbhrqr_management_hierar_node") {
    #}

    run-copy-oracle-postgres("02aa") # tbhrqr_management_hierar_node    - #Rows: 0
    run-copy-oracle-postgres("02ab") # tbhrqr_hierarchy                 - #Rows: 2.020.304
    run-copy-oracle-postgres("02ac") # tbctmr_terminal_type             - #Rows: 684
    run-copy-oracle-postgres("02ad") # tbctmr_logic_terminal            - #Rows: 237.816
    run-copy-oracle-postgres("02ae") # tbctmr_geogr_address             - #Rows: 800.398
    run-copy-oracle-postgres("02af") # tbctmr_host_bank                 - #Rows: 54
    run-copy-oracle-postgres("02ag") # tbctmr_merchant                  - #Rows: 122.433
    run-copy-oracle-postgres("02ah") # tbctmr_merchant_bank             - #Rows: 90.736
    run-copy-oracle-postgres("02ai") # tbctmr_config_pmt_group          - #Rows: 4.219.558
    run-copy-oracle-postgres("02aj") # tbctmr_merchant_bank_type        - #Rows: 2
    run-copy-oracle-postgres("02ak") # tbctmr_status_activity           - #Rows: 2
    run-copy-oracle-postgres("02al") # tbhrqr_hierarchical_node         - #Rows: 2.214.309
    run-copy-oracle-postgres("02am") # tbhrqr_merchant_node             - #Rows: 734.268
    run-copy-oracle-postgres("02an") # tbhrqr_node_type                 - #Rows: 5
    run-copy-oracle-postgres("02ao") # tbprdr_product                   - #Rows: 156
    run-copy-oracle-postgres("02ap") # tbprdr_card_association          - #Rows: 10
    run-copy-oracle-postgres("02aq") # tbprdr_payment_method            - #Rows: 3
    run-copy-oracle-postgres("02ar") # tbprdr_payment_category          - #Rows: 3
    run-copy-oracle-postgres("02as") # tbctmr_acquirer                  - #Rows: 2
    run-copy-oracle-postgres("02at") # tbprdr_contract_card_assoc       - #Rows: 3
    run-copy-oracle-postgres("02au") # tbsetr_adjustment                - #Rows: 4.898
    run-copy-oracle-postgres("02av") # tbsetr_settlement_transaction    - #Rows: 2.648.605
    run-copy-oracle-postgres("02aw") # tbctmr_config_pmt_merchant       - #Rows: 4.266.768
    run-copy-oracle-postgres("02ax") # tbhrqr_financial_chain_node      - #Rows: 731.163
    run-copy-oracle-postgres("02ay") # tbhrqr_root_cnpj_node            - #Rows: 669.602
    run-copy-oracle-postgres("02az") # tbhrqr_relationship_hrql_node    - #Rows: 18.579
    run-copy-oracle-postgres("02bb") # tbctmr_technology_type           - #Rows: 15
    run-copy-oracle-postgres("02bc") # tbcapr_auth_input_type           - #Rows: 10
    run-copy-oracle-postgres("02bd") # tbcapr_captured_transaction      - #Rows: 1.352.212
    run-copy-oracle-postgres("02be") # tbshrr_movement_date_process     - #Rows: 2.826
    run-copy-oracle-postgres("02bf") # tbshrr_holiday                   - #Rows: 78

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "stage-sec") {
    Write-Output ""
    Write-Output "2.2. Stage SEC - Mainframe ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output ""
    Write-Output "2.2.1. Clean-up stage area ..."
    Write-Output ""

    New-Item -ItemType Directory -Force -Path $HOME_TMP_DIR/sec | Out-Null
    ForEach ( $removeItem IN (
         "$HOME_TMP_DIR/sec/*"
      )) { 
      If (Test-Path $removeItem ) { Remove-Item -Recurse -Force $removeItem }
    }

    Write-Output ""
    Write-Output "2.2.2. Copying configurations and resources ..."
    Write-Output ""
    New-Item -ItemType Directory -Force -Path $HOME_TMP_DIR/sec/logs | Out-Null
    New-Item -ItemType Directory -Force -Path $HOME_TMP_DIR/sec/Evidences | Out-Null
    ForEach ( $copyItem IN (
        "$HOME_SCRIPT_DIR/bin/cielo/log4j.properties",
        "$HOME_SCRIPT_DIR/bin/cielo/mainframeSec.properties",
        "$HOME_SCRIPT_DIR/bin/cielo/resource"
      )) { 
      Copy-Item -Path $copyItem -Destination $HOME_TMP_DIR/sec -force -Recurse
    }

    Write-Output ""
    Write-Output "2.2.3. Extract mainframe navigation into (.csv) files ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    # Execute
    Set-Location -Path $HOME_TMP_DIR/sec | Out-Null
    & java -jar $HOME_SCRIPT_DIR/bin/cielo/devops.jar -a extracaoAT01AT02 #| Select-Object -Last 7

    Write-Output ""
    Write-Output "2.2.4. Copying (.csv) into PostgreSql database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    $at01SecCsvFile=Get-ChildItem $HOME_TMP_DIR/sec/Evidences -Name mainframeSec_09_MainframeSec_Extracao_AT01_Estabelecimento.csv        -Recurse | Select-Object -Last 1
    $at02SecCsvFile=Get-ChildItem $HOME_TMP_DIR/sec/Evidences -Name mainframeSec_08_MainframeSec_Extracao_AT02_EstabelecimentoProduto.csv -Recurse | Select-Object -Last 1
    If ($DEBUG_LEVEL -ge 1 ) { # DEBUG_LEVEL_LOW
      Write-Output "       * Source-Folder-Path: $HOME_TMP_DIR/sec/Evidences"
      Write-Output "         - at01SecCsvFile: $at01SecCsvFile"
      Write-Output "         - at02SecCsvFile: $at02SecCsvFile"
      Write-Output "       * Destination-Folder-Path: $HOME_CSV2PG_DIR"
    }
    New-Item -ItemType Directory -Force -Path $HOME_CSV2PG_DIR | Out-Null
    If (Test-Path $HOME_CSV2PG_DIR/at01-sec.csv ) { Remove-Item -Force $HOME_CSV2PG_DIR/at01-sec.csv }
    If (Test-Path $HOME_CSV2PG_DIR/at02-sec.csv ) { Remove-Item -Force $HOME_CSV2PG_DIR/at02-sec.csv }
    # at01
    if ( "$at01SecCsvFile" -eq "" ) { 
      Write-Output "         - File Not Found! $HOME_TMP_DIR/sec/Evidences/$at01SecCsvFile"
    } else { 
      (Get-Content $HOME_TMP_DIR/sec/Evidences/$at01SecCsvFile) -replace ';$', '' -replace '";"', ';' -replace '"$', '' -replace '^"', '' | Set-Content $HOME_CSV2PG_DIR/at01-sec.csv
      Write-Output "DROP TABLE IF EXISTS stage_mainframe_sec_at01;" | Out-File $HOME_TMP_DIR/sec/at01-sec.sql -Encoding default
      $tableColumns=((Get-Content $HOME_CSV2PG_DIR/at01-sec.csv) | Select-Object -First 1 ) -replace ';', ' VARCHAR, ' -replace '$', ' VARCHAR'
      Write-Output "CREATE TABLE stage_mainframe_sec_at01( $tableColumns );" | Out-File $HOME_TMP_DIR/sec/at01-sec.sql -Encoding default -Append 
      Write-Output "SET CLIENT_ENCODING TO WIN1252;" | Out-File $HOME_TMP_DIR/sec/at01-sec.sql -Encoding default -Append 
      Write-Output "COPY stage_mainframe_sec_at01 FROM '$HOME_CSV2PG_DIR\at01-sec.csv' WITH CSV HEADER DELIMITER ';' ;" | Out-File $HOME_TMP_DIR/sec/at01-sec.sql -Encoding default -Append
      run-sql-script $HOME_TMP_DIR/sec/at01-sec.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    }
    # at02
    if ( "$at02SecCsvFile" -eq "" ) { 
      Write-Output "         - File Not Found! $HOME_TMP_DIR/sec/Evidences/$at02SecCsvFile"
    } else { 
      (Get-Content $HOME_TMP_DIR/sec/Evidences/$at02SecCsvFile) -replace ';$', '' -replace '";"', ';' -replace '"$', '' -replace '^"', '' | Set-Content $HOME_CSV2PG_DIR/at02-sec.csv
      Write-Output "DROP TABLE IF EXISTS stage_mainframe_sec_at02;" | Out-File $HOME_TMP_DIR/sec/at02-sec.sql -Encoding default
      $tableColumns=((Get-Content $HOME_CSV2PG_DIR/at02-sec.csv) | Select-Object -First 1 ) -replace ';', ' VARCHAR, ' -replace '$', ' VARCHAR'
      Write-Output "CREATE TABLE stage_mainframe_sec_at02( $tableColumns );" | Out-File $HOME_TMP_DIR/sec/at02-sec.sql -Encoding default -Append 
      Write-Output "SET CLIENT_ENCODING TO WIN1252;" | Out-File $HOME_TMP_DIR/sec/at02-sec.sql -Encoding default -Append 
      Write-Output "COPY stage_mainframe_sec_at02 FROM '$HOME_CSV2PG_DIR\at02-sec.csv' WITH CSV HEADER DELIMITER ';' ;" | Out-File $HOME_TMP_DIR/sec/at02-sec.sql -Encoding default -Append
      run-sql-script $HOME_TMP_DIR/sec/at02-sec.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    }

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "stage-redes") {
    Write-Output ""
    Write-Output "2.3. Stage REDES - Mainframe ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output ""
    Write-Output "2.3.1. Clean-up stage area ..."
    Write-Output ""

    New-Item -ItemType Directory -Force -Path $HOME_TMP_DIR/redes | Out-Null
    ForEach ( $removeItem IN (
         "$HOME_TMP_DIR/redes/*"
      )) { 
      If (Test-Path $removeItem ) { Remove-Item -Recurse -Force $removeItem }
    }

    Write-Output ""
    Write-Output "2.3.2. Copying configurations ..."
    Write-Output ""
    New-Item -ItemType Directory -Force -Path $HOME_TMP_DIR/redes/logs | Out-Null
    New-Item -ItemType Directory -Force -Path $HOME_TMP_DIR/redes/Evidences | Out-Null
    ForEach ( $copyItem IN (
        "$HOME_SCRIPT_DIR/bin/cielo/log4j.properties",
        "$HOME_SCRIPT_DIR/bin/cielo/mainframeRedes.properties",
        "$HOME_SCRIPT_DIR/bin/cielo/resource"
      )) { 
      Copy-Item -Path $copyItem -Destination $HOME_TMP_DIR/redes -force -Recurse
    }

    Write-Output ""
    Write-Output "2.3.3. Extract mainframe navigation into (.csv) files ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    # Execute
    Write-Output ""
    Write-Output "       a. extracaoConfigProdutoEstabelecimento"
    Write-Output ""
    Set-Location -Path $HOME_TMP_DIR/redes | Out-Null
    & java -jar $HOME_SCRIPT_DIR/bin/cielo/devops.jar -a extracaoConfigProdutoEstabelecimento #| Select-Object -Last 7
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""
    Write-Output "       b. extracaoTerminalNumeroAcesso"
    Write-Output ""
    Set-Location -Path $HOME_TMP_DIR/redes | Out-Null
    & java -jar $HOME_SCRIPT_DIR/bin/cielo/devops.jar -a extracaoTerminalNumeroAcesso #| Select-Object -Last 7

    Write-Output ""
    Write-Output "2.3.4. Copying (.csv) into PostgreSql database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    $g124RedesCsvFile=Get-ChildItem $HOME_TMP_DIR/redes/Evidences -Name "mainframeRedes_99(a)_MainframeRedes-Extracao-EstabelecimentoG124.csv"        -Recurse | Select-Object -Last 1
    $g125RedesCsvFile=Get-ChildItem $HOME_TMP_DIR/redes/Evidences -Name "mainframeRedes_99(b)_MainframeRedes-Extracao-ProdutoEstabelecimentoG125.csv" -Recurse | Select-Object -Last 1
    $g126RedesCsvFile=Get-ChildItem $HOME_TMP_DIR/redes/Evidences -Name "mainframeRedes_99(1)_MainframeRedes-Extracao-TerminalNumeroAcessoG126.csv"   -Recurse | Select-Object -Last 1
    If ($DEBUG_LEVEL -ge 1 ) { # DEBUG_LEVEL_LOW
      Write-Output "       * Source-Folder-Path: $HOME_TMP_DIR/redes/Evidences"
      Write-Output "         - g124RedesCsvFile: $g124RedesCsvFile"
      Write-Output "         - g125RedesCsvFile: $g125RedesCsvFile"
      Write-Output "         - g126RedesCsvFile: $g126RedesCsvFile"
      Write-Output "       * Destination-Folder-Path: $HOME_CSV2PG_DIR"
    }
    If (Test-Path $HOME_CSV2PG_DIR/g124-sec.csv ) { Remove-Item -Force $HOME_CSV2PG_DIR/g124-sec.csv }
    If (Test-Path $HOME_CSV2PG_DIR/g125-sec.csv ) { Remove-Item -Force $HOME_CSV2PG_DIR/g125-sec.csv }
    If (Test-Path $HOME_CSV2PG_DIR/g126-sec.csv ) { Remove-Item -Force $HOME_CSV2PG_DIR/g126-sec.csv }
    # g124
    if ( "$g124RedesCsvFile" -eq "" ) { 
      Write-Output "         * File Not Found! $HOME_TMP_DIR/redes/Evidences/$g124RedesCsvFile"
    } else { 
      (Get-Content $HOME_TMP_DIR/redes/Evidences/$g124RedesCsvFile) -replace ';$', '' -replace '";"', ';' -replace '"$', '' -replace '^"', '' | Set-Content $HOME_CSV2PG_DIR/g124-sec.csv
      Write-Output "DROP TABLE IF EXISTS stage_mainframe_redes_g124;" | Out-File $HOME_TMP_DIR/redes/g124-sec.sql -Encoding default
      $tableColumns=((Get-Content $HOME_CSV2PG_DIR/g124-sec.csv) | Select-Object -First 1 ) -replace ';', ' VARCHAR, ' -replace '$', ' VARCHAR'
      Write-Output "CREATE TABLE stage_mainframe_redes_g124( $tableColumns );" | Out-File $HOME_TMP_DIR/redes/g124-sec.sql -Encoding default -Append 
      Write-Output "SET CLIENT_ENCODING TO WIN1252;" | Out-File $HOME_TMP_DIR/redes/g124-sec.sql -Encoding default -Append 
      Write-Output "COPY stage_mainframe_redes_g124 FROM '$HOME_CSV2PG_DIR\g124-sec.csv' WITH CSV HEADER DELIMITER ';' ;" | Out-File $HOME_TMP_DIR/redes/g124-sec.sql -Encoding default -Append
      run-sql-script $HOME_TMP_DIR/redes/g124-sec.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    }
    # g125
    if ( "$g125RedesCsvFile" -eq "" ) { 
      Write-Output "         * File Not Found! $HOME_TMP_DIR/redes/Evidences/$g125RedesCsvFile"
    } else { 
      (Get-Content $HOME_TMP_DIR/redes/Evidences/$g125RedesCsvFile) -replace ';$', '' -replace '";"', ';' -replace '"$', '' -replace '^"', '' | Set-Content $HOME_CSV2PG_DIR/g125-sec.csv
      Write-Output "DROP TABLE IF EXISTS stage_mainframe_redes_g125;" | Out-File $HOME_TMP_DIR/redes/g125-sec.sql -Encoding default
      $tableColumns=((Get-Content $HOME_CSV2PG_DIR/g125-sec.csv) | Select-Object -First 1 ) -replace ';', ' VARCHAR, ' -replace '$', ' VARCHAR'
      Write-Output "CREATE TABLE stage_mainframe_redes_g125( $tableColumns );" | Out-File $HOME_TMP_DIR/redes/g125-sec.sql -Encoding default -Append 
      Write-Output "SET CLIENT_ENCODING TO WIN1252;" | Out-File $HOME_TMP_DIR/redes/g125-sec.sql -Encoding default -Append 
      Write-Output "COPY stage_mainframe_redes_g125 FROM '$HOME_CSV2PG_DIR\g125-sec.csv' WITH CSV HEADER DELIMITER ';' ;" | Out-File $HOME_TMP_DIR/redes/g125-sec.sql -Encoding default -Append
      run-sql-script $HOME_TMP_DIR/redes/g125-sec.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    }
    # g126
    if ( "$g126RedesCsvFile" -eq "" ) { 
      Write-Output "         * File Not Found! $HOME_TMP_DIR/redes/Evidences/$g126RedesCsvFile"
    } else { 
      (Get-Content $HOME_TMP_DIR/redes/Evidences/$g126RedesCsvFile) -replace ';$', '' -replace '";"', ';' -replace '"$', '' -replace '^"', '' | Set-Content $HOME_CSV2PG_DIR/g126-sec.csv
      Write-Output "DROP TABLE IF EXISTS stage_mainframe_redes_g126;" | Out-File $HOME_TMP_DIR/redes/g126-sec.sql -Encoding default
      $tableColumns=((Get-Content $HOME_CSV2PG_DIR/g126-sec.csv) | Select-Object -First 1 ) -replace ';', ' VARCHAR, ' -replace '$', ' VARCHAR'
      Write-Output "CREATE TABLE stage_mainframe_redes_g126( $tableColumns );" | Out-File $HOME_TMP_DIR/redes/g126-sec.sql -Encoding default -Append 
      Write-Output "SET CLIENT_ENCODING TO WIN1252;" | Out-File $HOME_TMP_DIR/redes/g126-sec.sql -Encoding default -Append 
      Write-Output "COPY stage_mainframe_redes_g126 FROM '$HOME_CSV2PG_DIR/g126-sec.csv' WITH CSV HEADER DELIMITER ';' ;" | Out-File $HOME_TMP_DIR/redes/g126-sec.sql -Encoding default -Append
      run-sql-script $HOME_TMP_DIR/redes/g126-sec.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    }

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "stage-simutermweb") {
    Write-Output ""
    Write-Output "2.4. Stage SimuTermWeb - SQLServer database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    $QUERY_TRUNCATE_FILENAME="${HOME_SCRIPT_DIR}/sql/node/cielo/etl/script-02ba-postgres-truncate.sql"
    $QUERY_SELECT_FILENAME="${HOME_SCRIPT_DIR}/sql/node/cielo/etl/script-02ba-sqlserver-queryselect.sql"
    $QUERY_INSERT_FILENAME="${HOME_SCRIPT_DIR}/sql/node/cielo/etl/script-02ba-postgres-queryinsert.sql"
    run-sql-script ${QUERY_TRUNCATE_FILENAME} ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD
    java -jar ${HOME_SCRIPT_DIR}/bin/copysqlservertopostgres.jar -S ${SQLSERVER_JDBC_URL} -P ${POSTGRES_JDBC_URL}  -i ${QUERY_INSERT_FILENAME} -s ${QUERY_SELECT_FILENAME}

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


Write-Output ""
Write-Output "3. Transform step-by-step execution ..."
Write-Output ""

if ($STEP -eq "all" -or $STEP -eq "transform-star") {
    Write-Output ""
    Write-Output "3.1. Transform STAR - Oracle database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_transform_star('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/transform-star.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/transform-star.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "transform-sec") {
    Write-Output ""
    Write-Output "3.2. Transform SEC - Mainframe ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_transform_sec('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/transform-sec.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/transform-sec.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "transform-redes") {
    Write-Output ""
    Write-Output "3.3. Transform REDES - Mainframe ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_transform_redes('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/transform-redes.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/transform-redes.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "transform-simutermweb") {
    Write-Output ""
    Write-Output "3.4. Transform SimuTermWeb - SQLServer database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_transform_simutermweb('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/transform-simutermweb.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/transform-simutermweb.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


Write-Output ""
Write-Output "4. Load step-by-step execution ..."
Write-Output ""

if ($STEP -eq "all" -or $STEP -eq "load-star") {
    Write-Output ""
    Write-Output "4.1. Load STAR - Oracle database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_load_star_ec('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/load-star.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/load-star.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "load-sec") {
    Write-Output ""
    Write-Output "4.2. Load SEC - Mainframe ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_load_sec_ec('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/load-sec.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/load-sec.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "load-redes") {
    Write-Output ""
    Write-Output "4.3. Load REDES - Mainframe ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_load_redes_ec('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/load-redes.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/load-redes.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "load-simutermweb") {
    Write-Output ""
    Write-Output "4.4. Load SimuTermWeb - SQLServer database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_load_simutermweb('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/load-simutermweb.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/load-simutermweb.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}




if ($STEP -eq "all" -or $STEP -eq "load-config") {
    Write-Output ""
    Write-Output "4.5. Load Config - Postgres database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_load_config_login('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/load-config.sql -Encoding default
    Write-Output "SELECT fx_node_load_config_subclasses('$ENVIRONMENT_TYPE_NAME');" | Out-File $HOME_TMP_DIR/load-config.sql -Encoding default -Append
    Write-Output "SELECT fx_node_load_config_capirdmdr('$ENVIRONMENT_TYPE_NAME');"  | Out-File $HOME_TMP_DIR/load-config.sql -Encoding default -Append
    run-sql-script $HOME_TMP_DIR/load-config.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "load-node-jsonfilter") {
    Write-Output ""
    Write-Output "4.6. Load Config - Postgres database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_load_json_filter();" | Out-File $HOME_TMP_DIR/load-config.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/load-config.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}


if ($STEP -eq "all" -or $STEP -eq "load-node-lookupvalue") {
    Write-Output ""
    Write-Output "4.7. Load Config - Postgres database ..."
    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

    Write-Output "SELECT fx_node_load_lookup_value();" | Out-File $HOME_TMP_DIR/load-config.sql -Encoding default
    run-sql-script $HOME_TMP_DIR/load-config.sql ${POSTGRES_HOST} ${POSTGRES_DBNAME} ${POSTGRES_USERNAME} $POSTGRES_PASSWORD

    Write-Output ""
    Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
    Write-Output ""

}



Write-Output ""
Write-Output "5. End of execution ..."
Write-Output ""
Get-Date -UFormat "%Y-%m-%d:%H:%M:%S"
Write-Output ""

# ##############################################################################
# END main()
# ##############################################################################
