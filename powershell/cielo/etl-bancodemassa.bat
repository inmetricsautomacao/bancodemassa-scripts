Echo OFF
REM # ########################################
REM # filename : etl-bancodemassa.bat
REM # arguments:
REM #            arg[1] .. arg[8]
REM # author   : Josemar F. A. Silva
REM # pre-reqs :
REM #            * n/a
REM # remarks  :
REM #            * 2018-02-21 JosemarSilva D: - E:
REM #            * 2017-11-13 JosemarSilva Convert BashScript to Bat/Powershell
REM # ##############################################################################

Echo
Echo Changing default directory ...
Echo
E:
cd \bancodemassa_scripts\lib\scripts\powershell\cielo

Echo
Echo Run PowerShell script ...
Echo
powershell.exe -File "etl-bancodemassa.ps1" %1 %2 %3 %4 %5 %6 %7 %8
